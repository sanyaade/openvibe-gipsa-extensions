#ifndef __ExtStim_H__
#define __ExtStim_H__

//#include <ov_types.h>
#include "ovas_base.h"

struct SExtStim 
{
	OpenViBE::uint64 timestamp;
	OpenViBE::uint64 identifier;
	OpenViBE::boolean processed;
};

#endif
