#include "ovasCConfigurationTobiA.h"

#include <openvibe-toolkit/ovtk_all.h>

#include <iostream>
#include <fstream>
#include <list>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace std;

//___________________________________________________________________//
//                                                                   //

CConfigurationTobiA::CConfigurationTobiA(const char* sGtkBuilderFileName)
	:CConfigurationBuilder(sGtkBuilderFileName)
	,m_sHostName("localhost")
	,m_ui32HostPort(4000)
    ,m_bNewProtocol(true)
{
}

CConfigurationTobiA::~CConfigurationTobiA(void)
{
}

//___________________________________________________________________//
//                                                                   //

boolean CConfigurationTobiA::setHostName(const CString& sHostName)
{
	m_sHostName=sHostName;
	return true;
}

boolean CConfigurationTobiA::setHostPort(const uint32 ui32HostPort)
{
	m_ui32HostPort=ui32HostPort;
	return true;
}


boolean CConfigurationTobiA::setNewProtocol(const boolean bSRCorrection)
{
	m_bNewProtocol=bSRCorrection;
	return true;
}
//___________________________________________________________________//
//                                                                   //

CString CConfigurationTobiA::getHostName(void) const
{
	return m_sHostName;
}

uint32 CConfigurationTobiA::getHostPort(void) const
{
	return m_ui32HostPort;
}

boolean CConfigurationTobiA::getNewProtocol(void) const
{
	return m_bNewProtocol;
}
//___________________________________________________________________//
//                                                                   //

boolean CConfigurationTobiA::preConfigure(void)
{
	boolean l_bParentResult=CConfigurationBuilder::preConfigure();

	m_pHostName=GTK_WIDGET(gtk_builder_get_object(m_pBuilderConfigureInterface, "entry_host_name"));
	m_pHostPort=GTK_WIDGET(gtk_builder_get_object(m_pBuilderConfigureInterface, "spinbutton_host_port"));
	m_pNewProtocol=GTK_WIDGET(gtk_builder_get_object(m_pBuilderConfigureInterface, "checkbutton_NewProtocol"));

	gtk_spin_button_set_value(
		GTK_SPIN_BUTTON(m_pHostPort),
		m_ui32HostPort);
	gtk_entry_set_text(
		GTK_ENTRY(m_pHostName),
		m_sHostName.toASCIIString());
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(m_pNewProtocol),
		m_bNewProtocol);

	return l_bParentResult;
}

boolean CConfigurationTobiA::postConfigure(void)
{
	if(m_bApplyConfiguration)
	{
		m_ui32HostPort=gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(m_pHostPort));
		m_sHostName=gtk_entry_get_text(GTK_ENTRY(m_pHostName));
        m_bNewProtocol=gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(m_pNewProtocol));
	}

	return CConfigurationBuilder::postConfigure();
}
