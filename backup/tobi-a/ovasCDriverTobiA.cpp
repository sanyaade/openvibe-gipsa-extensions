#include "ovasCDriverTobiA.h"
#include "ovasCConfigurationTobiA.h"
#include <openvibe-toolkit/ovtk_all.h>

#include <system/Time.h>

using namespace OpenViBEAcquisitionServer;
using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace std;

//___________________________________________________________________//
//                                                                   //

CDriverTobiA::CDriverTobiA(IDriverContext& rDriverContext)
	:IDriver(rDriverContext)
	,m_pCallback(NULL)
	,m_ui32SampleCountPerSentBlock(0)
	,m_pSample(NULL)
	,m_sHostName("127.0.0.1")
	,m_ui32PortNumber(9000)
	,m_i32ConnectionID(-1)
	,m_bNewProtocol(true)
	,client_(true)
	,packet_(0)
{
	m_oHeader.setSamplingFrequency(0);
	m_oHeader.setChannelCount(0);
}

CDriverTobiA::~CDriverTobiA(void)
{

}

const char* CDriverTobiA::getName(void)
{
	return "TobiA Driver";
}

//___________________________________________________________________//
//                                                                   //

OpenViBE::boolean CDriverTobiA::initialize(
	const uint32 ui32SampleCountPerSentBlock,
	IDriverCallback& rCallback)
{
	if (m_rDriverContext.isConnected()) return false;

	try
	{
	   //client_.connect("127.0.0.1", 9000);
	   client_.useNewTiA(this->m_bNewProtocol);
	   client_.connect(m_sHostName.toASCIIString(), (int)m_ui32PortNumber);

	   if (!client_.connected()) return false;

	   client_.requestConfig();
	   
	   tia::SignalInfo info = client_.config().signal_info;
	   m_oHeader.setSamplingFrequency(info.masterSamplingRate());
	   m_oHeader.setChannelCount(info.signals().begin()->second.channels().size());

	   m_rDriverContext.getLogManager() << LogLevel_Info << "Sampling frequency: " << m_oHeader.getSamplingFrequency() << "\n";
	   m_rDriverContext.getLogManager() << LogLevel_Info << "Channels: " << m_oHeader.getChannelCount() << "\n";

	   m_pSample = new float32[m_oHeader.getChannelCount()*ui32SampleCountPerSentBlock];

	}
	catch(std::exception& e)
	{
	  m_rDriverContext.getLogManager() << LogLevel_Error  << "Error when connecting or receiving config -- " << e.what() << "\n";
	  return false;
	}

	if (!m_oHeader.isChannelCountSet()||!m_oHeader.isSamplingFrequencySet()) return false;

	m_pCallback = &rCallback;
	m_ui32SampleCountPerSentBlock = ui32SampleCountPerSentBlock;

	return true;
}

OpenViBE::boolean CDriverTobiA::start(void)
{
	if (!m_rDriverContext.isConnected()) return false;
	if (m_rDriverContext.isStarted()) return false;

	packet_ = client_.getEmptyDataPacket();

	OpenViBE::boolean l_bIsUdp = false;
	client_.startReceiving(l_bIsUdp);

	return client_.connected();
}

OpenViBE::boolean CDriverTobiA::loop(void)
{
	if (!m_rDriverContext.isConnected() || !client_.connected()) return false;

	if (m_rDriverContext.isStarted())
	{
	    int32 l_iSampleCount;
		OpenViBE::CStimulationSet l_oStimulationSet;
		l_oStimulationSet.setStimulationCount(0);

        vector<double> v;

		vector<vector<OpenViBE::float32>> samples;

        /*  if (!client_.receiving())
        {
           //wait
        }*/

        if(client_.receiving())
        {
            try
			{
				for(uint32 j=0; j<m_ui32SampleCountPerSentBlock; j++)
				{
					client_.getDataPacket(*packet_);

					v = packet_->getSingleDataBlock(SIG_EEG);

					std::vector<OpenViBE::float32> v_float(v.begin(), v.end());

					samples.push_back(v_float);
				}

				for(uint32 i=0; i<m_oHeader.getChannelCount(); i++) 
				{
					for(uint32 j=0; j<m_ui32SampleCountPerSentBlock; j++)
					{
						m_pSample[i*m_ui32SampleCountPerSentBlock+j]=(samples[j])[i];
					}
				}
			}

			catch (std::exception& e)
            {
                m_rDriverContext.getLogManager() << LogLevel_Error << "*** " << e.what() << "\n";
            }
		  }

		m_pCallback->setSamples(m_pSample,m_ui32SampleCountPerSentBlock);

		m_pCallback->setStimulationSet(l_oStimulationSet);

		m_rDriverContext.correctDriftSampleCount(m_rDriverContext.getSuggestedDriftCorrectionSampleCount());
	}

	return true;
}

OpenViBE::boolean CDriverTobiA::stop(void)
{
	if (!m_rDriverContext.isConnected()) return false;
	if (!m_rDriverContext.isStarted()) return false;

	try 
	{
      client_.stopReceiving();
    }
    catch (std::exception& e)
    {
       m_rDriverContext.getLogManager() << LogLevel_Error << "Stop Receiving failed -- Error:" << "\n" << "--> " << e.what() << "\n";
    }

	return true;
}

OpenViBE::boolean CDriverTobiA::uninitialize(void)
{
	if (!m_rDriverContext.isConnected()) return false;
	if (m_rDriverContext.isStarted()) return false;

	client_.disconnect();

	if (!m_pSample)
	{
		delete [] m_pSample;
		m_pSample = NULL;
    } 
	m_pCallback=NULL;


	return true;
}

//___________________________________________________________________//
//                                                                   //
OpenViBE::boolean CDriverTobiA::isConfigurable(void)
{
	return true;
}

OpenViBE::boolean CDriverTobiA::configure(void)
{
	CConfigurationTobiA l_oConfiguration("../share/openvibe-applications/acquisition-server/interface-TobiA.ui");
	l_oConfiguration.setNewProtocol(m_bNewProtocol);
	l_oConfiguration.setHostPort(m_ui32PortNumber);
	l_oConfiguration.setHostName(m_sHostName);

	if (l_oConfiguration.configure(m_oHeader))
	{
		m_bNewProtocol = l_oConfiguration.getNewProtocol();
		m_ui32PortNumber = l_oConfiguration.getHostPort();
		m_sHostName = l_oConfiguration.getHostName();
		return true;
	}
	return false;
}