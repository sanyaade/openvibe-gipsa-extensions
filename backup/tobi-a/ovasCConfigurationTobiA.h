#ifndef __OpenViBE_AcquisitionServer_CConfigurationTobiA_H__
#define __OpenViBE_AcquisitionServer_CConfigurationTobiA_H__

#include "../ovasCConfigurationBuilder.h"

namespace OpenViBEAcquisitionServer
{
    /**
	 * \class CConfigurationTobiA
     * \author Anton Andreev Gipsa-lab/CNRS
	 * \date 23/1/2013
	 * \brief TobiA driver for SignalServer -- Version: 1.0 developed by Laboratory of Brain-Computer Interfaces, Graz University of Technology
	 * TODO: details
	 *
	 */
	class CConfigurationTobiA : public OpenViBEAcquisitionServer::CConfigurationBuilder
	{
	public:

		CConfigurationTobiA(const char* sGtkBuilderFileName);
		virtual ~CConfigurationTobiA(void);

		virtual OpenViBE::boolean setHostName(const OpenViBE::CString& sHostName);
		virtual OpenViBE::boolean setHostPort(const OpenViBE::uint32 ui32HostPort);
		virtual OpenViBE::boolean setNewProtocol(OpenViBE::boolean isNewProtocol);

		virtual OpenViBE::CString getHostName(void) const;
		virtual OpenViBE::uint32 getHostPort(void) const;
		virtual OpenViBE::boolean getNewProtocol(void) const;

	protected:

		virtual OpenViBE::boolean preConfigure(void);
		virtual OpenViBE::boolean postConfigure(void);

	private:

		CConfigurationTobiA(void);

	protected:

		::GtkWidget* m_pHostName;
		::GtkWidget* m_pHostPort;
		::GtkWidget* m_pMinSamples;
		::GtkWidget* m_pNewProtocol;

		OpenViBE::CString m_sHostName;
		OpenViBE::uint32 m_ui32HostPort;
		OpenViBE::boolean m_bNewProtocol;
	};
};

#endif // __OpenViBE_AcquisitionServer_CConfigurationTobiA_H__
