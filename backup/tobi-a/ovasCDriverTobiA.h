#ifndef __OpenViBE_AcquisitionServer_CDriverTobiA_H__
#define __OpenViBE_AcquisitionServer_CDriverTobiA_H__

#include "../ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

// STL
#include <iostream>
#include <fstream>
#include <algorithm>

// Boost
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>

// local
#include "tia/data_packet_interface.h"
#include "tia/tia_client.h"
#include "tia/ssconfig.h"
#include "tia/defines.h"


namespace OpenViBEAcquisitionServer
{
	/**
	 * \class CDriverTobiA
	 * \author Anton Andreev Gipsa-lab/CNRS
	 * \date 23/1/2013
	 * \brief TobiA driver for SignalServer -- Version: 1.0 developed by Laboratory of Brain-Computer Interfaces, Graz University of Technology
	 * You may need to compile as "Multi-threaded Debug DLL (/MDd)" in VC++
	 *
	 */
	class CDriverTobiA : public OpenViBEAcquisitionServer::IDriver
	{
	public:

		CDriverTobiA(OpenViBEAcquisitionServer::IDriverContext& rDriverContext);
		virtual ~CDriverTobiA(void);
		virtual const char* getName(void);

		virtual OpenViBE::boolean initialize(
			const OpenViBE::uint32 ui32SampleCountPerSentBlock,
			OpenViBEAcquisitionServer::IDriverCallback& rCallback);

		virtual OpenViBE::boolean uninitialize(void);

		virtual OpenViBE::boolean start(void);
		virtual OpenViBE::boolean stop(void);
		virtual OpenViBE::boolean loop(void);

		virtual OpenViBE::boolean isConfigurable(void);
		virtual OpenViBE::boolean configure(void);
		virtual const OpenViBEAcquisitionServer::IHeader* getHeader(void) { return &m_oHeader; }
		

	protected:

		OpenViBEAcquisitionServer::IDriverCallback* m_pCallback;
		OpenViBEAcquisitionServer::CHeader m_oHeader;

		OpenViBE::uint32 m_ui32SampleCountPerSentBlock;
		OpenViBE::float32* m_pSample;
	
	private:

		OpenViBE::CString m_sHostName;
		OpenViBE::uint32  m_ui32PortNumber;
		OpenViBE::int32   m_i32ConnectionID;
		OpenViBE::boolean m_bNewProtocol;

		tia::TiAClient                   client_;
        tia::DataPacket*                 packet_;

	};
};

#endif // __OpenViBE_AcquisitionServer_CDriverTobiA_H__
