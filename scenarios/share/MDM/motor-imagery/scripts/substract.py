import numpy
#from scipy.linalg.matfuncs import sqrtm

class MyOVBox(OVBox):
    # the constructor creates the box and initializes object variables
	def __init__(self):
		OVBox.__init__(self)

	# the initialize method reads settings and outputs the first header
	def initialize(self):
		# all settings
		ScoreHeader = OVStreamedMatrixHeader(0,0,[1],['Score'])
		self.output[0].append(ScoreHeader)

	def process(self):
		# process signal input
		for chunk_index in range( len(self.input[0]) ):
												
			if(type(self.input[0][chunk_index]) == OVStreamedMatrixHeader):
				self.matrix_header = self.input[0].pop()

			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixBuffer):
				chunk = self.input[0].pop()
				X = numpy.matrix(chunk).reshape(tuple(self.matrix_header.dimensionSizes))
				score = X[0,0]-X[0,1]						
				
				# output row scores
				outBuffer = OVStreamedMatrixBuffer(self._currentTime,self._currentTime+1.0/self._clock, [score])
				self.output[0].append(outBuffer)
					
			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixEnd):
				self.input[0].pop()
	
if __name__ == '__main__':
	box = MyOVBox()
