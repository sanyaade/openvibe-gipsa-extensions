#!/bin/lua

-- this function is called when the box is initialized
function initialize(box)
		dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")
        offset = box:get_setting(2)
        stimulation_id = _G[box:get_setting(3)]
        is_send = false        
end

-- this function is called when the box is uninitialized
function uninitialize(box)

end

-- this function is called once by the box
function process(box)

      time = box:get_current_time()
            
      -- sends stimulation
      box:send_stimulation(1, stimulation_id, time+offset)
      
end
