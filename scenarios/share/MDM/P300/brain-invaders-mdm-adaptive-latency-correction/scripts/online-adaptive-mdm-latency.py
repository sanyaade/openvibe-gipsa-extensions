import numpy
import os
import time
from multiprocessing import Queue
from threading import Thread
#from scipy.linalg.matfuncs import sqrtm
from numpy.linalg import inv, eigvalsh, norm, eig

class MyOVBox(OVBox):
    # the constructor creates the box and initializes object variables
	def __init__(self):
		OVBox.__init__(self)
		self.iteration = 0.0
		self.train_iteration = 0.0
		self.trained_iteration = 0.0
		self.Nrep = None

		self.pending_stim = []
		self.pending_score = []
		self.target_chunk = []
		self.non_target_chunk = []
		
		self.res = dict()
		self.endOfRep = False
		self.localC1 = None
		self.localC0 = None
		
		self.p = None
                
	# the initialize method reads settings and outputs the first header
	def initialize(self):
		# all settings
		NrepEnv = os.environ.get('NREP')
		if NrepEnv is None:
				self.Nrep = int(self.setting['Nrep'])
		else:
				self.Nrep = int(NrepEnv)
		self.newtarget()
		
		self.globalP1 = numpy.matrix(numpy.loadtxt(open(self.setting['P1 file'],"rb"),delimiter=","));
		self.globalC1 = numpy.matrix(numpy.loadtxt(open(self.setting['C1 file'],"rb"),delimiter=","));
		self.globalC0 = numpy.matrix(numpy.loadtxt(open(self.setting['C0 file'],"rb"),delimiter=","));
		self.globalP0 = numpy.matrix(numpy.loadtxt(open(self.setting['P0 file'],"rb"),delimiter=","));
		
		self.C1 = self.globalC1
		self.C0 = self.globalC0
		self.P0 = numpy.zeros((self.C1.shape[0]-self.globalP1.shape[0],160))
		self.latency = 16
		
		self.q = Queue()
		self.p = Thread(target=self.update_local_params, args=(self.target_chunk,self.non_target_chunk,self.q))
		print 'initialize descision box with max number of repetition = ' + str(self.Nrep)

	def process(self):

		# process first stimulation input
		for chunk_index in range( len(self.input[1]) ):
			
			if(type(self.input[1][chunk_index]) == OVStimulationHeader):
				self.stimulation_header = self.input[1].pop()
			
			elif(type(self.input[1][chunk_index]) == OVStimulationSet):
				stim = self.input[1].pop()
				while(len(stim)>0):
					st = stim.pop()
					# Row and col stim
					if( (st.identifier>=33024) and (st.identifier<=33055)):
							self.pending_stim.insert(0,st.identifier-33024)
					#end of repetition stim
					elif(st.identifier==33279):
							self.endOfRep = True
							self.Number_of_stim = len(self.pending_stim)
							print "end of repetition : " + str(self.Number_of_stim) + " stimulations code received"
							
		# process signal input
		for chunk_index in range( len(self.input[0]) ):
                        
			if(type(self.input[0][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header = self.input[0].pop()
	
			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixBuffer):
					chunk = self.input[0].pop()
					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header.dimensionSizes))
					self.P0 += X
					#Global systeme
					X2 = numpy.concatenate((self.globalP1,X[:,self.latency:self.latency+self.globalP1.shape[1]]),axis=0)
					C = numpy.matrix(numpy.cov(X2))

					d1 = self.distance(C,self.C1)
					d0 = self.distance(C,self.C0)
					score = d1-d0								
					self.pending_score.insert(0,[score])
					
		

			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixEnd):
					self.input[0].pop()

		# process target input
		for chunk_index in range( len(self.input[2]) ):
                        
			if(type(self.input[2][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header_Target = self.input[2].pop()
	
			elif(type(self.input[2][chunk_index]) == OVStreamedMatrixBuffer):
					chunk = self.input[2].pop()
					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header_Target.dimensionSizes))
					self.target_chunk.insert(0,X)

			elif(type(self.input[2][chunk_index]) == OVStreamedMatrixEnd):
					self.input[2].pop()
		
		# process non target input
		for chunk_index in range( len(self.input[3]) ):
                        
			if(type(self.input[3][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header_NonTarget = self.input[3].pop()
	
			elif(type(self.input[3][chunk_index]) == OVStreamedMatrixBuffer):
					chunk = self.input[3].pop()
					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header_NonTarget.dimensionSizes))
					self.non_target_chunk.insert(0,X)

			elif(type(self.input[3][chunk_index]) == OVStreamedMatrixEnd):
					self.input[3].pop()

		#release time for thread 
		if (self.p.is_alive()):
			time.sleep(0.0025)
		
		#~ # End of repetition
		if(self.endOfRep):
				
				Number_of_chunk = len(self.pending_score)
				# Wait for receiving the last results 
				if(Number_of_chunk<self.Number_of_stim):
						pass
				else:

						# Get the right number of results ==> Start Processing
						self.iteration += 1
						self.train_iteration +=1
						self.latency = self.estimate_latency(self.globalP0,self.P0)
						print self.latency
						for index in range(self.Number_of_stim):
								
								score = self.pending_score.pop()
								
								stim  = self.pending_stim.pop()
								if(stim<16):
										#it's a row !!!
										if(self.rowresults.has_key(stim)):
												self.rowresults[stim].append(score)
										else:
												self.rowresults[stim] = [score]
								if(stim>=16):
										#it's a column !!!
										if(self.colresults.has_key(stim-16)):
												self.colresults[stim-16].append(score)
										else:
												self.colresults[stim-16] = [score]                               
						
						if not(self.p.is_alive()):
							print "Start thread"
							self.p = Thread(target=self.update_local_params, args=(self.target_chunk,self.non_target_chunk,self.q,self.train_iteration,self.latency))
							self.p.start()
						for i in range(self.q.qsize()):
							[self.localC1,self.localC0,self.trained_iteration] = self.q.get()
							alpha = numpy.min([self.trained_iteration/40,1])
							self.C1 = self.geodesic(self.globalC1,self.localC1,alpha)
							self.C0 = self.geodesic(self.globalC0,self.localC0,alpha)
						
						
						self.endOfRep = False

						if (self.iteration==self.Nrep):
								#send results
								resultsRow = []
								for r in self.rowresults.itervalues():
										resultsRow.append(numpy.mean(r))

								resultsCol = []
								for c in self.colresults.itervalues():
										resultsCol.append(numpy.mean(c))

				  
								# output row scores
								NRows = len(resultsRow)
								NCols = len(resultsCol)
								#chanLabelsRow = (NRows+NCols+1)*['Row']
								chanLabelsRow = ['value'] + (NRows)*['Row']+(NCols)*['Column'] + ['empty']*(32-(NRows+NCols))
								outHeaderRow = OVStreamedMatrixHeader(self.matrix_header.startTime,self.matrix_header.endTime,[1,32],chanLabelsRow)
								self.output[0].append(outHeaderRow)
								resultsRow.extend(resultsCol)
								resultsRow.extend([0]*(32-(NRows+NCols)))
								outBufferRow = OVStreamedMatrixBuffer(self._currentTime,self._currentTime+1.0/self._clock, resultsRow)
								self.output[0].append(outBufferRow)
								print resultsRow


								self.newtarget()
                                                
                                                
	
	def newtarget(self):
		self.iteration = 0.0
		self.rowresults = dict()
		self.colresults = dict()
			

	def covariance_p300(self,Dict,P1,latency):
		CovSet = []
		for index in range(len(Dict)):
			X = numpy.concatenate((P1,Dict[index][:,latency:latency+P1.shape[1]]),axis=0)
			CovSet.append(numpy.matrix(numpy.cov(X)))
		return CovSet
		
	def mean_covariance(self,Dict):
		#init 
		C = numpy.mean(Dict,axis=0)
		k=0
		J = numpy.eye(2)
		# stop when J<10^-4 or max iteration = 50
		while (numpy.linalg.norm(J,ord='fro')>0.001) and (k<20):
			k=k+1
			Cm12 = self.invsqrtm(C)
			C12 = self.sqrtm(C)
			T = []
			for index in range(len(Dict)):
				T.append(self.logm(numpy.matrix(Cm12*Dict[index]*Cm12)))
			
			J = numpy.mean(T,axis=0)	
			C = numpy.matrix(C12*self.expm(J)*C12)
		return C			
	
	def update_local_params(self,target_chunk,non_target_chunk,queue,iteration,latency):
		P1 = self.globalP1
		CovSetT = self.covariance_p300(target_chunk,P1,latency)
		CovSetnT = self.covariance_p300(non_target_chunk,P1,latency)
		C1 = self.mean_covariance(CovSetT)
		C0 = self.mean_covariance(CovSetnT)
		queue.put([C1,C0,iteration])
	
	def sqrtm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.sqrt(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out

	def logm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.log(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out
		
	def expm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.exp(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out

	def invsqrtm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(1.0/numpy.sqrt(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out
	
	def powm(self,Ci,alpha):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(D**alpha))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out		
		
		
	def distance(self,C0,C1):
		P = self.invsqrtm(C0)
		A = P*C1*P
		D,V = eig(A)
		l = numpy.log(D)
		d = norm(l)
		return d
		
	def geodesic(self,C0,C1,alpha):
		E = (1.0-alpha)*C0 + alpha*C1
		A = numpy.matrix(self.sqrtm(C0))
		B = numpy.matrix(self.invsqrtm(C0))
		C = B*C1*B
		D = numpy.matrix(self.powm(C,alpha))
		E = numpy.matrix(A*D*A)
		return E
	
	def estimate_latency(self,gP0,lP0):
		diff = lP0.shape[1]-gP0.shape[1]
		r = []
		for i in range(diff):
			r.append(numpy.corrcoef(gP0.flatten(1),lP0[:,i:i+gP0.shape[1]].flatten(1))[0,1])
		return r.index(max(r))
		
	
	def uninitialize(self):
		print "process uninitialize!"
		if (self.p.is_alive()):
			self.p.join()	
if __name__ == '__main__':
	box = MyOVBox()
