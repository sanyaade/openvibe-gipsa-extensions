#!/bin/lua
dofile("../share/openvibe-plugins/stimulation/lua-stimulator-stim-codes.lua")

-- this function is called when the box is initialized
function initialize(box)
        box:log("Trace", "initialize has been called")
end

-- this function is called when the box is uninitialized
function uninitialize(box)
        box:log("Trace", "uninitialize has been called")
end

-- this function is called once by the box
function process(box)

	-- enters infinite loop
	-- cpu will be released with a call to sleep
	-- at the end of the loop
	while true do

		-- gets current simulated time
		t = box:get_current_time()

		-- loops on every received stimulation for a given input
		for stimulation = 1, box:get_stimulation_count(1) do

            box:send_stimulation(1, OVTK_StimulationId_Label_01, t, 0)
            box:send_stimulation(1, OVTK_StimulationId_Label_02, t+0.5, 0)

		-- discards it
		box:remove_stimulation(1, 1)

		end

		-- releases cpu
		box:sleep()
	end
end