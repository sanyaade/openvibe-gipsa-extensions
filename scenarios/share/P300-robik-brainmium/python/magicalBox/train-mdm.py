import numpy
from numpy.linalg import inv, eigvalsh, norm, eig
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import ShuffleSplit

class MyOVBox(OVBox):
    # the constructor creates the box and initializes object variables
	def __init__(self):
		OVBox.__init__(self)

		self.target_epochs = []
		self.nontarget_epochs = []

		self.C1 = None
		self.C0 = None
		self.P1 = None
		
		self.train = False
                
	# the initialize method reads settings and outputs the first header
	def initialize(self):
		# all settings
		print "Initialize"
		self.stim_start_train = OpenViBE_stimulation[self.setting['Start Training Stimulation']]
		self.p1file = self.setting['P1 file']
		self.c1file = self.setting['C1 file']
		self.c0file = self.setting['C0 file']
		self.stimtraincompleted = 33287
		self.crossvalid = self.setting['Cross validation']
		if (self.crossvalid=="true"):
			self.ncv = int(self.setting['Ncv'])
			print "Cross Validation set to " + str(self.ncv) + "-fold"

	def process(self):
		# process first stimulation input
		for chunk_index in range( len(self.input[2]) ):
			
			if(type(self.input[2][chunk_index]) == OVStimulationHeader):
				self.stimulation_header = self.input[2].pop()
			
			elif(type(self.input[2][chunk_index]) == OVStimulationSet):
				stim = self.input[2].pop()
				while(len(stim)>0):
					st = stim.pop()
					# Row and col stim
					if( (st.identifier==self.stim_start_train)):
						print "Start Training"
						self.train = True
							

		# process target input
		for chunk_index in range( len(self.input[0]) ):
                        
			if(type(self.input[0][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header_Target = self.input[0].pop()
	
			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixBuffer):
					chunk = self.input[0].pop()
					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header_Target.dimensionSizes))
					self.target_epochs.insert(0,X)

			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixEnd):
					self.input[0].pop()
		
		# process non target input
		for chunk_index in range( len(self.input[1]) ):
                        
			if(type(self.input[1][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header_NonTarget = self.input[1].pop()
	
			elif(type(self.input[1][chunk_index]) == OVStreamedMatrixBuffer):
					chunk = self.input[1].pop()
					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header_NonTarget.dimensionSizes))
					self.nontarget_epochs.insert(0,X)

			elif(type(self.input[1][chunk_index]) == OVStreamedMatrixEnd):
					self.input[1].pop()
					
		##~ # End of repetition
		if(self.train):
				
				Number_of_target = len(self.target_epochs)
				Number_of_nontarget = len(self.nontarget_epochs)
				
				print "Got " + str(Number_of_target) + " Target epochs and " + str(Number_of_nontarget) + " Non Target epochs"
				
				self.P1,self.C1,self.C0 = self.mdm(self.target_epochs,self.nontarget_epochs)
				
				numpy.savetxt(self.p1file,self.P1,delimiter=",")
				numpy.savetxt(self.c1file,self.C1,delimiter=",")
				numpy.savetxt(self.c0file,self.C0,delimiter=",")
				
				print "Training completed !"
				if (self.crossvalid=="true"):
					self.cross_validation(self.target_epochs,self.nontarget_epochs)
				
				self.train = False 
				time = self.getCurrentTime()
				stim_set = OVStimulationSet(time, time+1./self._clock)
				stim_set.append(OVStimulation(self.stimtraincompleted, time, 0.))
				self.output[0].append(stim_set)                                            

	def covariance_p300(self,Dict,P1):
		CovSet = []
		for index in range(len(Dict)):
			X = numpy.concatenate((P1,Dict[index]),axis=0)
			CovSet.append(numpy.matrix(numpy.cov(X)))
		return CovSet
		
	def mean_covariance(self,Dict):
		#init 
		C = numpy.mean(Dict,axis=0)
		k=0
		J = numpy.eye(2)
		# stop when J<10^-4 or max iteration = 50
		while (numpy.linalg.norm(J,ord='fro')>0.0001) and (k<50):
			k=k+1
			Cm12 = self.invsqrtm(C)
			C12 = self.sqrtm(C)
			T = []
			for index in range(len(Dict)):
				T.append(self.logm(numpy.matrix(Cm12*Dict[index]*Cm12)))
			
			J = numpy.mean(T,axis=0)	
			C = numpy.matrix(C12*self.expm(J)*C12)
			
		return C			
	
	def sqrtm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.sqrt(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out

	def logm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.log(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out
		
	def expm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.exp(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out

	def invsqrtm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(1.0/numpy.sqrt(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out
		
	def distance(self,C0,C1):
		P = self.invsqrtm(C0)
		A = P*C1*P
		D,V = eig(A)
		l = numpy.log(D)
		d = norm(l)
		return d
	
	def mdm(self,Target,NonTarget):
		P1 = numpy.mean(Target,axis=0)
		CovSetT = self.covariance_p300(Target,P1)
		CovSetnT = self.covariance_p300(NonTarget,P1)
		C1 = self.mean_covariance(CovSetT)
		C0 = self.mean_covariance(CovSetnT)
		return P1,C1,C0
		
	def cross_validation(self,Target,NonTarget):
		NT = len(Target)
		NnT = len(NonTarget)
		Set = numpy.array(Target + NonTarget)
		y = numpy.array([1]*NT+[0]*NnT)
		# Compute ROC curve and area the curve
		
		kf = ShuffleSplit(len(y), self.ncv, indices=True)
		roc_auc = []
		y1full = numpy.array([])
		Yfull = numpy.array([])
		for train, test in kf:
			trainSet = Set[train]
			testSet = Set[test]
			ytrain  = y[train]
			ytest = y[test]
			P1,C1,C0 = self.mdm(trainSet[ytrain==1],trainSet[ytrain==0])
			y1 = self.apply_mdm(testSet,P1,C1,C0)
		
			y1full = numpy.append(y1full,y1)
			Yfull = numpy.append(Yfull,ytest)
			fpr, tpr, thresholds = roc_curve(ytest,y1)
			roc_auc.append(auc(fpr, tpr))
					
		print "Area under ROC curve in single trial : %f" % numpy.mean(roc_auc)
	
		outT = y1full[Yfull==1]
		outNT = y1full[Yfull==0]
		
		for k in [2,3,4,5,6,7,8,9,10]:
			outT2 = numpy.reshape(outT[0:k*(len(outT)/k)],(k,len(outT)/k))
			outT2 = numpy.mean(outT2,axis=0)
			outNT2 = numpy.reshape(outNT[0:k*(len(outNT)/k)],(k,len(outNT)/k))
			outNT2 = numpy.mean(outNT2,axis=0)
			y2 = numpy.array([1]*len(outT2)+[0]*len(outNT2))
			out = numpy.append(outT2,outNT2)
			fpr, tpr, thresholds = roc_curve(y2,out)
			print "Area under ROC curve for %i repetitions : %f" % (k,auc(fpr, tpr))		
		
		
		
	def apply_mdm(self,epochs,P1,C1,C0):
		CovSet = self.covariance_p300(epochs,P1)
		y1 = []
		for index in range(len(epochs)):
			y1.append(self.distance(CovSet[index],C0)-self.distance(CovSet[index],C1))
		return numpy.array(y1)
	
if __name__ == '__main__':
	box = MyOVBox()
