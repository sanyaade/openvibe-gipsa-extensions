import numpy
import os
#from scipy.linalg.matfuncs import sqrtm
from numpy.linalg import inv, eigvalsh, norm, eig

class MyOVBox(OVBox):
    # the constructor creates the box and initializes object variables
	def __init__(self):
		OVBox.__init__(self)
		self.iteration = 0
		self.Nrep = None

		self.pending_stim = []
		self.pending_chunk = []
		self.pending_target_chunk = []
		self.pending_non_target_chunk = []
		
		self.res = dict()
		self.endOfRep = False
		self.C1 = None
		self.C0 = None
		self.P1 = None
		
		self.iterC0 = 1.0
		self.iterC1 = 1.0
                
	# the initialize method reads settings and outputs the first header
	def initialize(self):
		# all settings
		NrepEnv = os.environ.get('NREP')
		if NrepEnv is None:
				self.Nrep = int(self.setting['Nrep'])
		else:
				self.Nrep = int(NrepEnv)
		self.newtarget()

                self.P1 = numpy.matrix(numpy.loadtxt(open(self.setting['P1 file'],"rb"),delimiter=","));
                self.C1 = numpy.matrix(numpy.loadtxt(open(self.setting['C1 file'],"rb"),delimiter=","));
                self.C0 = numpy.matrix(numpy.loadtxt(open(self.setting['C0 file'],"rb"),delimiter=","));
		print 'initialize descision box with max number of repetition = ' + str(self.Nrep)


	def process(self):

		# process first stimulation input
		for chunk_index in range( len(self.input[1]) ):
			
			if(type(self.input[1][chunk_index]) == OVStimulationHeader):
				self.stimulation_header = self.input[1].pop()
			
			elif(type(self.input[1][chunk_index]) == OVStimulationSet):
				stim = self.input[1].pop()
				while(len(stim)>0):
					st = stim.pop()
					# Row and col stim
					if( (st.identifier>=33024) and (st.identifier<=33055)):
							self.pending_stim.insert(0,st.identifier-33024)
					#end of repetition stim
					elif(st.identifier==33279):
							self.endOfRep = True
							self.Number_of_stim = len(self.pending_stim)
							print "end of repetition : " + str(self.Number_of_stim) + " stimulations code received"
							
		# process signal input
		for chunk_index in range( len(self.input[0]) ):
                        
			if(type(self.input[0][chunk_index]) == OVStreamedMatrixHeader):
					self.matrix_header = self.input[0].pop()
	
			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixBuffer):
					chunk = self.input[0].pop()
					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header.dimensionSizes))
					
					if (self.P1==None):
						score = 0
					else:
						X2 = numpy.concatenate((self.P1,X),axis=0)
						C = numpy.matrix(numpy.cov(X2))

						d1 = self.distance(C,self.C1)
						d0 = self.distance(C,self.C0)
						score = d1-d0								
						
					self.pending_chunk.insert(0,[score])

			elif(type(self.input[0][chunk_index]) == OVStreamedMatrixEnd):
					self.input[0].pop()

##		# process target input
##		for chunk_index in range( len(self.input[2]) ):
##                        
##			if(type(self.input[2][chunk_index]) == OVStreamedMatrixHeader):
##					self.matrix_header_Target = self.input[2].pop()
##	
##			elif(type(self.input[2][chunk_index]) == OVStreamedMatrixBuffer):
##					chunk = self.input[2].pop()
##					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header_Target.dimensionSizes))
##					self.pending_target_chunk.insert(0,X)
##
##			elif(type(self.input[2][chunk_index]) == OVStreamedMatrixEnd):
##					self.input[2].pop()
##		
##		# process non target input
##		for chunk_index in range( len(self.input[3]) ):
##                        
##			if(type(self.input[3][chunk_index]) == OVStreamedMatrixHeader):
##					self.matrix_header_NonTarget = self.input[3].pop()
##	
##			elif(type(self.input[3][chunk_index]) == OVStreamedMatrixBuffer):
##					chunk = self.input[3].pop()
##					X = numpy.matrix(chunk).reshape(tuple(self.matrix_header_NonTarget.dimensionSizes))
##					self.pending_non_target_chunk.insert(0,X)
##
##			elif(type(self.input[3][chunk_index]) == OVStreamedMatrixEnd):
##					self.input[3].pop()
					
		#~ # End of repetition
		if(self.endOfRep):
				
				Number_of_chunk = len(self.pending_chunk)
				# Wait for receiving the last results 
				if(Number_of_chunk<self.Number_of_stim):
						pass
				else:
						# Get the right number of results ==> Start Processing
						self.iteration += 1
						for index in range(self.Number_of_stim):
								chunk = self.pending_chunk.pop()
								stim  = self.pending_stim.pop()

								if(stim<16):
										#it's a row !!!
										if(self.rowresults.has_key(stim)):
												self.rowresults[stim].append(chunk[0])
										else:
												self.rowresults[stim] = [chunk[0]]
								if(stim>=16):
										#it's a column !!!
										if(self.colresults.has_key(stim-16)):
												self.colresults[stim-16].append(chunk[0])
										else:
												self.colresults[stim-16] = [chunk[0]]                               
						
##						for index in range(len(self.pending_target_chunk)):
##							self.update_average(self.pending_target_chunk[index])
##							
##						for index in range(len(self.pending_target_chunk)):
##							X = self.pending_target_chunk.pop()
##											
##							X2 = numpy.concatenate((X,self.P1),axis=0)
##							C = numpy.matrix(numpy.cov(X2))
##							alpha = min(self.iterC1,20)
##							self.updateMatrixC1(C,alpha)
##							
##						   	
##						for index in range(len(self.pending_non_target_chunk)):
##							X = self.pending_non_target_chunk.pop()
##							
##							X2 = numpy.concatenate((X,self.P1),axis=0)
##							C = numpy.matrix(numpy.cov(X2))
##							alpha = min(self.iterC0,20)
##							self.updateMatrixC0(C,alpha)
								

						self.endOfRep = False

						if (self.iteration==self.Nrep):
								#send results
								resultsRow = []
								for r in self.rowresults.itervalues():
										resultsRow.append(numpy.mean(r))

								resultsCol = []
								for c in self.colresults.itervalues():
										resultsCol.append(numpy.mean(c))

				  
								# output row scores
								NRows = len(resultsRow)
								NCols = len(resultsCol)
								#chanLabelsRow = (NRows+NCols+1)*['Row']
								chanLabelsRow = ['value'] + (NRows)*['Row']+(NCols)*['Column'] + ['empty']*(32-(NRows+NCols))
								outHeaderRow = OVStreamedMatrixHeader(self.matrix_header.startTime,self.matrix_header.endTime,[1,32],chanLabelsRow)
								self.output[0].append(outHeaderRow)
								resultsRow.extend(resultsCol)
								resultsRow.extend([0]*(32-(NRows+NCols)))
								outBufferRow = OVStreamedMatrixBuffer(self._currentTime,self._currentTime+1.0/self._clock, resultsRow)
								self.output[0].append(outBufferRow)
								print resultsRow


								self.newtarget()
                                                
                                                
	
	def newtarget(self):
		self.iteration = 0
		self.rowresults = dict()
		self.colresults = dict()

	def update_average(self,X):
		if (self.P1==None):
			self.P1=X
			self.iterP1=1.0
		else:
			self.P1 = (self.iterP1/(self.iterP1+1))*self.P1 + (1/(self.iterP1+1))*X
			self.iterP1 +=1

	def distance(self,C0,C1):
                P = self.invsqrtm(C0)
		A = P*C1*P
		D,V = eig(A)
		l = numpy.log(D)
		d = norm(l)
		return d
		
	def updateMatrixC0(self,Ci,alpha):
		if (self.C0==None):
			self.C0=Ci
			self.iterC0=1.0
		else:
			A = numpy.matrix(self.sqrtm(self.C0))
			B = numpy.matrix(inv(A))
			C = B*Ci*B
			D = numpy.matrix(self.powm(C,1.0/alpha))
			self.C0 = A*D*A
			self.iterC0 +=1
	
	def updateMatrixC1(self,Ci,alpha):
		if (self.C1==None):
			self.C1=Ci
			self.iterC1=1.0			
		else:
			A = numpy.matrix(self.sqrtm(self.C1))
			B = numpy.matrix(inv(A))
			C = B*Ci*B
			D = numpy.matrix(self.powm(C,1.0/alpha))
			self.C1 = A*D*A
			self.iterC1+=1
			
	def sqrtm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(numpy.sqrt(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out

	def invsqrtm(self,Ci):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(1/numpy.sqrt(D)))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out

	def powm(self,Ci,alpha):
		D,V = eig(Ci)
		D = numpy.matrix(numpy.diag(D**alpha))
		V = numpy.matrix(V)
		Out = numpy.matrix(V*D*V.T)
		return Out	
	
if __name__ == '__main__':
	box = MyOVBox()
