rem This file works only with the downloadable version of OpenVibe

@ECHO OFF
SETLOCAL EnableExtensions

REM Get the directory location of this script, assume it contains the OpenViBE dist tree. These variables will be used by OpenViBE executables.
SET "OV_PATH_ROOT=%~dp0"
SET "OV_PATH_BIN=%OV_PATH_ROOT%\bin"
SET "OV_PATH_LIB=%OV_PATH_ROOT%\bin"
SET "OV_PATH_DATA=%OV_PATH_ROOT%\share"

SET OV_PAUSE=PAUSE
SET OV_RUN_IN_BG=

IF /i "%1"=="--no-pause" (
	SET OV_PAUSE=
	SHIFT
)
IF /i "%1"=="--run-bg" (
	REM Run in background, disable pause. The first start arg is the app title.
	SET OV_RUN_IN_BG=START "openvibe-vr-demo.exe"
	SET OV_PAUSE=
	SHIFT
)

SET "OV_ENVIRONMENT_FILE=%OV_PATH_ROOT%\bin\OpenViBE-set-env.cmd"
IF NOT EXIST "%OV_ENVIRONMENT_FILE%" (
	ECHO Error: "%OV_ENVIRONMENT_FILE%" was not found
	GOTO EndOfScript
)
CALL "%OV_ENVIRONMENT_FILE%"

copy "%OV_PATH_ROOT%"\share\openvibe-applications\brain-invaders\brain-invaders\resources.cfg-base "%OV_PATH_ROOT%"\share\openvibe-applications\brain-invaders\brain-invaders\resources.cfg 

rem For the regular OpenVibe installation:
type "%OV_PATH_ROOT%"\dependencies\cegui\resources.cfg >> "%OV_PATH_ROOT%"\share\openvibe-applications\brain-invaders\brain-invaders\resources.cfg 
rem For a version built from source and the installer pointing to the "Dist":
type "%OV_PATH_ROOT%"\..\dependencies\cegui\resources.cfg >> "%OV_PATH_ROOT%"\share\openvibe-applications\brain-invaders\brain-invaders\resources.cfg 

cd "%OV_PATH_ROOT%"\share\openvibe-applications\brain-invaders\brain-invaders 

"%OV_PATH_ROOT%"bin\Brain-invaders-dynamic.exe "%OV_PATH_ROOT%share\brain-invaders.conf
              

	
:EndOfScript

%OV_PAUSE%