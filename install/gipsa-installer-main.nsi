	SetCompressor /FINAL /SOLID lzma
	SetCompressorDictSize 16

	!include "MUI.nsh"
	!include "zipdll.nsh"

	;Name and file
	Name "OpenVibe Gipsa-lab Extensions"
	OutFile "gipsa-setup-main.exe"

	;Default installation folder	
	InstallDir "$PROGRAMFILES" ;Note that the part of this string following the last \ will be used if the user selects 'browse', and may be appended back on to the string at install time (to disable this, end the directory with a \ (which will require the entire parameter to be enclosed with quotes).
	;Var OLDINSTDIR
	;Var DIRECTX_MISSING
	Var OVInstFolder
	Var PythonFolder

;Interface Settings

	!define MUI_ABORTWARNING

;Pages

	!insertmacro MUI_PAGE_WELCOME
	;!insertmacro MUI_PAGE_LICENSE "..\COPYING"
	!insertmacro MUI_PAGE_DIRECTORY
	!insertmacro MUI_PAGE_INSTFILES
	!insertmacro MUI_PAGE_FINISH

	!insertmacro MUI_UNPAGE_WELCOME
	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES
	!insertmacro MUI_UNPAGE_FINISH

;Languages

	!insertmacro MUI_LANGUAGE "English"

;Installer and uninstaller icons

	Icon "${NSISDIR}\Contrib\Graphics\Icons\box-install.ico"
	UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\box-uninstall.ico"

;##########################################################################################################################################################
;##########################################################################################################################################################
;##########################################################################################################################################################

Function .onInit

	UserInfo::GetAccountType
	Pop $R1
	StrCmp $R1 "Admin" has_admin_rights 0
		;MessageBox MB_OK "You must be administrator to install OpenVibe Gipsa Extensions" /SD IDOK
		;Quit
		Abort "You must be administrator to install OpenVibe Gipsa Extensions"
has_admin_rights:

	ReadRegStr $OVInstFolder HKLM SOFTWARE\openvibe InstallDir

	${If} $OVInstFolder != ""
		IfFileExists "$OVInstFolder\*.*" installation_found installation_abort		
	${Else}
       goto installation_abort
    ${EndIf}

installation_abort:
    MessageBox MB_OK "INRIA OpenVibe 1.2 or later is required in order to proceed. OpenVibe was not found on your system! Aborting! First registry is checked for the installtion folder and then the folder must exist.$\n" /SD IDOK
	;Abort 
	
installation_found:
	StrCpy $INSTDIR $OVInstFolder
	
FunctionEnd

Function .onVerifyInstDir

    #the install folder must exist
    IfFileExists $INSTDIR PathGood
      Abort ; if $INSTDIR is not a winamp directory, don't let us install there
    PathGood:
FunctionEnd

;##########################################################################################################################################################
;##########################################################################################################################################################
;##########################################################################################################################################################

Section "-OpenViBE"

    strcpy $OVInstFolder "$INSTDIR"
	; ${If} $OLDINSTDIR != ""
		; RMDir /r $OLDINSTDIR
		; RMDir /r "$SMPROGRAMS\OpenViBE"
	; ${EndIf}


	; WriteRegStr HKLM "SOFTWARE\openvibe" "InstallDir" "$INSTDIR"
	WriteUninstaller UninstallGipsaOVExtensions.exe

	
	; CreateDirectory "$INSTDIR\dependencies\arch"
	; StrCpy $DIRECTX_MISSING "false"

	; SetOutPath "$INSTDIR\dependencies"
	; IfFileExists "$SYSDIR\d3dx9_42.dll" no_need_to_install_directx
	; NSISdl::download "http://www.microsoft.com/downloads/info.aspx?na=90&p=&SrcDisplayLang=en&SrcCategoryId=&SrcFamilyId=04ac064b-00d1-474e-b7b1-442d8712d553&u=http%3a%2f%2fdownload.microsoft.com%2fdownload%2fB%2f7%2f9%2fB79FC9D7-47B8-48B7-A75E-101DEBEB5AB4%2fdirectx_aug2009_redist.exe" "arch\openvibe-directx.exe"
	; Pop $R0 ; Get the return value
		; StrCmp $R0 "success" +4
			; MessageBox MB_OK "Download failed: $R0$\nCheck your Internet connection and your firewall settings.$\nDirect X won't be installed and 3D functionalities won't be available...$\nYou can install DirectX later to enable 3D functionalities !" /SD IDOK
			; StrCpy $DIRECTX_MISSING "true"
			; Goto no_need_to_install_directx ; Quit
	; ExecWait '"arch\openvibe-directx.exe" /T:"$INSTDIR\tmp" /Q'
	; ExecWait '"$INSTDIR\tmp\DXSETUP.exe" /silent'
	
;no_need_to_install_directx:

    SetOverwrite on

	SetOutPath "$OVInstFolder\bin"
	
	File .\bin\OpenViBE-plugins-gipsa.dll 
;	File .\bin\openvibe-acquisition-server.exe ;disabled for now because Inria implemented our g.tec driver
	File .\bin\Brain-invaders-dynamic.exe
	File .\bin\inpout32.dll
	File .\bin\brain-invaders-launcher.exe
	File .\bin\openvibe-tagging-wrapper.dll
	
;	Bayes Point Machine classifier 
;   SetOutPath "$OVInstFolder\bin"
;	File .\bin\bpm\BayesPointMachineClassifier.dll
;	File .\bin\bpm\Infer.Compiler.dll
;	File .\bin\bpm\Infer.Runtime.dll
 
	;ms dlls
;	File .\bin\msdlls\msvcp100d.dll
;	File .\bin\msdlls\msvcr100d.dll
	
;   LSL used for streaming data from OpenVibe
;    File .\bin\lsl\release\lsl.dll
	
;	SetOutPath "$OVInstFolder\share\openvibe-applications\acquisition-server"
;	File .\UI\interface.ui
;	File .\UI\interface-TobiA.ui

    SetOutPath "$OVInstFolder"
    File .\scripts\ov-brain-invaders.cmd
	File .\scripts\ov-brain-invaders-launcher.cmd
	
	SetOutPath "$OVInstFolder\share\"
    File .\docs\BI_instructions.pdf ;Documents - such as the play instructions for Brain Invaders
	File .\docs\BI_quick_start_guide.pdf
	File .\config\brain-invaders.conf
	File .\docs\BI_multi-player.pdf
	File .\config\bi_openvibe_electrodes.map
	
	SetOutPath "$OVInstFolder\share\openvibe-applications"
	File .\brain-invaders-resources.zip
	ZipDLL::extractall "brain-invaders-resources.zip" "."
	
	SetOutPath "$OVInstFolder\share\openvibe\scenarios"
	File .\gipsa-lab-scenarios.zip
	ZipDLL::extractall "gipsa-lab-scenarios.zip" "."
	
	FileOpen $0 "$OVInstFolder\share\openvibe\kernel\openvibe.conf" a
	FileSeek $0 0 END
	FileWrite $0 "$\r$\n"
	FileWrite $0 "#####################################################################################$\r$\n"
    FileWrite $0 "# Gipsa Installer$\r$\n"
    FileWrite $0 "#####################################################################################$\r$\n"
	
	;FileWrite $0 "AcquisitionServer_ExternalTriggers = true$\r$\n" ;old
	;FileWrite $0 "AcquisitionServer_ExternalStimulations = true$\r$\n" ;latest
    ;FileWrite $0 "AcquisitionServer_ExternalStimulationsQueueName = clinet_to_ov$\r$\n" ;since ST was integrated in OV and our name is different	
	;FileWrite $0 "AcquisitionServer_ExternalStimulations = true$\r$\n" ; ;users should enabled this
    FileWrite $0 "AcquisitionServer_ExternalStimulationsQueueName = clinet_to_ov$\r$\n"
	
	;FileWrite $0 "robik = $${Path_Root}/share/openvibe-scenarios/robik$\r$\n" ;not used anymore
	;FileWrite $0 "gipsa = $${Path_Root}/share/openvibe-scenarios/Gipsa-lab$\r$\n" ;not used anymore
	FileClose $0
	
	;Icons
	SetOutPath "$OVInstFolder\share\openvibe-applications\brain-invaders"
	File .\UI\Brain_Invaders_BCI.ico
	File .\UI\Brain_Ivaders_Launcher.ico
	
	;Python
	SetOutPath "$OVInstFolder\dependencies"
	
	File .\python-2.7.3.msi ; added for Python offline
			
	ReadRegStr $PythonFolder HKLM SOFTWARE\Python\PythonCore\2.7\InstallPath ""
	
	${If} $PythonFolder == "" 
	    ReadRegStr $PythonFolder HKLM SOFTWARE\Wow6432Node\Python\PythonCore\2.7\InstallPath ""
	${EndIf}
	
    ${If} $PythonFolder != "" 
		IfFileExists "$PythonFolder\*.*" python_installation_skipped 
	${EndIf}
	
    IfFileExists "c:\Python27\*.*" python_installation_skipped +1

	MessageBox MB_YESNO "Python 2.7 32 bit is required by OpenVibe Gipsa-lab Extensions. Python 2.7 was not found on your system. Do you wish to install it?" IDYES +1 IDNO python_installation_skipped

	; NSISdl::download "http://www.python.org/ftp/python/2.7.3/python-2.7.3.msi" "$OVInstFolder\dependencies\python-2.7.3.msi"
	; Pop $R0 ; Get the return value
		; StrCmp $R0 "success" +3
			; MessageBox MB_OK "Python download failed: $R0$\nCheck your Internet connection and your firewall settings.$\n!" /SD IDOK
			; Goto python_installation_skipped ; Quit
			
	ExecWait '"msiexec" /i "$OVInstFolder\dependencies\python-2.7.3.msi" /qn ALLUSERS=1'

 python_installation_skipped:

    ;Numpy
	MessageBox MB_YESNO "Numpy 1.6.2 is required by OpenVibe Gipsa-lab Extensions. Do you wish to install it now? This will start Numpy graphical installer." IDYES +1 IDNO numpy_installation_skipped

	SetOutPath "$OVInstFolder\dependencies\"
	File .\numpy-1.6.2-win32-superpack-python2.7.exe
	ExecWait 'numpy-1.6.2-win32-superpack-python2.7.exe'

numpy_installation_skipped:	
    ; SetOutPath "$OVInstFolder\dependencies"
	; NSISdl::download "http://downloads.sourceforge.net/project/numpy/NumPy/1.6.2/numpy-1.6.2-win32-superpack-python2.7.exe?r=&ts=1349882990&use_mirror=switch" "$OVInstFolder\dependencies\numpy-1.6.2-win32-superpack-python2.7.exe"
	; Pop $R0 ; Get the return value
		; StrCmp $R0 "success" +3
			; MessageBox MB_OK "Numpy download failed: $R0$\nCheck your Internet connection and your firewall settings.$\n!" /SD IDOK
			; Goto no_need_to_install_numpy ; Quit
	; ExecWait '"msiexec" /i "$OVInstFolder\dependencies\numpy-1.6.2-win32-superpack-python2.7.exe" /qn'

; no_need_to_install_numpy:
     





	; File ..\dependencies\arch\openvibe-vcredist-2008-sp1.exe
	; File ..\dependencies\arch\openvibe-vcredist-2010.exe
	; File ..\dependencies\arch\lua-5.1.4-30.zip
	; File ..\dependencies\arch\expat-2.0.1.zip
	; File ..\dependencies\arch\gtk-2.22.1-runtime.zip
	; ;File ..\dependencies\arch\gtk-themes-2009.09.07.zip
	; File ..\dependencies\arch\itpp-4.0.7-runtime.zip
	; File ..\dependencies\arch\ogre-1.7.1-vs100-runtime.zip
	; File ..\dependencies\arch\cegui-0.7.2-vs100-runtime.zip
	; File ..\dependencies\arch\vrpn-7.26-runtime.zip
	; File ..\dependencies\arch\pthreads-2.8.0-runtime.zip
  ; File ..\dependencies\arch\openal-1.1-runtime.zip
  ; File ..\dependencies\arch\freealut-1.1.0-bin-runtime.zip
  ; File ..\dependencies\arch\libvorbis-1.3.2-vs100-runtime.zip
  ; File ..\dependencies\arch\libogg-1.2.1-vs100-runtime.zip

	; SetOutPath "$INSTDIR\dependencies"
	; ExecWait '"arch\openvibe-vcredist-2005-sp1.exe" /q'
	; ExecWait '"arch\openvibe-vcredist-2008-sp1.exe" /q'
	; ExecWait '"arch\openvibe-vcredist-2010.exe" /q'

	; SetOutPath "$INSTDIR\dependencies"
	; ZipDLL::extractall "arch\lua-5.1.4-30.zip" "lua"
	; ZipDLL::extractall "arch\expat-2.0.1.zip" "expat"
	; ZipDLL::extractall "arch\gtk-2.22.1-runtime.zip" "gtk"
	; ZipDLL::extractall "arch\gtk-themes-2009.09.07.zip" "gtk"
	; ZipDLL::extractall "arch\itpp-4.0.7-runtime.zip" "itpp"
	; ZipDLL::extractall "arch\ogre-1.7.1-vs100-runtime.zip" "ogre"
	; ZipDLL::extractall "arch\cegui-0.7.2-vs100-runtime.zip" "cegui"
	; ZipDLL::extractall "arch\vrpn-7.26-runtime.zip" "vrpn"
	; ZipDLL::extractall "arch\pthreads-2.8.0-runtime.zip" "pthreads"
	; ZipDLL::extractall "arch\openal-1.1-runtime.zip" "openal"
	; ZipDLL::extractall "arch\freealut-1.1.0-bin-runtime.zip" "freealut"
	; ZipDLL::extractall "arch\libvorbis-1.3.2-vs100-runtime.zip" "libvorbis"
	; ZipDLL::extractall "arch\libogg-1.2.1-vs100-runtime.zip" "libogg"

	; SetOutPath "$INSTDIR"
	; File /nonfatal /r ..\dist\bin
	; ; File /nonfatal /r ..\dist\doc
	; ; File /nonfatal /r ..\dist\etc
	; ; File /nonfatal /r ..\dist\include
	; ; File /nonfatal /r ..\dist\lib
	; File /nonfatal /r ..\dist\log
	; File /nonfatal /r ..\dist\share
	; ; File /nonfatal /r ..\dist\tmp

	; StrCmp $DIRECTX_MISSING "false" no_need_to_patch_3d_functionnality
	; FileOpen $0 "$INSTDIR\share\openvibe.conf" a
	; FileSeek $0 0 END

	; CreateDirectory "$SMPROGRAMS\OpenViBE"
	; CreateDirectory "$SMPROGRAMS\OpenViBE\Developer tools"
	; CreateShortCut "$SMPROGRAMS\OpenViBE\Developer tools\openvibe id generator.lnk"       "$INSTDIR\openvibe-id-generator.cmd"        "" "%SystemRoot%\system32\shell32.dll" 57
	; CreateShortCut "$SMPROGRAMS\OpenViBE\Developer tools\openvibe plugin inspector.lnk"   "$INSTDIR\openvibe-plugin-inspector.cmd"    "" "%SystemRoot%\system32\shell32.dll" 57
	; CreateShortCut "$SMPROGRAMS\OpenViBE\Developer tools\openvibe skeleton generator.lnk" "$INSTDIR\openvibe-skeleton-generator.cmd"  "" "%SystemRoot%\system32\shell32.dll" 57
	; CreateShortCut "$SMPROGRAMS\OpenViBE\openvibe designer.lnk"                           "$INSTDIR\openvibe-designer.cmd"            "" "%SystemRoot%\system32\shell32.dll" 137
	; CreateShortCut "$SMPROGRAMS\OpenViBE\openvibe acquisition server.lnk"                 "$INSTDIR\openvibe-acquisition-server.cmd"  "" "%SystemRoot%\system32\shell32.dll" 18
	; CreateShortCut "$SMPROGRAMS\OpenViBE\openvibe vr-demo tie-fighter.lnk"                "$INSTDIR\openvibe-vr-demo-tie-fighter.cmd" "" "%SystemRoot%\system32\shell32.dll" 200
	
	SetOutPath "$OVInstFolder"
	CreateDirectory "$SMPROGRAMS\Gipsa-lab"
	CreateShortCut "$SMPROGRAMS\Gipsa-lab\Brain Invaders.lnk"                   "$OVInstFolder\ov-brain-invaders.cmd"    "" "$OVInstFolder\share\openvibe-applications\brain-invaders\Brain_Invaders_BCI.ico" 
	CreateShortCut "$SMPROGRAMS\Gipsa-lab\Brain Invaders Launcher.lnk"          "$OVInstFolder\ov-brain-invaders-launcher.cmd"    "" "$OVInstFolder\share\openvibe-applications\brain-invaders\Brain_Ivaders_Launcher.ico"
	CreateShortCut "$SMPROGRAMS\Gipsa-lab\Brain Invaders Play Instructions (French).lnk"          "$OVInstFolder\share\BI_instructions.pdf"    "" ""
	CreateShortCut "$SMPROGRAMS\Gipsa-lab\Brain Invaders Electrode Montage.lnk"              "$OVInstFolder\share\openvibe\scenarios\gipsa-lab\MDM\P300\Montage.png"    "" ""
	CreateShortCut "$SMPROGRAMS\Gipsa-lab\Brain Invaders Quick Start Guide.lnk"              "$OVInstFolder\share\BI_quick_start_guide.pdf"    "" ""
	CreateShortCut "$SMPROGRAMS\Gipsa-lab\Brain Invaders Multi-player Guide.lnk"              "$OVInstFolder\share\BI_multi-player.pdf"    "" ""
	
	; CreateShortCut "$SMPROGRAMS\OpenViBE\uninstall.lnk"                                   "$INSTDIR\Uninstall.exe"

	AccessControl::GrantOnFile "$OVInstFolder\share\openvibe-applications\brain-invaders" "(BU)" "FullAccess"
	AccessControl::GrantOnFile "$OVInstFolder\share\openvibe-scenarios" "(BU)" "FullAccess"
SectionEnd

Section "Uninstall"

	;RMDir /r $INSTDIR
	;RMDir /r "$SMPROGRAMS\Gipsa-lab"

SectionEnd
