/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCOutputChannel.h"

#include <iostream>
#include <algorithm>    // std::max
#include <system/Memory.h>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::Streaming;


boolean COutputChannel::initialize(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm>* pTBoxAlgorithm)
{
	m_pTBoxAlgorithm            = pTBoxAlgorithm;
	m_bIsWorking                = false;

	//init output signal channel 1
	m_pStreamEncoderSignal      = &m_pTBoxAlgorithm->getAlgorithmManager().getAlgorithm(m_pTBoxAlgorithm->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalStreamEncoder));
	m_pStreamEncoderSignal->initialize();
	op_pMemoryBufferSignal.initialize(m_pStreamEncoderSignal->getOutputParameter(OVP_GD_Algorithm_SignalStreamEncoder_OutputParameterId_EncodedMemoryBuffer));
	ip_pMatrixSignal.initialize(m_pStreamEncoderSignal->getInputParameter(OVP_GD_Algorithm_SignalStreamEncoder_InputParameterId_Matrix));
	ip_ui64SamplingRateSignal.initialize(m_pStreamEncoderSignal->getInputParameter(OVP_GD_Algorithm_SignalStreamEncoder_InputParameterId_SamplingRate));

	//init output stimulation channel 1
	m_pStreamEncoderStimulation = &m_pTBoxAlgorithm->getAlgorithmManager().getAlgorithm(m_pTBoxAlgorithm->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamEncoder));
	m_pStreamEncoderStimulation->initialize();
	op_pMemoryBufferStimulation.initialize(m_pStreamEncoderStimulation->getOutputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_OutputParameterId_EncodedMemoryBuffer));
	ip_pStimulationSetStimulation.initialize(m_pStreamEncoderStimulation->getInputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_InputParameterId_StimulationSet));
	
	return true;
}

boolean COutputChannel::uninitialize()
{	
	ip_ui64SamplingRateSignal.uninitialize();
	ip_pMatrixSignal.uninitialize();
	op_pMemoryBufferSignal.uninitialize();
	m_pStreamEncoderSignal->uninitialize();
	m_pTBoxAlgorithm->getAlgorithmManager().releaseAlgorithm(*m_pStreamEncoderSignal);

	op_pMemoryBufferStimulation.uninitialize();
	ip_pStimulationSetStimulation.uninitialize();
	m_pStreamEncoderStimulation->uninitialize();
	m_pTBoxAlgorithm->getAlgorithmManager().releaseAlgorithm(*m_pStreamEncoderStimulation);

	return true;
}

void COutputChannel::sendHeader(const CInputChannel& inputChannel1, const CInputChannel& inputChannel2)
{
	m_ui32CurrentSamples   = 0;
	m_ui32ProcessedSamples = 0;
	m_ui64InterpolateIndex = 0;
	m_ui64InterpolateShift = 0;

	m_ui64SamplingRate1    = inputChannel1.getSamplingRate();
	m_ui64NbOfChannels1    = inputChannel1.getNbOfChannels();
	m_ui64NbOfSamples1     = inputChannel1.getNbOfSamples();
	m_ui64SamplingRate2    = inputChannel2.getSamplingRate();
	m_ui64NbOfChannels2    = inputChannel2.getNbOfChannels();
	m_ui64NbOfSamples2     = inputChannel2.getNbOfSamples();
	m_ui64NbOfFullSamples2 = std::max(m_ui64SamplingRate2, m_ui64SamplingRate1);

	IBoxIO& l_rDynamicBoxContext=m_pTBoxAlgorithm->getDynamicBoxContext();

	m_oMatrixWork.resize(size_t(m_ui64NbOfChannels2*m_ui64NbOfFullSamples2));

	op_pMemoryBufferSignal    = l_rDynamicBoxContext.getOutputChunk(SIGNAL_CHANNEL);
	OpenViBEToolkit::Tools::Matrix::copyDescription(*ip_pMatrixSignal, *inputChannel1.getOpMatrix());
	ip_pMatrixSignal->setDimensionSize(0, OpenViBE::uint32(m_ui64NbOfChannels1 + m_ui64NbOfChannels2));

	//copy channel names
	for (int i = 0; i< inputChannel1.getOpMatrix()->getDimensionSize(0); i++)
		ip_pMatrixSignal->setDimensionLabel(0, i , inputChannel1.getOpMatrix()->getDimensionLabel(0,i));

	for (int i = 0; i< inputChannel2.getOpMatrix()->getDimensionSize(0); i++)
		ip_pMatrixSignal->setDimensionLabel(0, inputChannel1.getOpMatrix()->getDimensionSize(0) + i , inputChannel2.getOpMatrix()->getDimensionLabel(0,i));

	ip_ui64SamplingRateSignal = m_ui64SamplingRate1;
	
	m_pStreamEncoderSignal->process(OVP_GD_Algorithm_SignalStreamEncoder_InputTriggerId_EncodeHeader);
	l_rDynamicBoxContext.markOutputAsReadyToSend(SIGNAL_CHANNEL, inputChannel1.getStartTimestamp(), inputChannel1.getEndTimestamp());
}

void COutputChannel::sendStimulation(IStimulationSet* stimset, OpenViBE::uint64 startTimestamp, OpenViBE::uint64 endTimestamp)
{
	IBoxIO& l_rDynamicBoxContext   = m_pTBoxAlgorithm->getDynamicBoxContext();

	ip_pStimulationSetStimulation  = stimset;
	op_pMemoryBufferStimulation    = l_rDynamicBoxContext.getOutputChunk(STIMULATION_CHANNEL);
	m_pStreamEncoderStimulation->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
	l_rDynamicBoxContext.markOutputAsReadyToSend(STIMULATION_CHANNEL, startTimestamp, endTimestamp);
}

// put the data from the first channel (reference channel) in the output buffer
// if  the working channel still contains some samples, process them immediately

void COutputChannel::prepareBlock1(OpenViBE::float64* pSrc, OpenViBE::uint64 startTimestamp, OpenViBE::uint64 endTimestamp)
{
	m_bIsWorking                   = true;
	m_ui64StartTimestamp           = startTimestamp;
	m_ui64EndTimestamp             = endTimestamp;

    System::Memory::copy(bufferBlockDst1(), pSrc, sizeofBlockDst1());

	processBlock2(); // if  the working channel still contains some samples, process them immediately
}

// append the data from the second channel to the working temporary buffer
// process it immediately
void COutputChannel::prepareBlock2(OpenViBE::float64* pSrc)
{
	if(!appendBlock2(pSrc))
		return;

	processBlock2();
}

// process the working temporary buffer in 3 steps
// - interpolation
// - send the data if the output buffer is full
// - discard the processed samples from the working buffer 

void COutputChannel::processBlock2()
{
	interpolateBlock2();
	
	if(sendSignal())
		shiftBlock2();
}

// append the data from the second channel to the working temporary buffer
bool COutputChannel::appendBlock2(OpenViBE::float64* pSrc)
{
	if((m_ui32CurrentSamples + m_ui64NbOfSamples2) >= m_ui64NbOfFullSamples2)
	{	m_pTBoxAlgorithm->getLogManager() << OpenViBE::Kernel::LogLevel_Fatal << "Problem with number of samples!\n";
		return false;
	}

	for(uint64 ii=0; ii < m_ui64NbOfChannels2; ii++, pSrc+=m_ui64NbOfSamples2)
	{	OpenViBE::float64* pDst	= bufferBlockSrc(ii, m_ui32CurrentSamples);
		System::Memory::copy(pDst, pSrc, sizeofBlock(m_ui64NbOfSamples2));
	}

	m_ui32CurrentSamples		+= OpenViBE::uint32(m_ui64NbOfSamples2);
	m_ui32CurrentSamplesLim		 = m_ui32CurrentSamples - 1;

	return true;
}

// linear interpolation of the data stored in the working temporary buffer
void COutputChannel::interpolateBlock2()
{
	while(m_ui32CurrentSamples)
	{	if(m_ui32ProcessedSamples == m_ui64NbOfSamples1)
			break; // stops the interpolation process if the output buffer is full
		
		OpenViBE::float64	float64Pos   = (OpenViBE::float64(m_ui64InterpolateIndex)*OpenViBE::float64(m_ui64SamplingRate2))/OpenViBE::float64(m_ui64SamplingRate1);
		OpenViBE::uint64	uint64Pos    = OpenViBE::uint32(float64Pos) - m_ui64InterpolateShift;

		if(uint64Pos >= m_ui32CurrentSamplesLim)
			break; // stops the interpolation process if the working buffer is empty
		
		OpenViBE::float64	float64D     = float64Pos - OpenViBE::float64(OpenViBE::uint32(float64Pos));
		OpenViBE::float64	float64D_1   = 1.0 - float64D;

		for(uint64 ii=0; ii < m_ui64NbOfChannels2; ii++)
		{	
			OpenViBE::float64* pSrc = bufferBlockSrc(ii, uint64Pos);
			OpenViBE::float64* pDst = bufferBlockDst2(ii, m_ui32ProcessedSamples);
		
			OpenViBE::float64 v1  = *pSrc;
			OpenViBE::float64 v2  = *(pSrc + 1);
			OpenViBE::float64 v   = v1*float64D_1 + v2*float64D;
			*pDst                 = v;
		}

		m_ui64InterpolateIndex++;
		m_ui32ProcessedSamples++;
	}
}

// send the output buffer if it is full
OpenViBE::boolean COutputChannel::sendSignal()
{
	if(m_ui32ProcessedSamples != m_ui64NbOfSamples1)
		return false;

	m_bIsWorking                   = false;
	m_ui32ProcessedSamples         = 0;
	IBoxIO& l_rDynamicBoxContext   = m_pTBoxAlgorithm->getDynamicBoxContext();

	op_pMemoryBufferSignal         = l_rDynamicBoxContext.getOutputChunk(SIGNAL_CHANNEL);
	m_pStreamEncoderSignal->process(OVP_GD_Algorithm_SignalStreamEncoder_InputTriggerId_EncodeBuffer);
	l_rDynamicBoxContext.markOutputAsReadyToSend(SIGNAL_CHANNEL, m_ui64StartTimestamp, m_ui64EndTimestamp);

	return true;
}

// - discard the processed samples from the working buffer by left shifting 
void COutputChannel::shiftBlock2()
{
	OpenViBE::float64	float64Pos   = (OpenViBE::float64(m_ui64InterpolateIndex) * OpenViBE::float64(m_ui64SamplingRate2))/OpenViBE::float64(m_ui64SamplingRate1);
	OpenViBE::uint64	uint64Pos    = OpenViBE::uint32(float64Pos) - m_ui64InterpolateShift;

	if(uint64Pos >= m_ui32CurrentSamples)
		uint64Pos = m_ui32CurrentSamples;

	if(m_ui32CurrentSamples - uint64Pos)
	{	
		for(uint64 ii=0; ii < m_ui64NbOfChannels2; ii++)
		{	
			OpenViBE::float64* pSrc	= bufferBlockSrc(ii, uint64Pos);
			OpenViBE::float64* pDst	= bufferBlockSrc(ii);
			System::Memory::copy(pDst, pSrc, sizeofBlock(m_ui32CurrentSamples - uint64Pos));
	    }	
	}

	m_ui64InterpolateShift     += OpenViBE::uint32(uint64Pos);
	m_ui32CurrentSamples	   -= OpenViBE::uint32(uint64Pos);
	m_ui32CurrentSamplesLim		= m_ui32CurrentSamples - 1;
}
