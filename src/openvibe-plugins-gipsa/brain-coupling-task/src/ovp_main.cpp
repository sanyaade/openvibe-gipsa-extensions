#include "ovp_defines.h"

//GIPSA plugins
#include "ovpCBoxAlgorithmPP2Stim.h"
#include "ovpCBoxAlgorithmSignal2Stim.h"
#include "ovpCBoxStimulationTransformer.h"
#include "ovpCBoxAlgorithmStreamedMatrixMerger.h"
#include "ovpCBoxUpsample.h"

#include "ovpCBoxTagger.h"
#include "ovpCBoxAlgorithmP300Tagger.h"
#include "ovpCBoxAlgorithmTrainMDM.h"

#if defined TARGET_OS_Windows
#include "ovpCAlgorithmClassifierBayesPointMachine.h"
#endif

//#ifndef _DEBUG 
#include "ovpCBoxAlgorithmXDAWN2SpatialFilterTrainer.h"
#include "ovpCBoxRiemannPotato.h"
//#endif

#include "ovpCBoxAlgorithmProcessMDM.h"
#include "ovpCBoxAlgorithmStreamMergerWithResampling.h"
#include "ovpCBoxAlgorithmBrainampFileWriter.h"
#include "ovpCBoxLSLExport.h"
#include "ovpCBoxAlgorithmPulsarTagging.h"
//next GIPSA include to your new box

OVP_Declare_Begin()

	//Register dropdowns
	rPluginModuleContext.getTypeManager().registerEnumerationType (OVP_TypeId_BinaryFormat, "Binary format select");
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_BinaryFormat, "INT_16",            OVP_TypeId_BinaryFormat_int16.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_BinaryFormat, "UINT_16",           OVP_TypeId_BinaryFormat_uint16.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_BinaryFormat, "IEEE_FLOAT_32",     OVP_TypeId_BinaryFormat_float32.toUInteger());

	//Register boxes
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmPP2StimDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmSignal2StimDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxStimulationTransformerDesc);
	OVP_Declare_New(OpenViBEPlugins::Streaming::CBoxAlgorithmStreamedMatrixMergerDesc);

	#if defined TARGET_OS_Windows
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxTaggerDesc);
	OVP_Declare_New(OpenViBEPlugins::SimpleVisualisation::CBoxAlgorithmP300TaggerDesc)
    #endif
	
    //#ifndef _DEBUG 
	OVP_Declare_New(OpenViBEPlugins::SignalProcessingGpl::CBoxAlgorithmXDAWN2SpatialFilterTrainerDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessingGpl::CBoxAlgorithmRiemannPotatoDesc);
    //#endif
	
	#if defined TARGET_OS_Windows
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(
        OVTK_TypeId_ClassificationAlgorithm,
        "Bayes Point Machine (BPM)",
        OVP_ClassId_Algorithm_BayesPointMachineClassifier.toUInteger());

    OVP_Declare_New(OpenViBEPlugins::Local::CAlgorithmClassifierBayesPointMachineDesc);
	#endif
	
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmUpsampleDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmTrainMDMDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmProcessMDMDesc);
	OVP_Declare_New(OpenViBEPlugins::Streaming::CBoxAlgorithmStreamMergerWithResamplingDesc);
	OVP_Declare_New(OpenViBEPlugins::FileIO::CBoxAlgorithmBrainampFileWriterDesc);
	OVP_Declare_New(OpenViBEPlugins::Streaming::CBoxAlgorithmCBoxAlgorithmLSLExportDesc);
	OVP_Declare_New(OpenViBEPlugins::SignalProcessing::CBoxAlgorithmPulsarTaggingDesc);
	//next GIPSA OVP_Declare_New

OVP_Declare_End()
