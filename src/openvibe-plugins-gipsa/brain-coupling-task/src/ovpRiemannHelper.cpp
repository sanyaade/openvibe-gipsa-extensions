/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include <math.h>
#include <numeric> 

#include <itpp/base/algebra/eigen.h>
#include <itpp/base/algebra/inv.h>
#include <itpp/base/converters.h>
#include <itpp/stat/misc_stat.h>
#include <itpp/itsignal.h>

#include "ovpRiemannHelper.h"
#include <iostream>
using namespace std;
using namespace itpp;

namespace Riemann
{
  // matrix pseudo power
  itpp::mat mpower(itpp::mat A,float y)
  {

    mat U;
    vec s;

    itpp::eig_sym(A,s,U);
    int n = itpp::length(s);
    mat Out = itpp::zeros(n,n);
    
    for(int i=0;i<n;i++){
        if(s[i]>0) {
          vec u = U.get_col(i);
          Out = Out + outer_product(u,u)*pow(s[i],(double)y);
        }
    }
    return Out;

  }
  
  //matrix pseudo square root
  itpp::mat sqrtm(itpp::mat A)
  {
    
    mat U;
    vec s;

    itpp::eig_sym(A,s,U);
    int n = itpp::length(s);
    mat Out = itpp::zeros(n,n);

    for(int i=0;i<n;i++){
        if(s[i]>0) {
          vec u = U.get_col(i);
          Out = Out + outer_product(u,u)*sqrt(s[i]);
        }
    }
    return Out;
    
  }

  // matrix pseudo inverse square root
  itpp::mat invsqrtm(itpp::mat A)
  {
    
    mat U;
    vec s;

    itpp::eig_sym(A,s,U);
    int n = itpp::length(s);
    itpp::mat Out = itpp::zeros(n,n);

    for(int i=0;i<n;i++){
        if(s[i]>0) {
          vec u = U.get_col(i);
          Out = Out + outer_product(u,u)*(1.0/sqrt(s[i]));
        }
    }
    return Out;
    
  }
  
  // matrix exponantial
  itpp::mat expm(itpp::mat A)
  {
    
    mat U;
    vec s;

    itpp::eig_sym(A,s,U);
    int n = itpp::length(s);
    itpp::mat Out = itpp::zeros(n,n);
	  itpp::vec es = itpp::exp(s);

    for(int i=0;i<n;i++)
	{
      vec u = U.get_col(i);
      Out = Out + outer_product(u,u) * es(i);
    }
    return Out;
    
  }
  
  // matrix pseudo logarithm
  itpp::mat logm(itpp::mat A)
  {
    
    mat U;
    vec s;

    itpp::eig_sym(A,s,U);
    int n = itpp::length(s);
    itpp::mat Out = itpp::zeros(n,n);
    itpp::vec ls = itpp::log(s);

    for(int i=0;i<n;i++)
	{
      if(s[i]>0) 
	  {
        vec u = U.get_col(i);
        Out = Out + outer_product(u,u) * ls(i);
      }
    }
    return Out;
    
  }

  // Riemannian distance
  float distance(itpp::mat A,itpp::mat B)
  {
    itpp::mat P1 = Riemann::invsqrtm(A);
    itpp::mat P = P1 * B * P1;

    itpp::vec landa = itpp::eig_sym(P);	

    float d = 0;
    for(int i=0;i<length(landa);i++) {
      if(landa[i]>0){
        d += sqr(log(landa[i]));
      }
    }
    
    d = sqrt(d);

    return d;
  }
  
  // Riemannian geodesic
  itpp::mat geodesic(itpp::mat A,itpp::mat B, float alpha)
  {
    itpp::mat Asq = Riemann::sqrtm(A);
    itpp::mat Aisq =  Riemann::invsqrtm(A);

    itpp::mat C = Aisq * B * Aisq;
    itpp::mat D = Riemann::mpower(C, (1/alpha));

    itpp::mat Out = Asq * D * Asq;
    return Out;
  }
  
  // Riemannian mean
  itpp::mat mean(std::vector<itpp::mat> vMatSet)
  {
    int Nmat = vMatSet.size();
    int nIter = 0;
    int matDim = vMatSet.begin()->rows();
    std::vector<itpp::mat>::iterator it;
    itpp::mat J = itpp::eye(matDim);
    
    // Init with Euclidean mean
    itpp::mat C = std::accumulate( vMatSet.begin(), vMatSet.end(), itpp::zeros(matDim,matDim));
    C = C/Nmat;

    // iterate 
    while((itpp::norm(J,"fro")>(0.00001)) && (nIter<50))
    {
      nIter++;
      J = itpp::zeros(matDim,matDim);
      itpp::mat Cm12 = Riemann::invsqrtm(C);
      itpp::mat C12  = Riemann::sqrtm(C);

      for(it=vMatSet.begin(); it!=vMatSet.end(); it++)
      {
         J = J + Riemann::logm(Cm12 * *it * Cm12);
         
      }

      J = J/Nmat;
      C = C12 * Riemann::expm(J) * C12; 
    }
    return C;
  }

};
