#ifndef __ov_concurrency_H__
#define __ov_concurrency_H__

//For TBB:
//debian: apt-get install libtbb-dev
//you must manually configure tbb on windows and linux and set HAS_TBB

//both Intel TBB and Microsoft TPP provide the same core functionality
#if defined(HAS_TBB)
    #define HAS_CONCURRENCY
	#include <tbb/tbb.h>
    #include <tbb/parallel_for.h>
	#include <tbb/concurrent_vector.h>
	using namespace tbb;
	#define vector_type concurrent_vector
	#pragma message("TBB set as concurrency library")

#elif (defined(TARGET_OS_Windows) && _MSC_VER >= 1600) //VS 2010 and up
    #define HAS_CONCURRENCY
	#include <ppl.h>
	#include <concurrent_vector.h>
	using namespace Concurrency; //after VS 2010 it is with small letter: concurrency
	#define vector_type concurrent_vector
    #pragma message("PPT set as concurrency library")
#else
    #define vector_type std::vector
#endif

#endif
