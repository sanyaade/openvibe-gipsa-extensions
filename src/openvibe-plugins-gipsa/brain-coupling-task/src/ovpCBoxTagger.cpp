/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCBoxTagger.h"

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessing;

#include <iostream>

namespace
{
	class _AutoCast_
	{
	public:
		
		_AutoCast_(IBox& rBox, IConfigurationManager& rConfigurationManager, const uint32 ui32Index) : m_rConfigurationManager(rConfigurationManager) { rBox.getSettingValue(ui32Index, m_sSettingValue); }
		operator uint64 (void) { return m_rConfigurationManager.expandAsUInteger(m_sSettingValue); }
		operator int64 (void) { return m_rConfigurationManager.expandAsInteger(m_sSettingValue); }
		operator float64 (void) { return m_rConfigurationManager.expandAsFloat(m_sSettingValue); }
		operator OpenViBE::boolean (void) { return m_rConfigurationManager.expandAsBoolean(m_sSettingValue); }
		operator const CString (void) { return m_sSettingValue; }

	protected:
		IConfigurationManager& m_rConfigurationManager;
		CString m_sSettingValue;
	};
};

OpenViBE::boolean CBoxTagger::initialize(void)
{
	IBox& l_rStaticBoxContext=this->getStaticBoxContext();

	commHardwareT = new OVComm(SerialPort,"COM5"); //select OpenVibe communication

	if (commHardwareT->lLastError!=1)
	{
		this->getLogManager() << LogLevel_Error << "Error with serial port!";
		return false;
	}
	//commHardwareT = new OVComm(SerialPort);      //OpenVibe external communicator
	//commSoftwareT = new OVComm(SoftwareTagging,""); //OpenVibe external communicator
	commSoftwareT = new OVComm(Robik,""); //OpenVibe external communicator


	//clear values
	commHardwareT->SetFinalized();
	commSoftwareT->SetFinalized();

	m_uint64StimCode1  = FSettingValueAutoCast(*this->getBoxAlgorithmContext(),0);
	m_uint64StimCode2  = FSettingValueAutoCast(*this->getBoxAlgorithmContext(),1);
	m_uint64rawValue     = FSettingValueAutoCast(*this->getBoxAlgorithmContext(),2);

	//> init input stimulation
	m_pStimulationDecoderTrigger=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
    m_pStimulationDecoderTrigger->initialize();
    ip_pMemoryBufferToDecodeTrigger.initialize(m_pStimulationDecoderTrigger->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
    op_pStimulationSetTrigger.initialize(m_pStimulationDecoderTrigger->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));

	return true;
}

OpenViBE::boolean CBoxTagger::uninitialize(void)
{
	//clear values
	commHardwareT->SetFinalized();
	commSoftwareT->SetFinalized();

	delete commHardwareT;
	delete commSoftwareT;

	// uninit input stimulation
	m_pStimulationDecoderTrigger->uninitialize();
    ip_pMemoryBufferToDecodeTrigger.uninitialize();
	op_pStimulationSetTrigger.uninitialize();
    this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoderTrigger);

	return true;
}

OpenViBE::boolean CBoxTagger::processInput(uint32 ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

OpenViBE::boolean CBoxTagger::process(void)
{
	//> Get dynamic box context
	IBoxIO* l_pDynamicBoxContext=getBoxAlgorithmContext()->getDynamicBoxContext();

	for(uint32 j=0; j<l_pDynamicBoxContext->getInputChunkCount(0); j++)
	{
		ip_pMemoryBufferToDecodeTrigger=l_pDynamicBoxContext->getInputChunk(0, j);
		m_pStimulationDecoderTrigger->process();

		if(m_pStimulationDecoderTrigger->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
		  {
			uint64 l_ui64ChunkStartTime=l_pDynamicBoxContext->getInputChunkStartTime(0, j);
			uint64 l_ui64ChunkEndTime=l_pDynamicBoxContext->getInputChunkEndTime(0, j);

			for(uint32 stim=0; stim<op_pStimulationSetTrigger->getStimulationCount(); stim++)
			{
				
					OpenViBE::uint64 l_ui64CurrentIdentifier = op_pStimulationSetTrigger->getStimulationIdentifier(stim);
					uint64 l_ui64sampleTime = op_pStimulationSetTrigger->getStimulationDate(stim);

					if (l_ui64CurrentIdentifier == m_uint64StimCode1) //stimulation start or rising edge (flash start)
					{
						commHardwareT->SetFlashRow(m_uint64rawValue);
						commSoftwareT->SetFlashRow(m_uint64rawValue);

					}else
					if (l_ui64CurrentIdentifier == m_uint64StimCode2) //stimulation end or falling edge (flash end)
					{
						commHardwareT->SetFinalized();
						commSoftwareT->SetFinalized();
					}
				
			}
		  }

		l_pDynamicBoxContext->markInputAsDeprecated(0,j);

	}

	return true;
}// End Process
