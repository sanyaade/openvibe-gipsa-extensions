/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEPlugins_Riemann_Helper_H__
#define __OpenViBEPlugins_Riemann_Helper_H__


#include <vector>
#include <itpp/base/mat.h>

namespace Riemann
{
	/*class Opit
	{
	  public: */

			itpp::mat mpower(itpp::mat A,float y);

			itpp::mat sqrtm (itpp::mat A);

			itpp::mat invsqrtm(itpp::mat A);

			itpp::mat expm(itpp::mat A);

			itpp::mat logm(itpp::mat A);

			float distance(itpp::mat A,itpp::mat B);

			itpp::mat geodesic(itpp::mat A,itpp::mat B, float alpha);

			itpp::mat mean(std::vector<itpp::mat> vMatSet);
	//};
};

#endif
