//#ifndef _DEBUG 

#ifndef __OpenViBEPlugins_BoxAlgorithm_XDAWN2SpatialFilterTrainer_H__
#define __OpenViBEPlugins_BoxAlgorithm_XDAWN2SpatialFilterTrainer_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#define OVP_ClassId_BoxAlgorithm_XDAWN2SpatialFilterTrainer      OpenViBE::CIdentifier(0x9D9B8EDB, 0xD63CEBFA)
#define OVP_ClassId_BoxAlgorithm_XDAWN2SpatialFilterTrainerDesc  OpenViBE::CIdentifier(0x52F556C9, 0x3C332725)

#define OVP_Algorithm_SpatialFilterMatrix_OutputParameterId         OpenViBE::CIdentifier(0x1E262366, 0x0DDD3FC4)

namespace OpenViBEPlugins
{
	namespace SignalProcessingGpl
	{
		class CBoxAlgorithmXDAWN2SpatialFilterTrainer : public OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual OpenViBE::boolean initialize(void);
			virtual OpenViBE::boolean uninitialize(void);
			virtual OpenViBE::boolean processInput(OpenViBE::uint32 ui32InputIndex);
			virtual OpenViBE::boolean process(void);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_XDAWN2SpatialFilterTrainer);

		protected:

			//Input
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalDecoder;
			
			//Output
			//stimulations
			OpenViBEToolkit::TStimulationEncoder<CBoxAlgorithmXDAWN2SpatialFilterTrainer> m_oStimulationEncoder;
			//spatial filters
			OpenViBE::CMatrix op_pMatrixToEncode;
			OpenViBE::Kernel::IAlgorithmProxy* m_pMatrixStreamEncoder;

			OpenViBE::uint64 m_ui64StimulationIdentifier;
			OpenViBE::CString m_sSpatialFilterConfigurationFilename;
			OpenViBE::uint64 m_ui64FilterDimension;
      OpenViBE::float64 m_f64EpochDuration;

			OpenViBE::boolean m_bHasSentHeader;
      
      OpenViBE::uint64 l_ui64SampleFrequency;
 			OpenViBE::Kernel::TParameterHandler < OpenViBE::uint64 > op_ui64SamplingRate;

      
		};

		class CBoxAlgorithmXDAWN2SpatialFilterTrainerDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("xDAWN 2 Spatial Filter Trainer"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Yann Renard, Anton Andreev"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("INRIA/Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("This a modified version of the \"xDAWN 2 Spatial Filter Trainer\" box by Gipsa-lab. It computes spatial filter coeffcients in order to get better evoked potential classification (typically used for P300 detection)"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-missing-image"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_XDAWN2SpatialFilterTrainer; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessingGpl::CBoxAlgorithmXDAWN2SpatialFilterTrainer; }

			virtual OpenViBE::boolean getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Stimulations", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput  ("Session signal", OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput ("Train-completed Flag", OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput ("Spatial filters", OV_TypeId_StreamedMatrix);

				//rBoxAlgorithmPrototype.addOutputParameter (OVP_Algorithm_ConfusionMatrixAlgorithm_OutputParameterId_ConfusionMatrix,           "Confusion matrix",     OpenViBE::Kernel::ParameterType_Matrix);

				rBoxAlgorithmPrototype.addSetting("Train stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Train");
				rBoxAlgorithmPrototype.addSetting("Spatial filter configuration", OV_TypeId_Filename, "");
				rBoxAlgorithmPrototype.addSetting("Filter dimension", OV_TypeId_Integer, "4");
        rBoxAlgorithmPrototype.addSetting("Epoch duration in second", OV_TypeId_Float, "0.6");

				// rBoxAlgorithmPrototype.addFlag(OpenViBE::Kernel::BoxFlag_IsUnstable);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_XDAWN2SpatialFilterTrainerDesc);
		};
	};
};

#endif // OpenViBEPlugins_BoxAlgorithm_XDAWN2SpatialFilterTrainer

//#endif
