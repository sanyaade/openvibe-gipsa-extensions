/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "ovpCBoxRiemannPotato.h"

using namespace OpenViBE;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessingGpl;

#include <system/Memory.h>

#include <iostream>
#include <math.h>




#include <iostream>//printing only
#include "ovpRiemannHelper.h"
using namespace std;

using namespace itpp;
using namespace Riemann;

namespace
{
	class _AutoCast_
	{
	public:
		_AutoCast_(IBox& rBox, IConfigurationManager& rConfigurationManager, const uint32 ui32Index) : m_rConfigurationManager(rConfigurationManager) { rBox.getSettingValue(ui32Index, m_sSettingValue); }
		operator uint64 (void) { return m_rConfigurationManager.expandAsUInteger(m_sSettingValue); }
		operator int64 (void) { return m_rConfigurationManager.expandAsInteger(m_sSettingValue); }
		operator float64 (void) { return m_rConfigurationManager.expandAsFloat(m_sSettingValue); }
		operator OpenViBE::boolean (void) { return m_rConfigurationManager.expandAsBoolean(m_sSettingValue); }
		operator const CString (void) { return m_sSettingValue; }
	protected:
		IConfigurationManager& m_rConfigurationManager;
		CString m_sSettingValue;
	};
};

OpenViBE::boolean CBoxAlgorithmRiemannPotato::initialize(void)
{
	IBox& l_rStaticBoxContext=this->getStaticBoxContext();

	//> init epoched input
	m_pSignalDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalStreamDecoder));
	m_pSignalDecoder->initialize();

	//> init input stimulation
	m_pStimulationDecoderTrigger=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
  m_pStimulationDecoderTrigger->initialize();
  ip_pMemoryBufferToDecodeTrigger.initialize(m_pStimulationDecoderTrigger->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
  op_pStimulationSetTrigger.initialize(m_pStimulationDecoderTrigger->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));

	//> init stimulation output 
	m_pStimulationEncoder1=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamEncoder));
	m_pStimulationEncoder1->initialize();
	ip_pStimulationsToEncode1.initialize(m_pStimulationEncoder1->getInputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_InputParameterId_StimulationSet));
	op_pEncodedMemoryBuffer1.initialize(m_pStimulationEncoder1->getOutputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_OutputParameterId_EncodedMemoryBuffer));

	//> load parameters
	m_ui64StartTrainingStim =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_ui64StopTrainingStim =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_ui64StartArtifactStim =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	m_ui64StopArtifactStim =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);
	m_ui64ResetStim =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 4);


	//op_ui64SamplingRateTrigger.initialize(m_pSignalDecoder->getOutputParameter(OVP_GD_Algorithm_SignalStreamDecoder_OutputParameterId_SamplingRate));
  
	m_ui64AdaptationSpeed = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 5);
	m_f64ThresholdParameter        = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 6);

	m_ui64Iteration   = 0;
	m_currentMean     = 0;
  m_currentStd      = 0;
  m_isInArtifactNow = false;
  m_bStartTrain = false;

	return true;
}

OpenViBE::boolean CBoxAlgorithmRiemannPotato::uninitialize(void)
{
	// uninit epoched input
	m_pSignalDecoder->uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pSignalDecoder);

	// uninit input stimulation
	m_pStimulationDecoderTrigger->uninitialize();
    ip_pMemoryBufferToDecodeTrigger.uninitialize();
	op_pStimulationSetTrigger.uninitialize();
    this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoderTrigger);

	// uninit stimulation output
	m_pStimulationEncoder1->uninitialize();
	ip_pStimulationsToEncode1.uninitialize();
	op_pEncodedMemoryBuffer1.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationEncoder1);

	//op_ui64SamplingRateTrigger.uninitialize();

	return true;
}

OpenViBE::boolean CBoxAlgorithmRiemannPotato::processInput(uint32 ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

OpenViBE::boolean CBoxAlgorithmRiemannPotato::process(void)
{
	//> Get dynamic box context
	IBoxIO* l_rDynamicBoxContext = getBoxAlgorithmContext()->getDynamicBoxContext();

	  //1. process stimulations to see if signal is be processed - input channel 1 
	for(uint32 i=0; i<l_rDynamicBoxContext->getInputChunkCount(1); i++)
	{
		uint64 l_ui64ChunkStartTime = l_rDynamicBoxContext->getInputChunkStartTime(0, i);

		TParameterHandler < const IMemoryBuffer* > ip_pMemoryBuffer(m_pStimulationDecoderTrigger->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
		ip_pMemoryBuffer=l_rDynamicBoxContext->getInputChunk(1, i);
		m_pStimulationDecoderTrigger->process();
		
		//header
		if(m_pStimulationDecoderTrigger->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedHeader))
		{
      
		}
    
		//buffer - input channel 1
		if(m_pStimulationDecoderTrigger->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
		  TParameterHandler < IStimulationSet* > op_pStimulationSetTrigger(m_pStimulationDecoderTrigger->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));
		  // Loop on stimulations
		  for(int j=0; j<op_pStimulationSetTrigger->getStimulationCount(); j++)
		  {
			// Check for Start Train Stimulation
			if (op_pStimulationSetTrigger->getStimulationIdentifier(j)==m_ui64StartTrainingStim)
			{
			  m_bStartTrain = true;
			  this->getLogManager() << LogLevel_Info << "Start training\n";
			}
        
			// Check for Stop train Stimulation
			if (op_pStimulationSetTrigger->getStimulationIdentifier(j)==m_ui64StopTrainingStim)
			{
			  m_bStartTrain = false;
			  this->getLogManager() << LogLevel_Info << "Stop training\n";
			}
        
			// Check for Reset Stimulation
			if (op_pStimulationSetTrigger->getStimulationIdentifier(j)==m_ui64ResetStim)
			{
			  Reset(l_rDynamicBoxContext,i);
			  this->getLogManager() << LogLevel_Info << "Reset\n";
			}
        
		  }
		}

		l_rDynamicBoxContext->markInputAsDeprecated(1, i);
	}


	//2. Process signal - input channel 0
	for(uint32 i=0; i<l_rDynamicBoxContext->getInputChunkCount(0); i++)
	{
		uint64 l_ui64ChunkStartTime = l_rDynamicBoxContext->getInputChunkStartTime(0, i);
		uint64 l_ui64ChunkEndTime   = l_rDynamicBoxContext->getInputChunkEndTime(0, i);

		TParameterHandler<const IMemoryBuffer*> ip_pMemoryBuffer(m_pSignalDecoder->getInputParameter(OVP_GD_Algorithm_SignalStreamDecoder_InputParameterId_MemoryBufferToDecode));
		ip_pMemoryBuffer=l_rDynamicBoxContext->getInputChunk(0, i);
			
		m_pSignalDecoder->process();

		ip_pStimulationsToEncode1->clear();

		//Header
		if(m_pSignalDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedHeader))
		{
		}
			
		//Buffer
		if(m_pSignalDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
		  if(m_bStartTrain)
			{
				TParameterHandler<IMatrix*> ip_pMatrix(m_pSignalDecoder->getOutputParameter(OVP_GD_Algorithm_SignalStreamDecoder_OutputParameterId_Matrix));
				itpp::mat X = convert(*ip_pMatrix);
          
				OpenViBE::boolean artifactDetected=false;

				//this->getLogManager() << LogLevel_Error << " X Size " <<  X.rows() << " " << X.cols()<< "\n";

				itpp::mat P = itpp::cov(X.transpose(),false);  ///X * X.transpose(); //P = cov (X)

				//this->getLogManager() << LogLevel_Error << " P Size " <<  P.rows() << " " << P.cols()<< "\n";

				if (m_ui64Iteration == 0) this->m_currentReferencePoint = P;
          
				float32 d = Riemann::distance(P,m_currentReferencePoint);

				//this->getLogManager() << LogLevel_Error << " d =  " <<  d << "\n";

				if (d >  m_f64Threshold && m_ui64Iteration > 50)
				{
				   artifactDetected = true;
				}
				else 
				{
					m_ui64Iteration++;

					int alpha;

					//init alpha with min
					if (m_ui64Iteration < m_ui64AdaptationSpeed) alpha = m_ui64Iteration; else alpha = m_ui64AdaptationSpeed;

					UpdateReferencePoint(P,alpha);

					UpdateTreshold(d,alpha);
					//this->getLogManager() << LogLevel_Error << " Updated both " <<  m_ui64Iteration << "\n";

					artifactDetected = false;
				}
          
				//uint64 l_ui64sampleTime = l_ui64ChunkStartTime + uint64((i/float64(this->getClockFrequency()))*(1LL<<32));//not sure frequency is provided correctly
                //uint64 l_ui64sampleTime = l_ui64ChunkStartTime+ uint64((i/float64(op_ui64SamplingRateTrigger))*(1LL<<32));
				uint64 l_ui64artTime = l_ui64ChunkStartTime + (l_ui64ChunkEndTime-l_ui64ChunkStartTime)/2;

				if (! m_isInArtifactNow && artifactDetected)
				{
					//start new artifact 
					ip_pStimulationsToEncode1->appendStimulation(m_ui64StartArtifactStim,l_ui64artTime, 0); //add to st1
					m_isInArtifactNow = true;
				}

				if (m_isInArtifactNow && !artifactDetected)
				{
					//artifact ended
					ip_pStimulationsToEncode1->appendStimulation(m_ui64StopArtifactStim,l_ui64artTime, 0); //add to st1
					m_isInArtifactNow = false;
				}

				//output stimulation in OpenVibe
				op_pEncodedMemoryBuffer1=l_rDynamicBoxContext->getOutputChunk(0);
				m_pStimulationEncoder1->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
				l_rDynamicBoxContext->markOutputAsReadyToSend(0,l_ui64ChunkStartTime ,l_ui64ChunkEndTime );
			}
		}

		//Buffer end
		if(m_pSignalDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedEnd))
		{
		}

		l_rDynamicBoxContext->markInputAsDeprecated(0, i);
	}

	return true;
}// End Process


void CBoxAlgorithmRiemannPotato::UpdateReferencePoint(itpp::mat currentTrial,OpenViBE::float32 alpha)
{
    m_currentReferencePoint = Riemann::geodesic(this->m_currentReferencePoint,currentTrial,alpha);
}

void CBoxAlgorithmRiemannPotato::Reset(IBoxIO* rDynamicBoxContext,int chunkCount)
{
  this->m_currentMean     = 0;
  this->m_ui64Iteration   = 0;
  this->m_currentMean     = 0;
  this->m_currentStd      = 0;
  this->m_isInArtifactNow = false;
  this->m_bStartTrain     = false;

  //send "end of artifact" to reset the state
  uint64 l_ui64ChunkStartTime = rDynamicBoxContext->getInputChunkStartTime(0, chunkCount);
  uint64 l_ui64ChunkEndTime   = rDynamicBoxContext->getInputChunkEndTime(0, chunkCount);

  ip_pStimulationsToEncode1->appendStimulation(m_ui64StopArtifactStim,l_ui64ChunkEndTime, 0);

  op_pEncodedMemoryBuffer1=rDynamicBoxContext->getOutputChunk(0);
  m_pStimulationEncoder1->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
  rDynamicBoxContext->markOutputAsReadyToSend(0,l_ui64ChunkStartTime ,l_ui64ChunkEndTime );
}
  

void CBoxAlgorithmRiemannPotato::UpdateTreshold(OpenViBE::float32 riemannenanDistance,OpenViBE::float32 alpha)
{
	float32 a = alpha / (alpha + 1);
    float32 b = 1 / (alpha + 1);

	this->m_currentMean  = a * this->m_currentMean + b * riemannenanDistance;

	float32 c = a * itpp::sqr(this->m_currentStd) + b * itpp::sqr(riemannenanDistance - this->m_currentMean);

	this->m_currentStd = sqrt(c);

	m_f64Threshold = this->m_currentMean + m_f64ThresholdParameter * this->m_currentStd;
}


itpp::mat CBoxAlgorithmRiemannPotato::convert(const IMatrix& rMatrix)
{
		itpp::mat l_oResult(
			rMatrix.getDimensionSize(1),
			rMatrix.getDimensionSize(0));
		System::Memory::copy(l_oResult._data(), rMatrix.getBuffer(), rMatrix.getBufferElementCount()*sizeof(float64));
		return l_oResult.transpose();
}

