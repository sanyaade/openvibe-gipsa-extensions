/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCBoxUpsample.h"

#include <system/ovCMemory.h>

#include <itpp/base/algebra/eigen.h>
#include <itpp/base/algebra/inv.h>
#include <itpp/base/converters.h>
#include <itpp/stat/misc_stat.h>
#include <itpp/itsignal.h>
#include <itpp/signal/resampling.h>
#include <itpp/base/factory.h>
using namespace itpp;

#include <openvibe/ovITimeArithmetics.h>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessing;

//#define CBoxAlgorithmUpsample_Debug 0

boolean CBoxAlgorithmUpsample::initialize(void)
{
	m_pStreamDecoder=NULL;
	m_pStreamEncoder=NULL;

	m_i64DecimationFactor=FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	if(m_i64DecimationFactor<=1)
	{
		this->getLogManager() << LogLevel_Error << "Upsampling factor should be stricly greater than 1 !\n";
		return false;
	}

	m_pStreamDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalStreamDecoder));
	m_pStreamDecoder->initialize();

	ip_pMemoryBuffer.initialize(m_pStreamDecoder->getInputParameter(OVP_GD_Algorithm_SignalStreamDecoder_InputParameterId_MemoryBufferToDecode));
	op_pMatrix.initialize(m_pStreamDecoder->getOutputParameter(OVP_GD_Algorithm_SignalStreamDecoder_OutputParameterId_Matrix));
	op_ui64SamplingRate.initialize(m_pStreamDecoder->getOutputParameter(OVP_GD_Algorithm_SignalStreamDecoder_OutputParameterId_SamplingRate));

	m_pStreamEncoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalStreamEncoder));
	m_pStreamEncoder->initialize();

	ip_ui64SamplingRate.initialize(m_pStreamEncoder->getInputParameter(OVP_GD_Algorithm_SignalStreamEncoder_InputParameterId_SamplingRate));
	ip_pMatrix.initialize(m_pStreamEncoder->getInputParameter(OVP_GD_Algorithm_SignalStreamEncoder_InputParameterId_Matrix));
	op_pMemoryBuffer.initialize(m_pStreamEncoder->getOutputParameter(OVP_GD_Algorithm_SignalStreamEncoder_OutputParameterId_EncodedMemoryBuffer));

	m_ui32ChannelCount=0;
	m_ui32InputSampleIndex=0;
	m_ui32InputSampleCountPerSentBlock=0;
	m_ui32OutputSampleIndex=0;
	m_ui32OutputSampleCountPerSentBlock=0;

	m_ui64TotalSampleCount=0;
	m_ui64LastStartTime=0;

	return true;
}

boolean CBoxAlgorithmUpsample::uninitialize(void)
{
	op_pMemoryBuffer.uninitialize();
	ip_pMatrix.uninitialize();
	ip_ui64SamplingRate.uninitialize();

	if(m_pStreamEncoder)
	{
		m_pStreamEncoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pStreamEncoder);
		m_pStreamEncoder=NULL;
	}

	op_ui64SamplingRate.uninitialize();
	op_pMatrix.uninitialize();
	ip_pMemoryBuffer.uninitialize();

	if(m_pStreamDecoder)
	{
		m_pStreamDecoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pStreamDecoder);
		m_pStreamDecoder=NULL;
	}

	return true;
}

boolean CBoxAlgorithmUpsample::processInput(uint32 ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

boolean CBoxAlgorithmUpsample::process(void)
{
	// IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();
	uint32 i, j, k;

	for(i=0; i<l_rDynamicBoxContext.getInputChunkCount(0); i++)
	{
		ip_pMemoryBuffer=l_rDynamicBoxContext.getInputChunk(0, i);
		op_pMemoryBuffer=l_rDynamicBoxContext.getOutputChunk(0);

		uint64 l_ui64StartTime=l_rDynamicBoxContext.getInputChunkStartTime(0, i);
		uint64 l_ui64EndTime=l_rDynamicBoxContext.getInputChunkEndTime(0, i);

		if(l_ui64StartTime!=m_ui64LastEndTime)
		{
			m_ui64StartTimeBase=l_ui64StartTime;
			m_ui32InputSampleIndex=0;
			m_ui32OutputSampleIndex=0;
			m_ui64TotalSampleCount=0;
		}

		m_ui64LastStartTime=l_ui64StartTime;
		m_ui64LastEndTime=l_ui64EndTime;

		m_pStreamDecoder->process();

		//header
		if(m_pStreamDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedHeader))
		{
			m_ui32InputSampleIndex=0;
			m_ui32InputSampleCountPerSentBlock=op_pMatrix->getDimensionSize(1);
			m_ui64InputSamplingFrequency=op_ui64SamplingRate;

			/*if(m_ui64InputSamplingFrequency%m_i64DecimationFactor != 0)
			{
				this->getLogManager() << LogLevel_Error << "Input signal sampling frequency " << m_ui64InputSamplingFrequency << " is not a multiple of the decimation factor " << m_i64DecimationFactor << "\n";
				return false;
			}*/

			m_ui32OutputSampleIndex=0;
			//m_ui32OutputSampleCountPerSentBlock=(uint32)(m_ui32InputSampleCountPerSentBlock / m_i64DecimationFactor);
			m_ui32OutputSampleCountPerSentBlock=(uint32)(m_ui32InputSampleCountPerSentBlock * m_i64DecimationFactor); //changed by Anton
			m_ui32OutputSampleCountPerSentBlock=(m_ui32OutputSampleCountPerSentBlock?m_ui32OutputSampleCountPerSentBlock:1);
			
			//m_ui64OutputSamplingFrequency=op_ui64SamplingRate/m_i64DecimationFactor;
			m_ui64OutputSamplingFrequency=op_ui64SamplingRate * m_i64DecimationFactor; //anton 

			m_ui32ChannelCount=op_pMatrix->getDimensionSize(0);
			m_ui64TotalSampleCount=0;

			OpenViBEToolkit::Tools::Matrix::copyDescription(*ip_pMatrix, *op_pMatrix);
			ip_pMatrix->setDimensionSize(1, m_ui32OutputSampleCountPerSentBlock);

			ip_ui64SamplingRate=m_ui64OutputSamplingFrequency; //anton - setting the new output frequency
			
			m_pStreamEncoder->process(OVP_GD_Algorithm_SignalStreamEncoder_InputTriggerId_EncodeHeader);
			OpenViBEToolkit::Tools::Matrix::clearContent(*ip_pMatrix);

			l_rDynamicBoxContext.markOutputAsReadyToSend(0, l_ui64StartTime, l_ui64StartTime); // $$$ supposes we have one node per chunk
		}

		//buffer
		if(m_pStreamDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
			//input
			float64* l_pInputBuffer=op_pMatrix->getBuffer();
			uint32 l_ui32ChannelCount = op_pMatrix->getDimensionSize(0);
            uint32 l_ui32SamplesPerChannelInput = op_pMatrix->getDimensionSize(1);

			//output
			float64* l_pOutputBuffer=ip_pMatrix->getBuffer();//+m_ui32OutputSampleIndex
			uint32 l_ui32SamplesPerChannelOutput = ip_pMatrix->getDimensionSize(1);

			//this->getLogManager() << LogLevel_Error << "Input natrix " <<  op_pMatrix->getBufferElementCount() <<"\n";
			//this->getLogManager() << LogLevel_Error << "Output natrix " << ip_pMatrix->getBufferElementCount() << "\n";
			
			//this->getLogManager() << LogLevel_Error << "l_ui32SamplesPerChannelInput " << l_ui32SamplesPerChannelInput << "\n";
			//this->getLogManager() << LogLevel_Error << "l_ui32SamplesPerChannelOutput " <<l_ui32SamplesPerChannelOutput << "\n";
		    
		    itpp::Mat<float64> input( op_pMatrix->getBufferElementCount(), l_ui32ChannelCount);

			//Fill a matrix - OpenVibe provides the data ch1 (all values from all samples), ch2(all values from all samples) ... chN, 
			//In the generated matrix every row is a single sample (containing the data from all channels) and every column number is the number of the channel
			for (int k=0;k < l_ui32ChannelCount; k++) 
			{
				for (int j=0;j <l_ui32SamplesPerChannelInput; j++)
				{
					int index = (k * l_ui32SamplesPerChannelInput) + j;
					input.set(j,k,l_pInputBuffer[index]);
				}
			}

			//set the last row from the previous iteration as first row of the current input
			Vec<float64> v(l_ui32ChannelCount);

			//we need to add one more row because the lininterp returns n-1
			bool firstTime = (m_vLastRow.size() == 0);
			if (firstTime)
			{
			    for (int l = 0; l< l_ui32ChannelCount; l++)
			    {
					v[l]=0;
				}
			}
			else  
			{
				for (int l = 0; l< l_ui32ChannelCount; l++)
			    {
					v[l] = m_vLastRow[l];
				}
			}
			input.ins_row(0,v);//put as first row
			m_vLastRow.clear();
			//end of setting the last row

			//transpose because of itpp lininterp parameter input
		    input = input.transpose();

			//resample (upsample)
		    itpp::Mat<float64> output = itpp::lininterp (input, m_i64DecimationFactor);

	        output = output.transpose(); //back to OpenVibe from itpp
       
			//save the last row from the output
			for (int l = 0;l< l_ui32ChannelCount; l++)
			{
				float64 value = output.get(l_ui32SamplesPerChannelOutput, l); //save the last row for the next iteration (so that we continue from it)
				m_vLastRow.push_back(value);
			}

			//set output result
			//the matrix has one last row more, but we do not send the last one (already stored in m_vLastRow), so it is ok
			for (int i=0;i < l_ui32ChannelCount; i++) 
			{
				for (int j=0;j < l_ui32SamplesPerChannelOutput; j++)
				{
					int index = (i * l_ui32SamplesPerChannelOutput) + j;
					l_pOutputBuffer[index] = output.get(j,i);
				}
			}

			m_pStreamEncoder->process(OVP_GD_Algorithm_SignalStreamEncoder_InputTriggerId_EncodeBuffer);

			//adjust the time of the samples 
			uint64 l_ui64SampleStartTime = m_ui64StartTimeBase + ITimeArithmetics::sampleCountToTime(m_ui64OutputSamplingFrequency, m_ui64TotalSampleCount);
			uint64 l_ui64SampleEndTime   = m_ui64StartTimeBase + ITimeArithmetics::sampleCountToTime(m_ui64OutputSamplingFrequency, m_ui64TotalSampleCount + m_ui32OutputSampleCountPerSentBlock);
						
			l_rDynamicBoxContext.markOutputAsReadyToSend(0, l_ui64SampleStartTime, l_ui64SampleEndTime);
			m_ui64TotalSampleCount+=m_ui32OutputSampleCountPerSentBlock;

			OpenViBEToolkit::Tools::Matrix::clearContent(*ip_pMatrix);

		}

		if(m_pStreamDecoder->isOutputTriggerActive(OVP_GD_Algorithm_SignalStreamDecoder_OutputTriggerId_ReceivedEnd))
		{
			m_pStreamEncoder->process(OVP_GD_Algorithm_SignalStreamEncoder_InputTriggerId_EncodeEnd);
			l_rDynamicBoxContext.markOutputAsReadyToSend(0, l_ui64StartTime, l_ui64StartTime); // $$$ supposes we have one node per chunk
		}

		l_rDynamicBoxContext.markInputAsDeprecated(0, i);
	}
	return true;
}
