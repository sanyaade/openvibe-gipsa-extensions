/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu,

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
 
#ifndef __OpenViBEPlugins_BoxAlgorithm_ProcessMDM_H__
#define __OpenViBEPlugins_BoxAlgorithm_ProcessMDM_H__

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <itpp/base/algebra/eigen.h>
#include <itpp/base/algebra/inv.h>
#include <itpp/base/converters.h>
#include <itpp/stat/misc_stat.h>
#include <itpp/itsignal.h>
#include <itpp/itcomm.h>

#include <vector>
#include <cstdio>
#include <string>
#include <fstream>

#include "RClass.h"
#include "ovp_concurrency.h"

#define OVP_ClassId_BoxAlgorithm_ProcessMDM 	 	OpenViBE::CIdentifier(0x29917F69, 0x67F36C52)
#define OVP_ClassId_BoxAlgorithm_ProcessMDMDesc 	OpenViBE::CIdentifier(0x70B10CD1, 0x63A928A7)

namespace OpenViBEPlugins
{
	namespace SignalProcessing
	{
		class CBoxAlgorithmProcessMDM : public OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual OpenViBE::boolean initialize(void);
			virtual OpenViBE::boolean uninitialize(void);
			virtual OpenViBE::boolean processInput(OpenViBE::uint32 ui32InputIndex);
			virtual OpenViBE::boolean process(void);

			itpp::mat convert(const OpenViBE::IMatrix& rMatrix);
			OpenViBE::boolean loadFile(std::string l_sFileNamePath);
			static OpenViBE::uint32 ApplyMDM(itpp::mat P,vector_type<itpp::mat> m_vMean,std::vector<OpenViBE::float64>& vDistances);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_ProcessMDM);

		protected:

			//stimulation input 1
			/*OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoderTrigger;
			OpenViBE::Kernel::TParameterHandler <const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecodeTrigger;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > op_pStimulationSetTrigger;*/

			//output stimulation 1
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IStimulationSet* > ip_pStimulationsToEncode1;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer1;

			//input signal decoder
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalDecoder;
			OpenViBE::Kernel::TParameterHandler < const OpenViBE::IMemoryBuffer* > ip_pMemoryBufferToDecode1;
            OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > op_pDecodedMatrix1;

			//output signal encoder - score
			OpenViBE::Kernel::IAlgorithmProxy* m_pSignalEncoder;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMatrix* > ip_pMatrixToEncode;
			OpenViBE::Kernel::TParameterHandler < OpenViBE::IMemoryBuffer* > op_pEncodedMemoryBuffer;

			std::ifstream m_ParamFile;
			OpenViBE::boolean m_IsP300;
			OpenViBE::boolean m_bHasSentHeader;
			itpp::mat m_mP1; //used only for P300, stores the mean of all P300 epochs
			std::vector<OpenViBE::uint64> m_vClassStimulations; 
			vector_type<RClass*> m_vOnlineClasses;
		};

		class CBoxAlgorithmProcessMDMListener : public OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >
		{
		public:

			virtual OpenViBE::boolean onInputAdded(OpenViBE::Kernel::IBox& rBox, const OpenViBE::uint32 ui32Index)
			{
				char l_sName[1024];
				OpenViBE::uint32 i;

				rBox.setInputType(ui32Index, OV_TypeId_StreamedMatrix);
				for(i=0; i<rBox.getInputCount(); i++)
				{
					sprintf(l_sName, "Input Signal %u", i+1);
					rBox.setInputName(i, l_sName);
				}

				//rBox.addSetting("Class stimulation", OV_TypeId_Stimulation, "");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxListener < OpenViBE::Plugins::IBoxListener >, OV_UndefinedIdentifier);
		};

		class CBoxAlgorithmProcessMDMDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("Process MDM"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Anton Andreev, Alexandre Barachant"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("This is a classification online box. Works on different modalities such as: motor imagery, P300, SSVP."); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString("Use Train MDM box before using this box to first generate a parameters file. Reference: A. Barachant, S. Bonnet, M. Congedo and C. Jutten, 'Multiclass Brain-Computer Interface Classification by Riemannian Geometry,' in IEEE Transactions on Biomedical Engineering, vol. 59, no. 4, p. 920-928, 2012."); }
			//virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Streaming"); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Gipsa-lab"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-sort-ascending"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_ProcessMDM; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SignalProcessing::CBoxAlgorithmProcessMDM; }
			virtual OpenViBE::Plugins::IBoxListener* createBoxListener(void) const               { return new CBoxAlgorithmProcessMDMListener; }
			virtual void releaseBoxListener(OpenViBE::Plugins::IBoxListener* pBoxListener) const { delete pBoxListener; }

			virtual OpenViBE::boolean getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Input signal",          OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput ("Labels",	               OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput ("Classification state",  OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("Parameters file path:", OV_TypeId_Filename,"");
				rBoxAlgorithmPrototype.addSetting("Modality is P300",      OV_TypeId_Boolean, "false");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 1", OV_TypeId_Stimulation, "OVTK_GDF_Left");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 2", OV_TypeId_Stimulation, "OVTK_GDF_Right");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 3", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 4", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 5", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 6", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 7", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 8", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 9", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 10", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addSetting("Class stimulation 11", OV_TypeId_Stimulation, "");
				rBoxAlgorithmPrototype.addFlag   (OpenViBE::Kernel::BoxFlag_CanAddInput);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ProcessMDMDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_ProcessMDM_H__
