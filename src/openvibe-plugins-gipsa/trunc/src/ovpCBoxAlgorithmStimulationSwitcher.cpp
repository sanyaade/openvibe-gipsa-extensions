/* Project: Gipsa-lab plugins for OpenVibe
 * AUTHORS AND CONTRIBUTORS: Andreev A., Barachant A., Congedo M., Ionescu,Gelu, Goyat Matthieu

 * This file is part of "Gipsa-lab plugins for OpenVibe".
 * You can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
 
#include "ovpCBoxAlgorithmStimulationSwitcher.h"

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SignalProcessing;

#include <iostream>

namespace
{
	class _AutoCast_
	{
	public:
		_AutoCast_(IBox& rBox, IConfigurationManager& rConfigurationManager, const uint32 ui32Index) : m_rConfigurationManager(rConfigurationManager) { rBox.getSettingValue(ui32Index, m_sSettingValue); }
		operator uint64 (void) { return m_rConfigurationManager.expandAsUInteger(m_sSettingValue); }
		operator int64 (void) { return m_rConfigurationManager.expandAsInteger(m_sSettingValue); }
		operator float64 (void) { return m_rConfigurationManager.expandAsFloat(m_sSettingValue); }
		operator OpenViBE::boolean (void) { return m_rConfigurationManager.expandAsBoolean(m_sSettingValue); }
		operator const CString (void) { return m_sSettingValue; }
	protected:
		IConfigurationManager& m_rConfigurationManager;
		CString m_sSettingValue;
	};
};

OpenViBE::boolean CBoxAlgorithmStimulationSwitcher::initialize(void)
{
	IBox& l_rStaticBoxContext=this->getStaticBoxContext();

	//> init input stimulation
	m_pStimulationDecoder1=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
    m_pStimulationDecoder1->initialize();
    ip_pMemoryBufferToDecode1.initialize(m_pStimulationDecoder1->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
    op_pStimulationSet1.initialize(m_pStimulationDecoder1->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));

	m_pStimulationDecoder2=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
    m_pStimulationDecoder2->initialize();
    ip_pMemoryBufferToDecode2.initialize(m_pStimulationDecoder2->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
    op_pStimulationSet2.initialize(m_pStimulationDecoder2->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));
	
	
	//> used to decode the next value 
	m_pNextSignalDecoderTrigger=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalStreamDecoder));
	m_pNextSignalDecoderTrigger->initialize();
	m_ip_pNextMemoryBufferToDecodeTrigger.initialize(m_pNextSignalDecoderTrigger->getInputParameter(OVP_GD_Algorithm_SignalStreamDecoder_InputParameterId_MemoryBufferToDecode));
	m_op_pNextDecodedMatrixTrigger.initialize(m_pNextSignalDecoderTrigger->getOutputParameter(OVP_GD_Algorithm_SignalStreamDecoder_OutputParameterId_Matrix));

	//> init stimulation output 1
	m_pStimulationEncoder1=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamEncoder));
	m_pStimulationEncoder1->initialize();
	ip_pStimulationsToEncode1.initialize(m_pStimulationEncoder1->getInputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_InputParameterId_StimulationSet));
	op_pEncodedMemoryBuffer1.initialize(m_pStimulationEncoder1->getOutputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_OutputParameterId_EncodedMemoryBuffer));

	//> init stimulation output 2
	m_pStimulationEncoder2=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamEncoder));
	m_pStimulationEncoder2->initialize();
	ip_pStimulationsToEncode2.initialize(m_pStimulationEncoder2->getInputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_InputParameterId_StimulationSet));
	op_pEncodedMemoryBuffer2.initialize(m_pStimulationEncoder2->getOutputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_OutputParameterId_EncodedMemoryBuffer));

	//> get conversion list and parallel port policy
	m_ui64StimulationSwitchLeft  = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_ui64StimulationSwitchRight = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	m_ui64LastStimulation = 0;
	m_ui64LastDateTime	  = 0;

	return true;
}

OpenViBE::boolean CBoxAlgorithmStimulationSwitcher::uninitialize(void)
{
	// uninit input stimulation 1
	m_pStimulationDecoder1->uninitialize();
    ip_pMemoryBufferToDecode1.uninitialize();
	op_pStimulationSet1.uninitialize();
    this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoder1);

	// uninit input stimulation 2
	m_pStimulationDecoder2->uninitialize();
    ip_pMemoryBufferToDecode2.uninitialize();
	op_pStimulationSet2.uninitialize();
    this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoder2);

	// uninit the extra value processing
	m_pNextSignalDecoderTrigger->uninitialize();
	m_ip_pNextMemoryBufferToDecodeTrigger.uninitialize();
	m_op_pNextDecodedMatrixTrigger.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pNextSignalDecoderTrigger);
	
	// uninit stimulation output 1 
	m_pStimulationEncoder1->uninitialize();
	ip_pStimulationsToEncode1.uninitialize();
	op_pEncodedMemoryBuffer1.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationEncoder1);

	// uninit stimulation output 2 
	m_pStimulationEncoder2->uninitialize();
	ip_pStimulationsToEncode2.uninitialize();
	op_pEncodedMemoryBuffer2.uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationEncoder2);

	return true;
}

OpenViBE::boolean CBoxAlgorithmStimulationSwitcher::processInput(uint32 ui32InputIndex)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

OpenViBE::boolean CBoxAlgorithmStimulationSwitcher::process(void)
{
	//> Get dynamic box context
	IBoxIO* l_pDynamicBoxContext=getBoxAlgorithmContext()->getDynamicBoxContext();
	
	//Stimulation Channel 2 - control channel
	for(uint32 j=0; j<l_pDynamicBoxContext->getInputChunkCount(1); j++)
	{
		ip_pMemoryBufferToDecode2=l_pDynamicBoxContext->getInputChunk(1, j);
		m_pStimulationDecoder2->process();

		if(m_pStimulationDecoder2->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
			uint64 l_ui64ChunkStartTime = l_pDynamicBoxContext->getInputChunkStartTime(0, j);
			uint64 l_ui64ChunkEndTime = l_pDynamicBoxContext->getInputChunkEndTime(0, j);

			for(uint32 stim=0; stim<op_pStimulationSet2->getStimulationCount(); stim++)
			{
				OpenViBE::uint64 l_ui64CurrentIdentifier = op_pStimulationSet2->getStimulationIdentifier(stim);
				uint64 l_ui64sampleTime =				   op_pStimulationSet2->getStimulationDate(stim);

				m_ui64LastStimulation = l_ui64CurrentIdentifier;
				m_ui64LastDateTime	  = l_ui64sampleTime;

				if (m_ui64LastStimulation == m_ui64StimulationSwitchLeft)
					this->getLogManager() << LogLevel_Info << "Output switched to Left\n";
				else if (m_ui64LastStimulation == m_ui64StimulationSwitchRight)
					this->getLogManager() << LogLevel_Info << "Output switched to Right\n";
				else this->getLogManager() << LogLevel_Warning << "Received undefined control stimulation. Ignored.";
			}
		}

		l_pDynamicBoxContext->markInputAsDeprecated(1,j);
	}

	//Stimulation Channel 2 - stimulations to be redirected
	for(uint32 j=0; j<l_pDynamicBoxContext->getInputChunkCount(0); j++)
	{
		ip_pMemoryBufferToDecode1=l_pDynamicBoxContext->getInputChunk(0, j);
		m_pStimulationDecoder1->process();

		ip_pStimulationsToEncode1->clear();//st 1
		ip_pStimulationsToEncode2->clear();//st 2

		if(m_pStimulationDecoder1->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
			uint64 l_ui64ChunkStartTime=l_pDynamicBoxContext->getInputChunkStartTime(0, j);
			uint64 l_ui64ChunkEndTime=l_pDynamicBoxContext->getInputChunkEndTime(0, j);

			for(uint32 stim=0; stim<op_pStimulationSet1->getStimulationCount(); stim++)
			{
				//std::vector<ConversionEntry>::const_iterator cii;

				OpenViBE::uint64 l_ui64CurrentIdentifier = op_pStimulationSet1->getStimulationIdentifier(stim);
				uint64 l_ui64sampleTime					 = op_pStimulationSet1->getStimulationDate(stim);

				if (m_ui64LastStimulation != 0 && m_ui64LastDateTime != 0)
				{
					if (m_ui64LastStimulation == m_ui64StimulationSwitchLeft && l_ui64sampleTime > m_ui64LastDateTime)
						ip_pStimulationsToEncode1->appendStimulation(l_ui64CurrentIdentifier, l_ui64sampleTime, 0); 

					else if (m_ui64LastStimulation == m_ui64StimulationSwitchRight && l_ui64sampleTime > m_ui64LastDateTime)
						ip_pStimulationsToEncode2->appendStimulation(l_ui64CurrentIdentifier, l_ui64sampleTime, 0); 
				}

			}
			//output stimulation set 1
		    op_pEncodedMemoryBuffer1=l_pDynamicBoxContext->getOutputChunk(0);
			m_pStimulationEncoder1->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
			l_pDynamicBoxContext->markOutputAsReadyToSend(0,l_ui64ChunkStartTime ,l_ui64ChunkEndTime );

			//output stimulation set 2
		    op_pEncodedMemoryBuffer2=l_pDynamicBoxContext->getOutputChunk(1);
			m_pStimulationEncoder2->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
			l_pDynamicBoxContext->markOutputAsReadyToSend(1,l_ui64ChunkStartTime ,l_ui64ChunkEndTime );
		}

		l_pDynamicBoxContext->markInputAsDeprecated(0,j);
	 
	}

	return true;
}// End Process