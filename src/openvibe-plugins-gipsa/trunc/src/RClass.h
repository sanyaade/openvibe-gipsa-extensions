#ifndef __OpenViBEPlugins_Riemann_Class_H__
#define __OpenViBEPlugins_Riemann_Class_H__

#include <itpp/itcomm.h>
#include <vector>

class RClass // Riemann class
{
    public: 
		
        std::vector<itpp::mat> vBufferedSignal; 
		std::vector<itpp::mat> vCovarianceMatrices; // the input signal for the corresponding class represented as covariance matrices
		itpp::mat ResultMean; //the barycenter - the mean point of all the covraiance matrices (vCovarianceMatrices)
		std::vector<double> v_RiemmanDistances; //the distance between the barycenter and some input points provided 

		double DistanceMean;
		double DistanceSTD;
};

#endif