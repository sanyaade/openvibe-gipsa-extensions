
byte incomingByte = 0;   // for incoming serial data


void setup() {
  Serial.begin(115200);     // opens serial port, sets data rate to 115200 bps
  DDRD = DDRD | B11111100;  // this is safer as it sets pins 2 to 7 as outputs
                            // without changing the value of pins 0 & 1, which are RX & TX 
  DDRB = DDRB | B00000011;                            
}

void loop() {

        // send data only when you receive data:
        if (Serial.available() > 0) {
                // read the incoming byte:
                incomingByte = Serial.read();
                
                byte portdval = incomingByte << 2;
                byte portbval = incomingByte >> 6;
                // say what you got:
                PORTD = portdval;
                PORTB = portbval;       
        }
}
