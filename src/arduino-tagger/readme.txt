Contains the information to build a USB to parallel port converter using an Arduino Uno.

Use http://www.kicad-pcb.org/display/KICAD/KiCad+EDA+Software+Suite

to view the scheme. The second one is with galvanic protection, but it does not work with all amplifiers.