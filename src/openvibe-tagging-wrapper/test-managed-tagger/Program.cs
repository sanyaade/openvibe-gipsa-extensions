﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test_managed_tagger
{
    class Program
    {
        //managed wrapper openvibe-tagging-wrapper dll is required
        //unamanaged inpout32.dll is required
        static void Main(string[] args)
        {
            //OpenVibeTaggingWrapper p = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.Software,"openvibeExternalStimulations");
            //OpenVibeTaggingWrapper p = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort,"888");
            string comPort = "COM1";

            OpenVibeTaggingWrapper p = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.SerialPort, comPort);

            if (p.GetInitErrorMessage() != "")
            {
                Console.WriteLine(p.GetInitErrorMessage());
            }

            for (int i = 0; i < 10000; i++)
            {
                //p.SetTargetRow(5);
                Console.WriteLine(i);
                System.Threading.Thread.Sleep(1000);
                p.TestWriteValue(i);
                //p.TestWriteValue(32779);
            }

            //System.Threading.Thread.Sleep(1000);

            p.TestWriteValue(255);
        }
    }
}
