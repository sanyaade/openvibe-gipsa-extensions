﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Threading;
using System.ComponentModel;

namespace wpf_p300
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
       OpenVibeTaggingWrapper taggerHardware;
       OpenVibeTaggingWrapper taggerSoftware;

       SolidColorBrush[] fbrushes;
       TextBlock[] tboxes;

       BackgroundWorker bw;

       struct Parameters
       {
           public int FlashDuration;
           public int PauseDuration;
           public int FlashCount;
       }

       Parameters p;

        public Main()
        {
            fbrushes = new SolidColorBrush[36];
            tboxes = new TextBlock[36];


            InitializeComponent();

            InitializeTable();

            cbTagMethod.SelectedIndex = 0;

            //taggerHardware = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort); 
            //taggerHardware = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.SerialPort, "COM5");
            //taggerHardware = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.Robik, "");

            //taggerHardware.ConfigureFlashBaseRowCode(20);
            //taggerHardware.ConfigureFlashBaseColumnCode(40);
            //taggerHardware.ConfigureTargetBaseRowCode(60);
            //taggerHardware.ConfigureTargetBaseColumnCode(80);
            //taggerHardware.ConfigureExperimentStartCode(100);
            //taggerHardware.ConfigureTrainingStartCode(101);
            //taggerHardware.ConfigureRepetionCompletedCode(102);

            taggerSoftware = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.Software,"openvibeExternalStimulations");
            ConfigureTagger(taggerSoftware);

            cbTagMethod.SelectedIndex = 1;

            //SetTarget(5, 5);

            //double[] p; p = new double[6];
            //p[0] = -1;
            //p[1] = 2;
            //p[2] = -10;
            //p[3] = -1;
            //p[4] = 8;
            //p[4] = 4;
            //int[] r = taggerHardware.ProcessVRPNResult(3, 3, p);
        }

        private void ConfigureTagger(OpenVibeTaggingWrapper tagger)
        {
            //if ((m_config.CommProtocol == SerialPort) || (m_config.CommProtocol == EightbitPP) || (m_config.CommProtocol == Robik))
            //{
            //    //set numeric codes
            //    communicationHandler->ConfigureFlashBaseRowCode(20);
            //    communicationHandler->ConfigureFlashBaseColumnCode(40);

            //    communicationHandler->ConfigureTargetBaseRowCode(60);
            //    communicationHandler->ConfigureTargetBaseColumnCode(80);

            //    communicationHandler->ConfigureExperimentStartCode(100);
            //    communicationHandler->ConfigureTrainingStartCode(101);
            //    communicationHandler->ConfigureRepetionCompletedCode(102);
            //    communicationHandler->ConfigureResetCode(103);
            //}
            //else 
            if (tagger.GetProtocol() == OpenVibeTaggingWrapper.eProtocol.Software)
            {
                //The following stimulation codes are copied from OpenVibe because the current scenario expects them

                const int OVTK_StimulationId_ExperimentStart = 32769;//  0x00008001
                const int OVTK_StimulationId_ExperimentStop = 32770;  // 0x00008002
                const int OVTK_StimulationId_LabelEnd = 33279;//  0x000081ff

                const int OVTK_StimulationId_Label_00 = 33024;//        0x00008100
                const int OVTK_StimulationId_Label_10 = 33040;//       0x00008110

                const int OVTK_StimulationId_Beep = 33282;//       0x00008202

                //These two are defined for BI
                const int BI_Target_Row = 33056;//        0x00008120 //user defined
                const int BI_Target_Column = 33072;//        0x00008130 //user defined

                //Set stimulation codes which is different a bit different from numeric codes when doing hardware tagging
                //stimulation codes set here must be processed in OpenVibe, so that OpenVibe can detect matrix size and when target is flashed 
                tagger.ConfigureFlashBaseRowCode(OVTK_StimulationId_Label_00);
                tagger.ConfigureFlashBaseColumnCode(OVTK_StimulationId_Label_10);

                tagger.ConfigureTargetBaseRowCode(BI_Target_Row);
                tagger.ConfigureTargetBaseColumnCode(BI_Target_Column);

                tagger.ConfigureExperimentStartCode(OVTK_StimulationId_ExperimentStart);
                tagger.ConfigureTrainingStartCode(OVTK_StimulationId_ExperimentStop);
                tagger.ConfigureRepetionCompletedCode(OVTK_StimulationId_LabelEnd);

                tagger.ConfigureResetCode(OVTK_StimulationId_Beep);

            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;

            bw.DoWork +=
                new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            
            
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            int method = Convert.ToInt32(e.Argument);

            BackgroundWorker worker = sender as BackgroundWorker;

            System.Threading.Thread.Sleep(50);

            switch (method)
            {
                case 0: taggerHardware.SetFinalized(); break;
                case 1: taggerSoftware.SetFinalized(); break;
                case 2: taggerHardware.SetFinalized(); taggerSoftware.SetFinalized(); break;
            }
            //taggerHardware.SetFinalized();//clear if cancelled

            for (int i = 0; i < p.FlashCount; i++)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;

                    //taggerHardware.SetFinalized();
                    switch (method)
                    {
                        case 0: taggerHardware.SetFinalized(); break;
                        case 1: taggerSoftware.SetFinalized(); break;
                        case 2: taggerHardware.SetFinalized(); taggerSoftware.SetFinalized(); break;
                    }

                    break;
                }

                int row = i % 6;
                worker.ReportProgress(row, System.Windows.Media.Color.FromScRgb(Colors.Red.ScA,Colors.Red.ScR, Colors.Red.ScG,Colors.Red.ScB));
                
                //taggerHardware.SetFlashRow(row);
                switch (method)
                {
                    case 0: taggerHardware.SetFlashRow(row); break;
                    case 1: taggerSoftware.SetFlashRow(row); break;
                    case 2: taggerHardware.SetFlashRow(row); taggerSoftware.SetFlashRow(row); break;
                }

                System.Threading.Thread.Sleep(p.FlashDuration);
                
                //taggerHardware.SetFinalized();
                switch (method)
                {
                    case 0: taggerHardware.SetFinalized(); break;
                    case 1: taggerSoftware.SetFinalized(); break;
                    case 2: taggerHardware.SetFinalized(); taggerSoftware.SetFinalized(); break;
                }

                worker.ReportProgress(row, System.Windows.Media.Color.FromScRgb(Colors.Blue.ScA, Colors.Blue.ScR, Colors.Blue.ScG, Colors.Blue.ScB));
                

                System.Threading.Thread.Sleep(p.PauseDuration);

            }
        }
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            System.Windows.Media.Color c = (System.Windows.Media.Color)e.UserState;

            int row = e.ProgressPercentage;

            for (int i = row * 6; i < row * 6 + 6; i++)
            {
                SolidColorBrush b = fbrushes[i];

                b.Color = c;
                //tboxes[i].FontSize += 5;
            }
            
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                MessageBox.Show("Cancelled by user.");
            }

            else if (!(e.Error == null))
            {
                MessageBox.Show("Error: " + e.Error.Message + e.Error.Data + e.Error.GetBaseException());
            }

            else
            {
                MessageBox.Show("Done");
            }

            btnStart.IsEnabled = true;
            cbTagMethod.IsEnabled = true;
            btnStart.Content = "Start";
        }

        private void InitializeTable()
        {
            int numberOfColumns = 3;

            for (int x = 0; x < numberOfColumns; x++)
            {

                oTable.Columns.Add(new TableColumn());

                //oTable.Columns[x].Width = new GridLength(130);

            }

            // Create and add an empty TableRowGroup Rows.
            oTable.RowGroups.Add(new TableRowGroup());

            TableRow currentRow;

            //Add Libya data row
        
            oTable.RowGroups[0].Rows.Add(new TableRow());

            currentRow = oTable.RowGroups[0].Rows[0];
            ////Configure the row layout

            //currentRow.Background = System.Windows.Media.Brushes.White;

            //currentRow.Foreground = System.Windows.Media.Brushes.Navy;

            //Add the country name in the first cell

            currentRow.Cells.Add(AddNewObject("A",0));
            currentRow.Cells.Add(AddNewObject("B",1));
            currentRow.Cells.Add(AddNewObject("C",2));
            currentRow.Cells.Add(AddNewObject("D", 3));
            currentRow.Cells.Add(AddNewObject("E", 4));
            currentRow.Cells.Add(AddNewObject("E", 5));

            currentRow = new TableRow();
            oTable.RowGroups[0].Rows.Add(currentRow);

            currentRow.Cells.Add(AddNewObject("G", 6));
            currentRow.Cells.Add(AddNewObject("H", 7));
            currentRow.Cells.Add(AddNewObject("I", 8));
            currentRow.Cells.Add(AddNewObject("J", 9));
            currentRow.Cells.Add(AddNewObject("K", 10));
            currentRow.Cells.Add(AddNewObject("L", 11));

            currentRow = new TableRow();
            oTable.RowGroups[0].Rows.Add(currentRow);

            currentRow.Cells.Add(AddNewObject("M", 12));
            currentRow.Cells.Add(AddNewObject("N", 13));
            currentRow.Cells.Add(AddNewObject("O", 14));
            currentRow.Cells.Add(AddNewObject("P", 15));
            currentRow.Cells.Add(AddNewObject("Q", 16));
            currentRow.Cells.Add(AddNewObject("R", 17));

            currentRow = new TableRow();
            oTable.RowGroups[0].Rows.Add(currentRow);

            currentRow.Cells.Add(AddNewObject("S", 18));
            currentRow.Cells.Add(AddNewObject("T", 19));
            currentRow.Cells.Add(AddNewObject("U", 20));
            currentRow.Cells.Add(AddNewObject("V", 21));
            currentRow.Cells.Add(AddNewObject("W", 22));
            currentRow.Cells.Add(AddNewObject("X", 23));

            currentRow = new TableRow();
            oTable.RowGroups[0].Rows.Add(currentRow);

            currentRow.Cells.Add(AddNewObject("Y", 24));
            currentRow.Cells.Add(AddNewObject("Z", 25));
            currentRow.Cells.Add(AddNewObject("1", 26));
            currentRow.Cells.Add(AddNewObject("2", 27));
            currentRow.Cells.Add(AddNewObject("3", 28));
            currentRow.Cells.Add(AddNewObject("4", 29));

            currentRow = new TableRow();
            oTable.RowGroups[0].Rows.Add(currentRow);

            currentRow.Cells.Add(AddNewObject("5", 30));
            currentRow.Cells.Add(AddNewObject("6", 31));
            currentRow.Cells.Add(AddNewObject("7", 32));
            currentRow.Cells.Add(AddNewObject("8", 33));
            currentRow.Cells.Add(AddNewObject("9", 34));
            currentRow.Cells.Add(AddNewObject("0", 35));

        }

        

        public TableCell AddNewObject(string text,int i)
        {
            TextBlock t = new TextBlock();
            t.Text = text;
            //t.FontSize = 10;
            t.Width = 20;
            t.Height = 20;
            SolidColorBrush scb = new SolidColorBrush(Colors.Blue);
            t.Foreground = scb;

            fbrushes[i] = scb;
            tboxes[i] = t;

            Viewbox vb = new Viewbox();
            vb.Stretch = Stretch.Uniform;
            vb.Child = t;
            
            BlockUIContainer buic = new BlockUIContainer();
            buic.Child = vb;

            TableCell cell = new TableCell();
            cell.Blocks.Add(buic);
            cell.TextAlignment = TextAlignment.Center;

            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("212"))));

            return cell;
        }

        //private void cbTagMethod_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    //if (cbTagMethod.SelectedIndex == 0)
        //    //    taggerHardware = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort);
        //    //else if (cbTagMethod.SelectedIndex == 1)
        //    //    taggerHardware = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.Software);
        //    //else throw new Exception("Tag method not found!");
        //}

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (bw.IsBusy != true)
            {
                p.FlashDuration = Convert.ToInt32(tboxFlashDuration.Text);
                p.PauseDuration = Convert.ToInt32(tboxPauseDuration.Text);
                p.FlashCount = Convert.ToInt32(tboxFlashCount.Text);

                cbTagMethod.IsEnabled = false;
                btnStart.Content = "Cancel";
                bw.RunWorkerAsync(cbTagMethod.SelectedIndex);
            }
            else
            {
                btnStart.IsEnabled = false;
                bw.CancelAsync();
                btnStart.IsEnabled = true;
                cbTagMethod.IsEnabled = true;
                btnStart.Content = "Start";
            }
        }

        private void SetTarget(int i, int j)
        {
            fbrushes[i * 6 + j].Color = System.Windows.Media.Color.FromScRgb(Colors.Red.ScA, Colors.Red.ScR, Colors.Red.ScG, Colors.Red.ScB);
        }
    }
}
