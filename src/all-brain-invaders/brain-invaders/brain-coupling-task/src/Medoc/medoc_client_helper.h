/**
 * Helper for sending frames to a GUI connected to medoc devices
 * Use:
 * int MedocClientDoConnect( int port )
 * int MedocClientDoDisconnect()
 * int MedocClientDoSendFrame( unsigned char* frame, unsigned int size)
 * return values are MEDOC_RESULT_* constants
 */

#ifndef __MEDOC_CLIENT_HELPER_H__
#define __MEDOC_CLIENT_HELPER_H__

// dll loader:
#ifdef unix
#include <dlfcn.h>
typedef void* dllHandle;
#else
#include <objbase.h>
typedef HMODULE dllHandle;
#endif

#include <string>
#include <iostream>

//Getter for the medoc_tagger library
static dllHandle& GetMedocClientLib()
{
    static dllHandle handle = NULL;
    return handle;
}

// Functions getters
typedef int (*ConnectFct)( int );
static ConnectFct& GetConnectFct()
{
    static ConnectFct connect = NULL;
    return connect;
}

typedef int (*DisconnectFct)();
static DisconnectFct& GetDisconnectFct()
{
    static DisconnectFct disconnect = NULL;
    return disconnect;
}

typedef int (*SendFrameFct)( unsigned char*, unsigned int );
static SendFrameFct& GetSendFrameFct()
{
    static SendFrameFct sendFrame = NULL;
    return sendFrame;
}

//Function to load a function from the given dllHandle
template <class FCT>
FCT loadFct( dllHandle handle, const std::string& name, std::string& error )
{
    error = "";
    if ( handle )
    {
        FCT res = (FCT) GetProcAddress( handle, name.c_str() );
        if ( !res )
            error = std::string("[medoc_client] Unable to find entry point ") 
            + name + "\n";
        return res;
    }
    else
    {
        error = "[medoc_client] No driver loaded \n";
        return NULL;
    }
}

//Loads the library medoc_tagger and the useful functions
bool loadLibraryAndFunctions()
{
    bool loaded = true;

    const char* lib = "medoc_client";

    GetMedocClientLib() = 
#ifdef unix 
        dlopen( lib, 0 );
#else
        LoadLibrary( lib );
#endif

    std::string error;

    GetConnectFct() = loadFct<ConnectFct>( GetMedocClientLib(), "MedocClientConnect", error );
    if ( !error.empty() || !GetConnectFct() ) 
    {
        printf(error.c_str());
        loaded = false;
    }

    GetDisconnectFct() = loadFct<DisconnectFct>( GetMedocClientLib(), "MedocClientDisconnect", error );
    if ( !error.empty() || !GetDisconnectFct() ) 
    {
        printf(error.c_str());
        loaded = false;
    }

    GetSendFrameFct() = loadFct<SendFrameFct>( GetMedocClientLib(), "MedocClientSendFrame", error );
    if ( !error.empty() || !GetSendFrameFct() ) 
    {
        printf(error.c_str());
        loaded = false;
    }

    return loaded;
}

int MedocClientDoConnect( int port )
{
    static bool loaded = loadLibraryAndFunctions();

    if ( !GetConnectFct() ) 
    {
        printf( "[medoc_client] Failed to get connect function \n" );
        return -1;
    }

    return (*GetConnectFct())( port );
}

int MedocClientDoDisconnect()
{
    static bool loaded = loadLibraryAndFunctions();

    if ( !GetDisconnectFct() ) 
    {
        printf( "[medoc_client] Failed to get disconnect function \n" );
        return -1;
    }

    return (*GetDisconnectFct())();
}

int MedocClientDoSendFrame( unsigned char* frame, unsigned int size)
{
    static bool loaded = loadLibraryAndFunctions();

    if ( !GetSendFrameFct() ) 
    {
        printf( "[medoc_client] Failed to get send frame function \n" );
        return -1;
    }

    return (*GetSendFrameFct())( frame, size );
}

#endif // __MEASUREMENT_HELPER_H__