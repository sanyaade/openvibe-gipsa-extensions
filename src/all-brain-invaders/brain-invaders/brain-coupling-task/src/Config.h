/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_BCONFIG_H__
#define __OpenViBEApplication_BCONFIG_H__

#include "Communication/OVComm.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
using namespace std;

/*!
* \author Anton Andreev (Gipsa)
* \date 2012-10-27
* \brief Contains all Brain Invaders configuration parameters
* 
*/
class Config
{
  public:

	Config();

	bool LoadConfigurationFile(int argc, char **argv);
	bool LoadConfigurationCmd(int argc, char **argv);
	std::string GetConfiguration();
	void PrintConfiguration();

	eProtocolVersion CommProtocol;

	bool ShuffleEnabled;
	char* ComPortName;

    double FlashNonTargetTime;	//!< Time in seconds a flash should last for non-target.
	double FlashTargetTime;	//!< Time in seconds a flash should last for target.
	//These parameters affect the ISI function that give the time between flashes
	double MinISI; //(seconds)
	double MaxISI; //(seconds)
	double MeanISI; //(seconds)
	
	double PauseTime;	//!< General pause time used in several places

	int PauseBetweenLevels; //!< Time in seconds between levels.
	double PauseAfterDestruction; //!< Time in seconds between reptetitions.
	double PauseBetweenRowsAndColumns; //(in seconds) First all rows and then all columns are displayed

	int Lives;    
	int RepetitionsPerLife; //!< How many repetition to show to the user before getting a result and losing a life
	
	int MaxTrainingRepetitionsPerTarget;
	int MaxTrainingTargets; 

	bool LogToDisk;
	bool LogToConsole;

	char* VrpnPeripheral;
	double VRPNTargetReceivedTimeout;  //!< Max time in which BI is paused, waiting for a target response from OV

	bool ShowOgreSetup;

	bool ISIAdaptationEnabled; 
	bool OGREDebugLoggingEnabled;

	int PercentISIAdapt;

	bool ManageOpenVibeEnabled;

    std::string SavePath;

	int MaxRepetitions(bool isTraining);

private:
	double to_double ( const char *p );
};

#endif