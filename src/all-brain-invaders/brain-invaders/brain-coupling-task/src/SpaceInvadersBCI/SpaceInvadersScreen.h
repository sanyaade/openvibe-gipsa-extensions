/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_SpaceInvadersScreen_H__
#define __OpenViBEApplication_SpaceInvadersScreen_H__

#include "CSpaceInvadersBCI.h"
using namespace BrainInvaders;

#include <Ogre.h>        ////////////////////////difference
#include <OIS.h>
using namespace Ogre;        ////////////////////////difference

namespace BrainInvaders {

	class CSpaceInvadersBCI;

	/*!
	 * \author Gijs van Veen (Gipsa)
	 * \date 2012-03-19
	 * \brief Abstraction of the Screens to be drawn in Brain Invaders.
	 *
	 * \details Abstract class Drawing and Updating a screen in Brain Invaders.
	 * 
	 */
	class SpaceInvadersScreen{

	public:

		SpaceInvadersScreen(CSpaceInvadersBCI *scrMan, SceneManager *mSM);
		
		/**
		* \brief Abstraction of setting the screens visibility.
		* \param isVisible Indicates whether the screen should be visible.
		*/
		virtual void visible(bool isVisible);

		/**
		* \brief Abstraction of updating the Screen.
		* \param timeSinceLastUpdate Time in seconds since last update.
		*/
		virtual bool update(double timeSinceLastUpdate);

		/**
		* \brief Abstraction of handling keyboard events.
		* \param evt The Keyboard event to handle.
		*/
		virtual void keyPressed(const OIS::KeyEvent& evt);

	protected:
		Ogre::SceneManager* sceneManager;							//!< The sceneManager this screen has to draw on.
		BrainInvaders::CSpaceInvadersBCI* screenManager;			//!< The screenManager this screen is handled by.
		

	private:



	};

};
#endif