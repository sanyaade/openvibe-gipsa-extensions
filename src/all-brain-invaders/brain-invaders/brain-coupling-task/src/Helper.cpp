#include "Helper.h"

Helper::Helper()
{
	random_device rseed; //used to generate a single seed number 
	
    m_rng = mt19937(rseed());
}

int* Helper::GenerateRandomNonRepetiveSequnece(int n)
{
	int* numbers = new int[n];
	int* result  = new int[n];

	for (int i=0;i<n;i++)
	{
		*(numbers+i) = i; 
	}

	for (int i=0;i<n;i++)
	{
		int m = rand() % (n - i);
		*(result+i) = numbers[m];

		numbers[m] = numbers[n - i - 1];
	}

	delete[] numbers; 

	return result;
}

double Helper::GenerateRandomNumberExpCustom(double expDistMean)
{
	double x = (double) rand() / (double) RAND_MAX;
	double landa = 1 / expDistMean;
	double result = landa * std::exp(-landa * x);
		
	return result;
}

double Helper::GenerateRandomNumberExpBuiltIn(double expDistMean)
{
    double const exp_dist_lambda = 1 / expDistMean;

	exponential_distribution<> exp_disp = exponential_distribution<>(exp_dist_lambda);

    double result = exp_disp(m_rng);

	return result;
}

double Helper::GenerateRandomNumberUniformZeroOne()
{
    uniform_real_distribution<double> uniform_dist = uniform_real_distribution<double>(0.0,1.0);

	double result = uniform_dist(m_rng);

	return result;
}

double Helper::GenerateRandomNumberUniform(int max)
{
	uniform_int_distribution<int> uniform_dist = uniform_int_distribution<int>(0,max);

	double result = uniform_dist(m_rng);

	return result;
}

void Helper::delay(int milliseconds)
{
	boost::this_thread::sleep(boost::posix_time::milliseconds(milliseconds));
}

std::string Helper::GetCurrentDateTime()
{
  using namespace boost;

  std::stringstream msg;
  const boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
  boost::posix_time::time_facet* const f=new boost::posix_time::time_facet("%H:%M:%S");//%d-%b-%Y %H:%M:%S
  
  msg.imbue(std::locale(msg.getloc(),f));

  msg << now;
  string result = msg.str();
  return result;
}

std::string Helper::GetEnvVariable(string name)
{
   #include <stdlib.h>

   char *tmp;

   tmp = getenv( name.c_str() );

   string result="";
   if( tmp != NULL )
   {
      result = tmp;
   }

   return result;
}
