/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "WinLooseScreen.h"
#include "CSpaceInvadersBCI.h"
#include "GameScreen.h"
using namespace BrainInvaders;

#include <Ogre.h>
#include <OgreFont.h>
#include <OgreFontManager.h>
#include <CEGUI.h>
#include <OIS.h>
using namespace Ogre;

#include <stdlib.h>
#include <stdlib.h> 

WinLooseScreen::WinLooseScreen(CSpaceInvadersBCI *ScrMan, SceneManager *mSM, CEGUI::WindowManager *mWM, CEGUI::Window *m_poSheet, GameScreen *GS, int pauseBetweenLevels) 
	: SpaceInvadersScreen(ScrMan, mSM),
	m_pauseBetweenLevels(pauseBetweenLevels)
{
	m_poGUIWindowManager = mWM;
	gameScreen = GS;

	const std::string l_sWonMenu = "Resources/GUI/brainInvadersWon.png";
	const std::string l_sLostMenu = "Resources/GUI/brainInvadersLost.png";

	//----------- CREATE WINDOWS -------------//
	// background Won
	CEGUI::Window * l_poWonMenu  = m_poGUIWindowManager->createWindow("TaharezLook/StaticImage", "WonMenu");
	l_poWonMenu->setPosition(CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)) );
	l_poWonMenu->setSize(CEGUI::UVector2(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));
	m_poSheet->addChildWindow(l_poWonMenu);	
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageWonMenu",l_sWonMenu); 
	l_poWonMenu->setProperty("Image","set:ImageWonMenu image:full_image");
	l_poWonMenu->setProperty("FrameEnabled","False");
	l_poWonMenu->setProperty("BackgroundEnabled","False");

	// background Lost
	CEGUI::Window * l_poLostMenu  = m_poGUIWindowManager->createWindow("TaharezLook/StaticImage", "LostMenu");
	l_poLostMenu->setPosition(CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)) );
	l_poLostMenu->setSize(CEGUI::UVector2(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));
	m_poSheet->addChildWindow(l_poLostMenu);	
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageLostMenu",l_sLostMenu); 
	l_poLostMenu->setProperty("Image","set:ImageLostMenu image:full_image");
	l_poLostMenu->setProperty("FrameEnabled","False");
	l_poLostMenu->setProperty("BackgroundEnabled","False");
}



void WinLooseScreen::visible(bool visible)
{
	// If this is a Win screen, show the win version
	m_poGUIWindowManager->getWindow("WonMenu")->setVisible(visible && winScreen);
	// If not, show the loose version.
	m_poGUIWindowManager->getWindow("LostMenu")->setVisible(visible && !winScreen);
	loadTime.reset();
	startedLoading = false;
	skipScreen = false;
}

//loads next level and switches back to the game
bool WinLooseScreen::update(double timeSinceLastUpdate)
{
	// Start loading the level.
	if (!startedLoading)
	{
		startedLoading = true;
		gameScreen->FinishLevel(); //loads next level
	}
	
	// Once done, wait if the screen has not been displayed long enough (so players can relax)
	if (loadTime.getMilliseconds() > m_pauseBetweenLevels || skipScreen)
	{
		startedLoading = false;
		skipScreen = false;

		screenManager->activateScreen(CSpaceInvadersBCI::MainGame); //switch back to game (next level loaded in finishLevel())
	}
	return true;
}

void WinLooseScreen::keyPressed(const OIS::KeyEvent& evt){
	if ( evt.key == OIS::KC_SPACE){
		// If space is hit during loading (and we are done) we start
		skipScreen = true;
	}
}