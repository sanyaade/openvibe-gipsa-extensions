/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "ProgressBar.h"
using namespace BrainInvaders;

#include <string>
#include <sstream>
using namespace std;

ProgressBar::ProgressBar(Ogre::SceneManager *mSM, int maxRepetitions) : m_maxRepetitions(maxRepetitions)
{
	sceneManager = mSM;
	barNode = sceneManager->getRootSceneNode()->createChildSceneNode("ProgressBarNode", Ogre::Vector3(0,0,0));
	
	// Create nodes for the maximum amount of repetitions allowed.
	for (int i = 0; i < m_maxRepetitions; i++)
	{
		//std::cout << "Repet " << i << std::endl;
		std::stringstream ss;
		ss << "ProgressBarNode" << i;
		SceneNode* tempNode = barNode->createChildSceneNode(ss.str(), Ogre::Vector3(0,0,0));
		ss.clear();
		ss << "ProgressBarElement" << i;
		barDots[i] = sceneManager->createEntity(ss.str(), "cube.mesh");
		tempNode->attachObject(barDots[i]);
	}
	//std::cout << "Done" << std::endl;
}

ProgressBar::~ProgressBar()
{
	// Destroy all children, then this.
	barNode->removeAndDestroyAllChildren();
	sceneManager->destroySceneNode(barNode);
}

void ProgressBar::reset(int repetitions)
{
	// Reset to the next max amound of repetitions.
	m_maxRepetitions = repetitions;
	m_currentRepetition = 0;
	// We want to have the elements spread across an equal distance independent of the amount of repetitions, so this spacing is calculated.
	int spacing = 1000/m_maxRepetitions;
	
	for (int i = 0; i < m_maxRepetitions; i++)
	{
		std::stringstream ss;
		ss << "ProgressBarNode" << i;
		SceneNode* tempNode = sceneManager->getSceneNode(ss.str());
		// Place all the nodes within the max amound in the line of the spacing
		if (i < m_maxRepetitions){
			tempNode->setPosition(Ogre::Vector3(1200 - i*spacing, -900, 0));
			tempNode->setScale(0.5,0.5,0.5);
			tempNode->setVisible(true);
			barDots[i]->setMaterialName("Spaceinvader/FullDot");
		}
		// Make the rest invisible.
		else{
			barDots[i]->setMaterialName("Spaceinvader/EmptyDot");
			tempNode->setVisible(false);
		}
	}
}

void ProgressBar::visible(bool isVisible)
{	
	// Only change the visibility for the elements that can be showed anyway (the rest should be automatically set to invisible)
	for (int i = 0; i < m_maxRepetitions; i++)
	{
		std::stringstream ss;
		ss << "ProgressBarNode" << i;
		SceneNode* tempNode = sceneManager->getSceneNode(ss.str());
		tempNode->setVisible(isVisible);
	}
}

void ProgressBar::nextRepetition(bool validShot){
	// Make one of the elements empty, since the repetition is done.
	if (m_currentRepetition < m_maxRepetitions)
	{
		if (validShot)
			barDots[m_currentRepetition]->setMaterialName("Spaceinvader/EmptyDot");
		else
			barDots[m_currentRepetition]->setMaterialName("Spaceinvader/InvalidDot");
		
		m_currentRepetition++;
	}
}