/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "KeyboardScreen.h"
#include "CSpaceInvadersBCI.h"
#include "AlienBlock.h"
using namespace BrainInvaders;

#include <Ogre.h>
#include <OgreFont.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>
using namespace Ogre;

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sstream>
#include <time.h>
#include <math.h>
#include <vector>
//#include <system/Time.h> //for sleep function
using namespace std;

#include "../tinyxml/tinyxml.h"

KeyboardScreen::KeyboardScreen(CSpaceInvadersBCI *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config) : 
           BrainInvadersScreen(ScrMan, mSM, vrpnPeripheral, config)

{
	LoadAssets();
}

void KeyboardScreen::LoadAssets()
{
	//Loading in all sprites beforehand makes sure animations dont lag.
	Entity *tempEntity = sceneManager->createEntity("temp","cube.mesh");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_1_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_1_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_1_F");
	sceneManager->destroyEntity("temp");
}

bool KeyboardScreen::update(double timeSinceLastUpdate)
{
	return true;
}

void KeyboardScreen::visible(bool isVisible)
{

}


void KeyboardScreen::keyPressed(const OIS::KeyEvent& evt)
{

}