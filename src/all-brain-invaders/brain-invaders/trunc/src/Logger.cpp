#include "Logger.h"
#include "Helper.h"

Config Logger::m_config;
std::ofstream Logger::gameLog;

void Logger::Initialize(Config config)
{
	m_config = config;

	if (m_config.LogToDisk)
	{
		if (m_config.SavePath!="") 
			gameLog.open(m_config.SavePath + "\\GameSession.txt"); 
		else gameLog.open("GameSession.txt");
	}
}

void Logger::LogEvent(std::string eventInfo,bool toFile, bool toConsole)
{
	//File
	if (toFile && m_config.LogToDisk && gameLog.is_open())
	  gameLog << 
	  "[" << Helper::GetCurrentDateTime() << "] BI: " << 
	  eventInfo << endl;

	//Console
	if (m_config.LogToConsole && toConsole)
	{
	   #if defined TARGET_OS_Windows
	       HANDLE chandle = GetStdHandle(STD_OUTPUT_HANDLE);
	       cout << Helper::setcolour(Helper::GREEN, chandle) 
		   << "[" << Helper::GetCurrentDateTime() << "] BI: " 
		   << Helper::setcolour(Helper::WHITE, chandle) 
		   << eventInfo << endl;
       #else
	        cout << 
			"[" << Helper::GetCurrentDateTime() << "] BI: " <<
			eventInfo << endl;
       #endif
	}
}

void Logger::Close()
{
	if (m_config.LogToDisk && gameLog.is_open())
		gameLog.close();
}
