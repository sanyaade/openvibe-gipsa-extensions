/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "SpaceInvadersBCI/BrainInvadersApplication.h"
#include "Communication/OVComm.h"
#include "Config.h"
#include "Helper.h"

void PrintLogo()
{
	printf("BRAIN INVADERS application started ! \n\n");
	printf("\n");
	printf("                                       \n");
    printf("             ##             ###        \n");
    printf("             ##             ###        \n");
    printf("               ###        ##           \n");
    printf("               ###        ##           \n");
    printf("             ##################        \n");
    printf("             ##################        \n");
    printf("          #####   ########  ######     \n");
    printf("       :::#####:::########::######::   \n");
    printf("       #############################   \n");
    printf("       ###   ##################   ##   \n");
    printf("       ###   ##################   ##   \n");
    printf("       ###   ##             ###   ##   \n");
    printf("       ###   ##             ###   ##   \n");
    printf("               #####�  #####           \n");
    printf("               #####�  #####           \n");
    printf("               ������  �����           \n");
    printf("               ������  �����           \n");
    printf("       ���   ��             ���   ��   \n");
    printf("       ���   ������������������   ��   \n");
    printf("       �����������������������������   \n");
    printf("          �����   ��������  ������     \n");
	printf("                                       \n");
    printf("                                       \n");
    printf("                 �#########            \n");
    printf("                 �#########            \n");
    printf("           #######################     \n");
    printf("         �.#######################��   \n");
    printf("        ############################   \n");
    printf("        #######     #####    #######   \n");
    printf("        #######     #####    #######   \n");
    printf("        ############################   \n");
    printf("        ############################   \n");
    printf("               #####     ####          \n");
    printf("             ::##:��:::::��##:::       \n");
    printf("             ####.  #####  #####       \n");
    printf("        #####                   ####   \n");
    printf("        #####                   ####   \n");
    printf("        �����                   ����   \n");
    printf("        �����                   ����   \n");
    printf("             ����   �����  �����       \n");
    printf("               �����     ����          \n");
    printf("        ����������������������������   \n");
    printf("    ������           �������     ����  \n");
	printf("                                       \n");
    printf("                                       \n");
	printf("                  ######               \n");
    printf("                  ######               \n");
    printf("               ###########�            \n");
    printf("               ###########�            \n");
    printf("             #################         \n");
    printf("             #################         \n");
    printf("          ######   ######   #####      \n");
    printf("          ######   ######   #####      \n");
    printf("          #######################      \n");
    printf("          #######################      \n");
    printf("                ###      ##�           \n");
    printf("                ###      ##�           \n");
    printf("             ###   ######   ##         \n");
    printf("             ###   ######   ##         \n");
    printf("          ###   ###      ##�  ###      \n");
    printf("          ###   ###      ##�  ###      \n");
    printf("          ���   ���      ���  ���      \n");
    printf("          ���   ���      ���  ���      \n");
    printf("             ���   ������   ��         \n");
    printf("                ���      ���           \n");
    printf("                ���      ��            \n");
    printf("       �������         ��������������� \n");
	printf("\n");
}

Config GetConfiguration(int argc, char **argv)
{
	Config BConfig;

	bool success = BConfig.LoadConfigurationFile(argc,argv);

	if (!success)
	{
		printf("Processing configuration file has failed! Using built-in default values.\n");
	}

	return BConfig;
}

int main(int argc, char **argv)
{
	//PrintLogo();

	Config BConfig = GetConfiguration(argc, argv);

	std::cout << "\nInitializing Brain Invaders...\n";
	std::cout << "Version: " << BConfig.Version << "\n";
	std::cout << "Release notes : new version compatible with OpenVibe 1.3 and the new versions of Ogre and CEGUI.\n";
	std::cout << "Home page: https://bitbucket.org/toncho11/openvibe-gipsa-extensions\n\n";

	BConfig.PrintConfiguration();

	OgreApplication * app = new BrainInvadersApplication(BConfig);

	std::cout << endl;
	Helper::DoConsolePause();
	
	//start 3d engine
	app->go();

	delete app;

	return 0;
}