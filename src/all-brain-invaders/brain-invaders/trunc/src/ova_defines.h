#ifndef __OpenViBEApplication_Defines_H__
#define __OpenViBEApplication_Defines_H__

//___________________________________________________________________//
//                                                                   //
// Build type identification                                         //
//___________________________________________________________________//
//                                                                   //

// #define OVK_BUILDTYPE_Debug
// #define OVK_BUILDTYPE_Release

#if defined TARGET_BUILDTYPE_Debug
 #define OVA_BUILDTYPE_Debug
#elif defined TARGET_BUILDTYPE_Release
 #define OVA_BUILDTYPE_Release
#else
 #pragma "No build type defined !"
#endif

//___________________________________________________________________//
//                                                                   //
// NULL Definition                                                   //
//___________________________________________________________________//
//                                                                   //

#ifndef NULL
	#define NULL 0
#endif

#endif // __OpenViBEApplication_Defines_H__
