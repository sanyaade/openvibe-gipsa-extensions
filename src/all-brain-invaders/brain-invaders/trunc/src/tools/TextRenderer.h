///* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
// * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
// * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
// * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
// * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
// 
// * This file is part of Brain Invaders.
// * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/
//
//#include <Ogre.h>
//
//#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
//	#include "Overlay/OgreOverlayManager.h"
//#endif
//
//#include <OgreSingleton.h>
// 
//class TextRenderer : public Ogre::Singleton<TextRenderer>
//{
//private:
// 
//    Ogre::OverlayManager*    _overlayMgr;
//    Ogre::Overlay*           _overlay;
//    Ogre::OverlayContainer*  _panel;
// 
//public:
// 
//    TextRenderer();
//    ~TextRenderer();
// 
//    void addTextBox(
//        const std::string& ID,
//        const std::string& text,
//        Ogre::Real x, Ogre::Real y,
//        Ogre::Real width, Ogre::Real height,
//        const Ogre::ColourValue& color = Ogre::ColourValue(1.0, 1.0, 1.0, 1.0));
// 
//    void removeTextBox(const std::string& ID);
// 
//    const std::string& getText(const std::string& ID);
//	
//	void setText(const std::string& ID, const std::string& Text);
//	void setWidth(const std::string& ID, Ogre::Real w);
//	void setHeight(const std::string& ID, Ogre::Real h);
//	void setCharHeight(const std::string& ID, const std::string& CharHeight);
//    void setPosition(const std::string& ID, Ogre::Real x, Ogre::Real y);
//	void setColor(const std::string& ID, const Ogre::ColourValue& color);
//	void setHorAlign(const std::string& ID, const std::string& align);
//};