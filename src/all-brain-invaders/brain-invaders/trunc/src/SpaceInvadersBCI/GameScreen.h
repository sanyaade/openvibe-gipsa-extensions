/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_GameScreen_H__
#define __OpenViBEApplication_GameScreen_H__

#include "SpaceInvadersScreen.h"
#include "BrainInvadersApplication.h"

#include "Alien.h"
#include "AlienBlock.h"
#include "SoundManager.h"
#include "ProgressBar.h"
#include "../Config.h"
#include "../Helper.h"
#include "BrainInvadersScreen.h"
#include <random>
using namespace BrainInvaders;

#include <Ogre.h>
#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	#include "Overlay/OgreOverlayManager.h"
	#include "Overlay/OgreFontManager.h"
#endif

#define MAXMATRIXSIZE 25
#define MAXBLOCKS 36

namespace BrainInvaders {

	/*!
	 * \author Gijs van Veen (Gipsa), Anton Andreev (Gipsa)
	 * \date 2012-03-19
	 * \brief The Game Screen of Brain Invaders.
	 *
	 * \details In this Game Screen, all the elements of the actual Game are managed. 
	 * This is also where game logic is processed.
	 * 
	 */
	class GameScreen : public BrainInvadersScreen
	{

	public:

		GameScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config);
		~GameScreen();

		void LoadAssets();

		/**
		* \brief Updates the Game Screen, handling all Game logic and Managing Flashing and BCI input.
		* \return indicates whether to continue.
		*/
		virtual bool update(double timeSinceLastUpdate);

		/**
		* \brief Sets the screens visibility.
		* \param isVisible Indicates whether the screen should be visible.
		*/
		virtual void visible(bool isVisible);

		/**
		* \brief Handles Keyboard events occuring in this screen.
		* \param evt The Keyboard Event to be processed.
		*/
		virtual void keyPressed(const OIS::KeyEvent& evt);

		bool ShootAlien();

		/**
		* \brief Starts a new game 
		*/
		void Initialize(bool isTraining);

		/**
		* \brief Finishes a level and if possible loads the next one. If no more levels then calls FinishGame() 
		*/
		void FinishLevel();

		/**
		* \brief Prepares a new level
		* \param The number of the level.
		* \return true if level correctly loaded.
		*/
		bool ProcessLevel(int level,state gameState);

		/**
		* \brief Constructs the overlay (GUI) of the game screen.
		*/
		void SetUpOverlay();

		void UpdateScoreText(std::string text);

		std::string GetScoreText();

	private:

		ProgressBar* progressBar;									//!< Bar that shows the progress of a level. Part of the overlay.
		bool m_bIsTargetShot;											//!< Used to carry information between two states.
		bool m_bIsScoreTextInitialized;								//used to overcome a bug in Ogre 

		void FixLayout(bool show);
	};
};
#endif