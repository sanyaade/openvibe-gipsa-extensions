/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "BrainInvadersApplication.h"

#include "GUIScreen.h"
#include "GameScreen.h"
#include "WinLooseScreen.h"
#include "SpaceInvadersScreen.h"
#include "KeyboardScreen.h"
using namespace BrainInvaders;

#include <Ogre.h>
#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	#include <Overlay\OgreFont.h>	
	#include <Overlay\OgreFontManager.h>
	#include <Overlay\OgreOverlayElement.h>
#else
	#include <OgreFont.h>
	#include <OgreFontManager.h>
	#include <OgreTextAreaOverlayElement.h>
#endif
using namespace Ogre;

#include <stdio.h>
#include <stdlib.h> 
#include <iostream>
using namespace std;

#include "../Logger.h"

//Contains the code to specific to Brain Invaders. You can call it a "screen manager".

BrainInvadersApplication::BrainInvadersApplication(Config config) : OgreApplication(config)
{
	Logger::Initialize(config);

	cout<<"Started setting communication mode...\n";

	// Set the correct communication protocol to OpenVibe
    communicationHandler = new OVComm((eProtocolVersion)m_config.CommProtocol,m_config.ComPortName);

	if ((m_config.CommProtocol == SerialPort) || (m_config.CommProtocol == EightbitPP))
	{
		//set numeric codes
		communicationHandler->ConfigureFlashBaseRowCode(20);
        communicationHandler->ConfigureFlashBaseColumnCode(40);

        communicationHandler->ConfigureTargetBaseRowCode(60);
        communicationHandler->ConfigureTargetBaseColumnCode(80);

        communicationHandler->ConfigureExperimentStartCode(100);
        communicationHandler->ConfigureTrainingStartCode(101);
        communicationHandler->ConfigureRepetionCompletedCode(102);
		communicationHandler->ConfigureResetCode(103);
	}
	else if (m_config.CommProtocol == SoftwareTagging)
	{
		//The following stimulation codes are copied from OpenVibe because the current scenario expects them

		#define OVTK_StimulationId_ExperimentStart                   0x00008001
		#define OVTK_StimulationId_ExperimentStop                    0x00008002
		#define OVTK_StimulationId_LabelEnd                          0x000081ff

		#define OVTK_StimulationId_Label_00                          0x00008100
		#define OVTK_StimulationId_Label_10                          0x00008110

		#define OVTK_StimulationId_Beep                              0x00008202

		//These two are defined for BI
		#define BI_Target_Row                                        0x00008120 //user defined
		#define BI_Target_Column                                     0x00008130 //user defined

		//Set stimulation codes which is different a bit different from numeric codes when doing hardware tagging
		//stimulation codes set here must be processed in OpenVibe, so that OpenVibe can detect matrix size and when target is flashed 
		communicationHandler->ConfigureFlashBaseRowCode     (OVTK_StimulationId_Label_00);
        communicationHandler->ConfigureFlashBaseColumnCode  (OVTK_StimulationId_Label_10);

        communicationHandler->ConfigureTargetBaseRowCode    (BI_Target_Row);
        communicationHandler->ConfigureTargetBaseColumnCode (BI_Target_Column);

        communicationHandler->ConfigureExperimentStartCode  (OVTK_StimulationId_ExperimentStart);
        communicationHandler->ConfigureTrainingStartCode    (OVTK_StimulationId_ExperimentStop);
        communicationHandler->ConfigureRepetionCompletedCode(OVTK_StimulationId_LabelEnd);

		communicationHandler->ConfigureResetCode(OVTK_StimulationId_Beep);
	}

	cout<<"Done setting and initializing communication mode.\n";

	resetTimer = true;
}

void BrainInvadersApplication::ExitGame()
{
	Logger::Close();

	m_bContinue = false;
}

void BrainInvadersApplication::StartGame(bool isTraining)
{
	if (isTraining)
	{
	     Logger::LogEvent("Started Training",true,true);//when you press "T"	 
	}
	else
	{
		if (resetTimer)
			((GameScreen *)screens[MainGame])->ResetTimer();
		
		resetTimer = false;
		Logger::LogEvent("Started Online Gaming",true,true);//when you press "space"
	}

	//Sends a flag to OpenVibe to notify that BI is functional
	communicationHandler->SetStartExperiment();
	Helper::delay(50);//System::Time::sleep(50);
	communicationHandler->SetFinalized();
	Helper::delay(50);//System::Time::sleep(50);
	
	// Start the game
	((GameScreen *)screens[MainGame])->Initialize(isTraining);
	activateScreen(MainGame);

	m_StartGameTime = boost::posix_time::microsec_clock::local_time( );
}

void BrainInvadersApplication::LeaveGame()
{
	//Show the main menu.
	activateScreen(GUI);
}

void BrainInvadersApplication::FinishGame()
{
	//start logging
	Logger::LogEvent("Game Finished",true,true);
	boost::posix_time::ptime end = boost::posix_time::microsec_clock::local_time( );
    boost::posix_time::time_duration elapsed = end - m_StartGameTime;

	std::stringstream ss;
	ss << "Time of last game session (in seconds): " << double(elapsed.total_milliseconds() / double(1000));
	Logger::LogEvent(ss.str(),true,true);
	//end logging

	// Reset the game to regular mode (modes: training and online(regular))
	((GameScreen *)screens[MainGame])->Initialize(false);

	// We want to see the Main Menu at the end of the game.
	activateScreen(GUI);
}

void BrainInvadersApplication::ActivateLoadScreen(bool isWonScreen, int m_scoreP1, int m_scoreP2)
{
	std::stringstream ss; //load local variable ss for console output
	ss << "old P1 score: " << ((WinLooseScreen *)screens[Loading])->m_scoreP1new;
	// Show a load screen, which varies based on whether the previous level was won or not.

	((WinLooseScreen *)screens[Loading])->m_isLastGameWon = isWonScreen;
	
	((WinLooseScreen *)screens[Loading])->m_scoreP1new = m_scoreP1;// MODIFICATION ADDED TO DISPLAY THE SCORE ON THE LOADING SCREEN
	ss << ",new: " << ((WinLooseScreen *)screens[Loading])->m_scoreP1new;
	ss << ". old P2 score: " << ((WinLooseScreen *)screens[Loading])->m_scoreP2new;
	((WinLooseScreen *)screens[Loading])->m_scoreP2new = m_scoreP2;// MODIFICATION ADDED TO DISPLAY THE SCORE ON THE LOADING SCREEN
    
	activateScreen(Loading);
	
	ss << ". new: " << ((WinLooseScreen *)screens[Loading])->m_scoreP2new;
	Logger::LogEvent(ss.str(),true,true);
}

// load 3D scene
bool BrainInvadersApplication::initialise()
{
	//Adjust Resolution
	// lights
	try
	{
		m_poSceneManager->setAmbientLight(ColourValue(1, 1, 1));

		Ogre::Light* l_poLight1 = m_poSceneManager->createLight("Light1");
		l_poLight1->setType(Light::LT_POINT);
		l_poLight1->setPosition(Vector3(250, 150, 250));
		l_poLight1->setSpecularColour(ColourValue::White);
		l_poLight1->setDiffuseColour(ColourValue::White);

		// camera
		m_poCamera->setNearClipDistance(5);
		m_poCamera->setProjectionType(PT_ORTHOGRAPHIC);
		m_poCamera->setPosition(Vector3(625, -450, 200));
		m_poCamera->setDirection(Vector3(0,0,-1));

		//Screens
		loadScreens();
		activateScreen(GUI);//activates the menus and visualize it
	}
	catch(Ogre::Exception ex)
	{
		std::cerr<<"[Brain Invaders FAILED] Ogre error: " << ex.getFullDescription() <<std::endl;
		m_bContinue = false;
		return false;
	}
	catch(std::exception ex)
	{
		std::cerr<<"[Brain Invaders FAILED] Error: " << ex.what() <<std::endl;
		m_bContinue = false;
		return false;
	}

	// Make sure the parallel Port is not sending any data other than 0
	communicationHandler->SetFinalized();

	m_bContinue = true;

	return true;
}

void BrainInvadersApplication::loadScreens()
{
	cout << "Loading screens ..." <<endl;

	// Create all the screens used by the game.
	GUIScreen* gui = new GUIScreen(this, m_poSceneManager, m_poGUIWindowManager, m_poSheet);
	screens[GUI] = gui;
	
	GameScreen* game = new GameScreen(this, m_poSceneManager, m_poVrpnPeripheral, this->m_config);
	screens[MainGame] = game;

	WinLooseScreen* winLoose = new WinLooseScreen(this, m_poSceneManager, m_poGUIWindowManager, m_poSheet, game, this->m_config.PauseBetweenLevels);
	screens[Loading] = winLoose;

	KeyboardScreen* keybaord = new KeyboardScreen(this, m_poSceneManager, m_poVrpnPeripheral, this->m_config);
	screens[Keyboard] = keybaord;

	//here add new game screen e.x. "Options"
}

//makes sure that only one screen is visible 
void BrainInvadersApplication::activateScreen(int toActivate)
{
	// Hide all the screens except for the one specified.
	for (int i = 0; i < SCREEN_NUM; i++)
	{
		if (i == toActivate)
			screens[i]->visible(true);
		else
			screens[i]->visible(false);
	}
	currentScreen = toActivate;
}

bool BrainInvadersApplication::keyPressed(const OIS::KeyEvent& evt)
{

	if ( evt.key == OIS::KC_1)
	{
		m_config.MM_IsMultiplayer = true;
		cout<< "Changing multi-player mode to: cooperative, same target." << endl;
		this->m_config.MM_IsCooperative = true;
		this->m_config.MM_IsSameTarget = true;
	}
	else
	if ( evt.key == OIS::KC_2)
	{
		m_config.MM_IsMultiplayer = true;
		cout<< "Changing multi-player mode to: cooperative, not same target." << endl;
		this->m_config.MM_IsCooperative = true;
		this->m_config.MM_IsSameTarget = false;
	}
	else
	if ( evt.key == OIS::KC_3)
	{
		m_config.MM_IsMultiplayer = true;
		cout<< "Changing multi-player mode to: competitive, same target." << endl;
		this->m_config.MM_IsCooperative = false;
		this->m_config.MM_IsSameTarget = true;
	}
	else
	if ( evt.key == OIS::KC_4)
	{
		m_config.MM_IsMultiplayer = true;
		cout<< "Changing multi-player mode to: competitive, not same target." << endl;
		this->m_config.MM_IsCooperative = false;
		this->m_config.MM_IsSameTarget = false;
	}
	

	// Send key event to the active screen.
	screens[currentScreen]->keyPressed(evt);
	return true;
}


// called by OV Ogre engine
bool BrainInvadersApplication::process(double timeSinceLastProcess)
{
	// Update the current screen.
	screens[currentScreen]->update(timeSinceLastProcess);

	return m_bContinue;
}