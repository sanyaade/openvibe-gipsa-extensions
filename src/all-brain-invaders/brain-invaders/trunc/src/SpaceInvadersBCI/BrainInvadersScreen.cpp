/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "BrainInvadersScreen.h"
#include "BrainInvadersApplication.h"
#include "AlienBlock.h"
using namespace BrainInvaders;

#include <Ogre.h>
#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
#include <Overlay\OgreFont.h>	
#include <Overlay\OgreFontManager.h>
#include <Overlay\OgreOverlayElement.h>
#else
#include <OgreFont.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>
#endif
using namespace Ogre;

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sstream>
#include <time.h>
#include <math.h>
#include <vector>
using namespace std;

#include "../tinyxml/tinyxml.h"
#include "../Logger.h"


BrainInvadersScreen::BrainInvadersScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config) : 
           SpaceInvadersScreen(ScrMan, mSM), m_config(config)
{
	Logger::LogEvent("BrainInvadersScreen Constructor",true,false);

	m_score = 0;
	m_scorePlayer2 = 0;

	m_totalLevelsPlayed = 0;

	m_isFirstTargetDestroyedCoop2Targets = false;

	CleanMatrices();

	srand (GetTimeInMilliSeconds());

	m_poVrpnPeripheral = vrpnPeripheral;

	//load the rest
	soundManager = new SoundManager();
}

BrainInvadersScreen::~BrainInvadersScreen()
{
	// Delete all elements in the matrix
	for(int i = 0; i < MAXBLOCKS; i++){
		if(alienBlocks[i] != NULL)
			delete alienBlocks[i];
	}

	// Reset the memory of the matrix
	CleanMatrices();

	// Delete remaining classes.
	delete soundManager;
}

int BrainInvadersScreen::GetTimeInMilliSeconds()
{
	int seconds = time(NULL);
	int milli = ((clock() % CLOCKS_PER_SEC) * 1000) / CLOCKS_PER_SEC;
	return (seconds * 1000) + milli;
}

bool BrainInvadersScreen::GenerateFlashSequence()
{
	m_vFlashSequence.clear();

	int actualRowsCount    = this->alienMatrixActualRowsCount();
	int actualColumnsCount = this->alienMatrixActualColumnsCount();

	//cout<< "actualRowsCount="   << actualRowsCount << endl;
	//cout<< "actualColumnsCount" << actualColumnsCount << endl;

	int* randomRows    = Helper::GenerateRandomNonRepetiveSequnece(actualRowsCount);
	int* randomColumns = Helper::GenerateRandomNonRepetiveSequnece(actualColumnsCount);

	for (int i=0;i<actualRowsCount;i++)
	{
		m_vFlashSequence.push_back(std::pair<int,char>(randomRows[i],'r'));
		//cout << "r " << randomRows[i];
	}

	//cout<<endl;

	for (int i=0;i<actualColumnsCount;i++)
	{
		m_vFlashSequence.push_back(std::pair<int,char>(randomColumns[i],'c'));
		//cout << "c " << randomColumns[i];
	}

	delete[] randomRows;
	delete[] randomColumns;

	//cout << "\nm_vFlashSequence" << m_vFlashSequence.size() << endl;

	return true;
}

std::deque<std::pair<int,int> > BrainInvadersScreen::GenerateTargetSequence(int count)
{
	Logger::LogEvent("Start generating sequence.",false,false);
	
	std::deque<std::pair<int,int> > output;

	for (int i=0; i<count; i++)
	{
		int column=rand() % alienMatrixActualColumnsCount();
		int row=rand() %    alienMatrixActualRowsCount();

		output.push_back(std::pair<int,int>(column,row));

		/*std::stringstream ss;
		ss << "column: " << column << "row: " << row;
		Logger::LogEvent(ss.str(),true,true);*/
	}
	Logger::LogEvent("End generating sequence.",false,false);

	return output;
}

bool BrainInvadersScreen::LoadLevel(int level)
{
	// This function is pretty hardcoded, meaning that an inproperly declared level will probably crash the program. Can be fixed by a LOT of null-checks making this function at least twice as long. Sort of TODO
	// In essence, the function parses through an XML document and loads all elements from it.
	// For specification of XML document, see documentation outside of code.

	std::stringstream ss;
	ss << "Load new level: " << level;
	Logger::LogEvent(ss.str(),true,true);
	
	std::stringstream ss_level_location;
	
	// Select the right level. For Training and Multiplayer we use the same one. For Online we select from 1 to 12.
	if (m_isTrainingMode || this->m_config.MM_IsMultiplayer)
		ss_level_location << "Resources/game/Levels/BILevelTraining.xml";
	else
		ss_level_location << "Resources/game/Levels/BILevel" << level << ".xml";

	TiXmlDocument doc(ss_level_location.str().c_str());

	// Try to load the level.
	if(doc.LoadFile()){
		
		Logger::LogEvent(ss.str(),true,false);
		
		//m_currentLevel = level;
		
		TiXmlElement* currentElement = doc.FirstChild("Level")->FirstChild("TotalBlocks")->ToElement();
		// We need the number of blocks for parsing purposes
		int numOfBlocks = atoi(currentElement->GetText());
		// Load in level settings (mostly speed related)
		double startSpeed = strtod(currentElement->NextSibling("StepSpeed")->ToElement()->GetText(),NULL);
		double speedIncrease = strtod(currentElement->NextSibling("SpeedIncrease")->ToElement()->GetText(),NULL);
		double maxSpeed = strtod(currentElement->NextSibling("MaxSpeed")->ToElement()->GetText(),NULL);
		
		// Process each block.
		for (int i = 0; i < numOfBlocks && i < MAXBLOCKS; i++){
			currentElement = currentElement->NextSibling("Block")->ToElement();
			TiXmlElement* blockElement = currentElement->FirstChild("TotalAliens")->ToElement();
			// We need to know how many aliens we will encounter.
			int totalAliens = atoi(blockElement->GetText());

			// Construct the path for this block
			TiXmlElement * pathElement = blockElement->NextSibling("Path")->FirstChild("NumOfSteps")->ToElement();
			
			// Lets keep track of the path length.
			int pathLength = atoi(pathElement->GetText());
			
			int path[MAXPATHSIZE][2];
			// Contruct the Path
			for(int j = 0; j < MAXPATHSIZE; j++)
			{
				// If we're still within path length, then there must be a step in the XML which we can parse.
				if (j < pathLength){
					pathElement = pathElement->NextSibling("Step")->ToElement();
					path[j][0] = atoi(pathElement->FirstChild("X")->ToElement()->GetText());
					path[j][1] = atoi(pathElement->FirstChild("Y")->ToElement()->GetText());
				}
				else{
					path[j][0] = -1;
					path[j][1] = -1;
				}
			}
			// Construct block
			alienBlocks[i] = new AlienBlock(m_sceneManager, path, startSpeed, speedIncrease, maxSpeed);

			// Load each Alien
			for(int j = 0; j < totalAliens; j++)
			{
				blockElement = blockElement->NextSibling("Alien")->ToElement();

				// Get info about the Alien.
				string name = blockElement->FirstChild("Name")->ToElement()->GetText();
				int relX = atoi(blockElement->FirstChild("RelPos")->FirstChild("X")->ToElement()->GetText());
				int relY = atoi(blockElement->FirstChild("RelPos")->FirstChild("Y")->ToElement()->GetText());

				int matX = atoi(blockElement->FirstChild("MatPos")->FirstChild("X")->ToElement()->GetText());
				int matY = atoi(blockElement->FirstChild("MatPos")->FirstChild("Y")->ToElement()->GetText());

				int type = atoi(blockElement->FirstChild("Type")->ToElement()->GetText());

				// Construct Alien from info.
				Alien *alien = new Alien(m_sceneManager, name, type);
				// Add the Alien to the currently parsed block.
				alienBlocks[i]->addAlien(alien, relX, relY);
				// Also keep the Alien in a seperate matrix for flashing purposes.
				
				alienMatrix[matX][matY] = alien;
				alien->VisualGridPosRow = relY; //anton
				alien->VisualGridPosColumn = relX; //anton
				//cout<< "relX=" << relX << "relY=" << relY <<endl;
			}
		}
		//end of xml loading



		return true;
	}
	else
	{
		//file format is wrong
		return false; 
	}
}

void BrainInvadersScreen::CleanMatrices()
{
	Logger::LogEvent("Clean the Matrix",true,false);
	
	// Set the value for all Aliens in the matrix to NULL.
	for(int i = 0; i < MAXMATRIXSIZE; i++){
		for(int j = 0; j < MAXMATRIXSIZE; j++){
			alienMatrix[i][j] = NULL;
		}
	}

	// Set the value to NULL for all blocks.
	for(int i = 0; i < MAXBLOCKS; i++){
		if(alienBlocks[i] != NULL){
			alienBlocks[i] = NULL;
		}
	}
}

void BrainInvadersScreen::ShuffleMatrix()
{
	/* This functions shuffles the elements in the alienMatrix. The results is that
	when a row in this matrix is flashed - the same number of elements are flashed
	but they are not in a horizontal row, but scattered. So a "row" or "column"
	becomes a group of randomly placed aliens. Also an alien at position x,y in
	the alienMatrix does not correspond to where it is visualized. The aliens are
	shuffled in alienMatrix, but they still keep their positions in the on-screen
	grid displayed to the user. Also check: GetAlienAtVisualPosition
	*/

	if (m_config.ShuffleEnabled)
		Logger::LogEvent("Shuffle the Matrix",true,false);
	
	// We want to use a random function.

	// First Randomize the rows
	for (int i = 0; i < MAXMATRIXSIZE; i++)
	{
		std::vector<Alien*> currentList;
		
		// Get a list of all the elements in the row.
		for (int j = 0; j < MAXMATRIXSIZE; j++)
		{
			if(alienMatrix[j][i] != NULL){
				currentList.push_back(alienMatrix[j][i]);
			}
		}
		
		int alienInRow = currentList.size();
		// We will place all the elements in the row next to each other, preferably shuffled
		for (int j = 0; j < alienInRow; j++){
			// Take a random element from the created list
			int elemToPlace = 0;
			if (m_config.ShuffleEnabled)
				elemToPlace = rand() % currentList.size();

			// Place this element next in line.
			alienMatrix[j][i] = currentList.at(elemToPlace);
			
			std::vector<Alien*> tempList;
			// Remove the added element from the generated list so we won't add it again. We use a temp list, so needs to overwrite currentList at osme point.
			for (std::size_t k = 0; k < currentList.size(); k++)
			{	
				if (k != elemToPlace){
					tempList.push_back(currentList.at(k));
				}
			}

			while (currentList.size() > 0)
				currentList.pop_back();

			for (std::size_t k = 0; k < tempList.size(); k++)
			{
				currentList.push_back(tempList.at(k));
			}

		}

		for (int j = alienInRow; j < MAXMATRIXSIZE; j++)
			alienMatrix[j][i] = NULL;
	}
	
	// Next randomize the Columns. Essentially the same. For explanation see above.
	for (int i = 0; i < MAXMATRIXSIZE; i++)
	{
		std::vector<Alien*> currentList;
		for (int j = 0; j < MAXMATRIXSIZE; j++)
		{
			if(alienMatrix[i][j] != NULL){
				currentList.push_back(alienMatrix[i][j]);
			}
		}
		
		int alienInCol = currentList.size();
		for (int j = 0; j < alienInCol; j++)
		{
			int elemToPlace = 0;
			if (m_config.ShuffleEnabled)
				elemToPlace = rand() % currentList.size();

			alienMatrix[i][j] = currentList.at(elemToPlace);

			std::vector<Alien*> tempList;
			for (std::size_t k = 0; k < currentList.size(); k++)
			{	
				if (k != elemToPlace){
					tempList.push_back(currentList.at(k));
				}
			}

			while (currentList.size() > 0)
				currentList.pop_back();

			for (std::size_t k = 0; k < tempList.size(); k++){
				currentList.push_back(tempList.at(k));
			}
		}
		for (int j = alienInCol; j < MAXMATRIXSIZE; j++)
			alienMatrix[i][j] = NULL;
	}

	//Set Target based on specification in the level
	if  (!m_config.MM_IsMultiplayer)
	{
		for (int i = 0; i < MAXMATRIXSIZE; i++)
		{
			for(int j = 0; j < MAXMATRIXSIZE; j++)
			{
				if (alienMatrix[i][j] != NULL)
				{
					//start looging
					/*std::stringstream ss;
					ss << "Alien at " << j << ":" << i << " of type " << alienMatrix[i][j]->getType();
					screenManager->LogEvent(ss.str(),true,false);*/
					//end logging

					//specify the target alien when located
					if (alienMatrix[i][j]->getType() == Alien::AlienType::Target) //SOLO and Player 1
					{
						m_targetAlien.first = i;
						m_targetAlien.second = j;

						Logger::LogEvent(string("Taget Alien set in single player mode"),true,true);
					}
				}
			}
		}
	}
	else //Trainig and Multiplayer
	{
		//Currently the Multiplayer is similar to the taining because it uses the training level without target
		//The function GetNextTarget sets the target and that is why it is not set here
	}
}

double BrainInvadersScreen::GetNextISI()
{
	//Find a new ISI time, randomly between min(ex. 50 ms ) and max(ex. 1 s) with specific mean, according to exponential distribution.

	if (m_vISISequence.empty())//this should not happen
	{
		Logger::LogEvent("Warning: the ISI queue was empty. New one was generated.",true,true);
		GenerateNewISISequence();
	}

	double result = m_vISISequence.front();
	m_vISISequence.pop_front();

	return result;
}

//Acquire flash target,set the flash duration, and visualize the flash. The flash will stay until the state "Flash" is in effect
void BrainInvadersScreen::FlashNext()
{
	m_flashesSinceLastRepetition++;

	// Get index of what to flash
	std::pair<int,char> next = m_vFlashSequence.front();
	//cout << "\n==============================";
	//cout << "\nnext to flash:" << next.first << " type: " << next.second << "\n";
	//cout << "==============================\n";

	if (next.second == 'r')
	{
		// In case of flash Row, flash all elements that have the same ending index, flashed aliens will appear not on the same row because ShuffleMatrix was used
		for(int i = 0; i < MAXMATRIXSIZE; i++)
		{
			if (alienMatrix[i][next.first] != NULL)
			{
				alienMatrix[i][next.first]->Flash();

				if (alienMatrix[i][next.first]->getType() == Alien::AlienType::Target || alienMatrix[i][next.first]->getType() == Alien::AlienType::Target2)
					// Flash the required average for the target row..
					flashTime = m_config.FlashTargetTime;//0.15 //increase flash time for the target
			}
		}

		//logging
		std::stringstream ss;
		ss << "Flash Row: " << next.first;
		Logger::LogEvent(ss.str(),true,true);

		// Tell the ParallelPort a flash occured
		if (!m_config.MM_IsMultiplayer)
		{
			bool isTarget = (m_targetAlien.second == next.first); //if the group to be flashed contains a target or not
			biApplication->communicationHandler->SetRow(next.first, isTarget);
		}
		else //multiplayer
		{
			if (m_config.MM_IsSameTarget) 
			{
				//the same as in single palyer mode
				bool isTarget = (m_targetAlien.second == next.first);
			    biApplication->communicationHandler->SetRow(next.first, isTarget);
			}
			else //multiplayer, 2 targets
			{
				bool isTarget1 = (m_targetAlien.second == next.first);
				bool isTarget2 = (m_targetAlienPlayer2.second == next.first);
			    biApplication->communicationHandler->SetRow(next.first, isTarget1, isTarget2);
			}
		}
	}
	else //group of type "column"
	{
		// In case of Column, flash all elements with same beginning index, flashed aliens will appear not on the same column because ShuffleMatrix was used
		for(int i = 0; i < MAXMATRIXSIZE; i++){
			if (alienMatrix[next.first][i] != NULL)
			{
				alienMatrix[next.first][i]->Flash();

				if (alienMatrix[next.first][i]->getType() == Alien::AlienType::Target || alienMatrix[next.first][i]->getType() == Alien::AlienType::Target2)
					// Flash the target for the average time.
					flashTime = m_config.FlashTargetTime;//0.15
			}
		}
		
		//logging
		std::stringstream ss;
		ss << "Flash Column: " << next.first;
		Logger::LogEvent(ss.str(),true,true);

		// Tell the ParallelPort a flash occured.
		if (!m_config.MM_IsMultiplayer)
		{
			bool isTarget = (m_targetAlien.first == next.first); //if the group to be flashed contains a target or not
			biApplication->communicationHandler->SetColumn(next.first, isTarget);
		}
		else //multiplayer
		{
			if (m_config.MM_IsSameTarget) 
			{
				//the same as in single player mode
				bool isTarget = (m_targetAlien.first == next.first); 
			    biApplication->communicationHandler->SetColumn(next.first, isTarget);
			}
			else //multiplayer, 2 targets
			{
				bool isTarget1 = (m_targetAlien.first == next.first);
				bool isTarget2 = (m_targetAlienPlayer2.first == next.first); 
			    biApplication->communicationHandler->SetColumn(next.first, isTarget1, isTarget2);
			}
		}
	}
}

void BrainInvadersScreen::UnFlashGroup()
{
	flashTime = m_config.FlashNonTargetTime;
	// Essentially the same as Flash, only now Unflash will be called.
	std::pair<int,char> next = m_vFlashSequence.front();

	if (next.second == 'r')
	{
		for(int i = 0; i < MAXMATRIXSIZE; i++)
		{
			if (alienMatrix[i][next.first] != NULL)
				alienMatrix[i][next.first]->UnFlash();
		}
		std::stringstream ss;
		ss << "Unflash Row: " << next.first;
		Logger::LogEvent(ss.str(),true,false);
	}
	else
	{
		for(int i = 0; i < MAXMATRIXSIZE; i++)
		{
			if (alienMatrix[next.first][i] != NULL)
				alienMatrix[next.first][i]->UnFlash();
		}
		std::stringstream ss;
		ss << "Unflash Column: " << next.first;
		Logger::LogEvent(ss.str(),true,false);
	}
}

bool BrainInvadersScreen::ProcessVRPNTargeting()
{
	//Process data for targeting
	if(!m_poVrpnPeripheral->m_vAnalog.empty())
	{
		std::list < double >* l_rVrpnAnalogState = &m_poVrpnPeripheral->m_vAnalog.front(); //get a result vector from VRPN - contains the data for all players

		//Start processing the VRPN result.
		//We need to make sure the matrix is properly sized because different levels can have different sizes
		if ( m_vCurrentTargetResponse.size() != totalRowsAndColumns()) 
		{
			m_vCurrentTargetResponse.resize(totalRowsAndColumns(),0);
			Logger::LogEvent("Matrix resized.",true,false);
		}

		std::list<double>::iterator ite = l_rVrpnAnalogState->begin(); //this iterator is used for both Player 1 and Player 2

		std::size_t i = 0;
		//In the current implementation all received results are valid and not to be ignored
		//We copy the values from the VRPN in our internal buffer m_vCurrentTargetResponse.
		//http://code.google.com/p/openvibe-gipsa-extensions/wiki/CommunicationProtocol
		
		if (l_rVrpnAnalogState->size() >0) cout<< "Player1: vrpn values: [";
		while ( ite!=l_rVrpnAnalogState->end() && i< totalRowsAndColumns())//we might have long vrpn array, but we process only the size we expect 
		{
			m_vCurrentTargetResponse[i] = *ite;
			cout << i << ":" << *ite << " ";
			i++;
			++ite;
		}
		if (l_rVrpnAnalogState->size() > 0) cout << "]" << endl;

		//cout<< "Total vrpn values accepted: " << i << endl;
		if ( i < m_vCurrentTargetResponse.size() ) Logger::LogEvent("Error: VRPN values not enough for Player 1!!!",true,true);
		//End processing the VRPN result

		//Set target to NULL before calculating it
		m_toShoot = NULL;
		m_toShootPlayer2 = NULL;

		//Player1 or SOLO
		bool testResetTabP300 = m_bResetTabP300;
		CalculateTarget(m_toShoot, m_vTabP300, m_vCurrentTargetResponse, m_bResetTabP300);//Updates the m_vTabP300 and finds the row and column with highest probability
		
		//Testing/debugging
		if (m_toShoot == NULL) Logger::LogEvent("Error: VRPN values received but target not calculated!" + (m_config.MM_IsMultiplayer) ? " For Player 1" : "",true,true);
		if (testResetTabP300 == true && m_bResetTabP300 == true) Logger::LogEvent("Error: The P300tab reset is still active! Score is not accumulating." + (m_config.MM_IsMultiplayer) ? " For Player 1" : "",true,true);

		if (m_config.MM_IsMultiplayer)
		{
				//Start processing the VRPN result for Player 2
				//We need to make sure the matrix is properly sized because different levels can have different sizes
				if ( m_vCurrentTargetResponsePlayer2.size() != totalRowsAndColumns()) 
				{
					m_vCurrentTargetResponsePlayer2.resize(totalRowsAndColumns(),0);
					Logger::LogEvent("Matrix2 resized.",true,false);
				}

				int i=0;
				//ite will continue from where it stopped (end of target 1) and get values with length = rows + columns 
				if (l_rVrpnAnalogState->size() >0) cout<< "Player2: vrpn values: [";
				while ( ite!=l_rVrpnAnalogState->end() && i< totalRowsAndColumns())//we might have long vrpn array, but we process only the size we expect 
				{
					m_vCurrentTargetResponsePlayer2[i] = *ite;
					cout << i << ":" << *ite << " ";
					i++;
					++ite;
				}
				if (l_rVrpnAnalogState->size() >0) cout << "]" << endl;

				//cout<< "Total vrpn values accepted: " << i << endl;
				if ( i < m_vCurrentTargetResponsePlayer2.size() ) Logger::LogEvent("Error: VRPN values not enough for Player 2!!!",true,true);
				//End processing the VRPN result
				
				CalculateTarget(m_toShootPlayer2, m_vTabP300Player2, m_vCurrentTargetResponsePlayer2, m_bResetTabP300Player2);
				if (m_toShootPlayer2 == NULL) Logger::LogEvent("Error: VRPN values received but target is not calculated for Player2!",true,true);
		 }

		// We don't need this VRPN state any more.
		m_poVrpnPeripheral->m_vAnalog.pop_front();

		return true;
	} //no values on vrpn
	else
	{
		// In case the VRPN is not functioning, we will shoot a random element. This is usefull for demo purposes.

		Logger::LogEvent("=======================================================",false,false);
		Logger::LogEvent("Error: No working VRPN, shooting Alien at random instead!!!",false,false);
		Logger::LogEvent("=======================================================",false,false);

		//NULL values will force random shooting later
		m_toShoot = NULL; 
		m_toShootPlayer2 = NULL;

		return false;
	}
}

//returns if a message/flag (on/off) was detected
//Only the last state per button is used  
//The VRPN queue is cleared
bool BrainInvadersScreen::ProcessVRPNMessages()
{
	bool changeStateRequested = false;

	m_poVrpnPeripheral->loop();

	//cout<<"Processing buttons.." << endl;

	//Process messaging from OpenVibe
	while(!m_poVrpnPeripheral->m_vButton.empty())
	{
		std::pair < int, int > button = m_poVrpnPeripheral->m_vButton.front();
	
		if (button.first==0) //buton OpenVibe start/stop
		{
			//cout<<"Button0 " << endl;
			//cout<<"Button0 state " << button.second << endl;
			if (button.second == 1)
			{
		       //OpenVibe has been successfully loaded/initialized
			   
				//Switch from wait to flashing mode
				if (m_config.ManageOpenVibeEnabled) 
				{
					Logger::LogEvent("OpenVibe started.",true,true);
					m_onHold = false;
				}
			}
			else
			{
			   //OpenVibe is done/shutting down
			}

			changeStateRequested = true;
			m_poVrpnPeripheral->m_vButton.pop_front();
		}
		else 
		if (button.first==1) //Artifact start/stop
		{
			//cout<<"Button1 " << endl;
			//cout<<"Button1 state " << button.second << endl;
			if (button.second == 1) //started
			{
				//pause game
				m_onHold = true;			
			}
			else //ended
			{
				//resume game
				m_onHold = false;
			}

			changeStateRequested = true;
			m_poVrpnPeripheral->m_vButton.pop_front();
		}
	
	}

	return changeStateRequested;
}

//Updates the m_vTabP300 and finds the row and column with highest probability. Sets pickedTarget which determines which alien to explode.
void BrainInvadersScreen::CalculateTarget(Alien*& p_toShoot, std::vector<double>& p_vTabP300, const std::vector<double>& p_vCurrentTargetResponse,bool& p_bResetTabP300)
{
	int actualRowCount = alienMatrixActualRowsCount();
	int actualColumnCount = alienMatrixActualColumnsCount();
	int matrixElementCount = actualRowCount * actualColumnCount;

	// Make sure the matrix is properly sized.
	if ( p_vTabP300.size() < matrixElementCount)
	{
		p_vTabP300.resize(matrixElementCount, 0);
	}

	// 1. Update the m_vTabP300 with the last received result
	// http://code.google.com/p/openvibe-gipsa-extensions/wiki/CommunicationProtocol
	// We iterate over m_vCurrentTargetResponse expecting first all the rows and then all the columns.
	for ( int i=0; i<actualRowCount; i++ )
	{
		for ( int j=0; j<actualColumnCount; j++ ) 
		{
			// warning : P300 trust values are inversely proportional to received values
			// Response equals the response to the row the element occurs in plus the column.
			// We want to find these rows and columns that give the highest probability
			double rowValue = p_vCurrentTargetResponse[i];
			double columnValue = p_vCurrentTargetResponse[actualRowCount + j];

			//The target is on the row with maximum value and on the column with maximum value (or mimimum depending on the current implementation).  
			double l_dbtmp = - ( rowValue + columnValue ); 
			
			// If we reset the response, overwrite value, else add them together.
			// Take into acccount the previous repetitions - this boosts the classification result
			if ( p_bResetTabP300 )
			{
				p_vTabP300[i * actualRowCount + j] = l_dbtmp;
	        }
			else
			{
				p_vTabP300[i * actualRowCount + j] += l_dbtmp; //accumulate from previous repetions
			}
		}
	}

	// Make sure the next repetition wont include a reset (overwritten if new level is loaded).
	p_bResetTabP300 = false;

	pair<int, int> l_pickedTarget;
	double l_lastP300Maximum = -9999;

	// 2. Find the max value of the response values of all the elements based on the previously updated m_vTabP300
	for(int i=0; i < actualRowCount; i++)
	{
		for(int j=0; j < actualColumnCount; j++)
		{
			double dbtmp=p_vTabP300[i * actualRowCount + j]; 
			
			//cout << "dbtmp=" << dbtmp << endl; 

			// We skip dead aliens
			if ( !alienMatrix[j][i]->alive ) continue;

			// If new maximum, set new target to shoot
			if(dbtmp>l_lastP300Maximum) 
			{
				//cout << "new maximum, dbtmp bigger than m_dLastP300Maximum=" << lastP300Maximum << endl; 

				l_pickedTarget.first = j;
				l_pickedTarget.second = i;

				l_lastP300Maximum = dbtmp;
			}
		}
	}

	// 3. Set the final result
	p_toShoot = alienMatrix[l_pickedTarget.first][l_pickedTarget.second];
	
	//logging
	std::stringstream ss;
	ss << "Shoot Alien at\n\tRow: " << l_pickedTarget.second << "\n\tColumn: " << l_pickedTarget.first;
	Logger::LogEvent(ss.str(),false,false);
}

//for training and multiplayer mode only
bool BrainInvadersScreen::GetNextTarget()
{
	std::pair<int,int> newTarget;

	Logger::LogEvent("Setting first target...",true,true);

	// Get the next target from the list
	if ( !m_vTargetSequence.empty() )
	{
		newTarget = m_vTargetSequence.front();
		m_vTargetSequence.pop_front();

		// Reset all the elements to normal except for the new target, which is set to Target Mode.
		for (int i = 0; i < MAXMATRIXSIZE; i++)
			for (int j = 0; j < MAXMATRIXSIZE; j++)
				if (alienMatrix[i][j] != NULL)
				{
					bool isTaget = (i == newTarget.first && j == newTarget.second);
					alienMatrix[i][j]->makeTarget(isTaget,true);
				}

		// Communicate the target.	
		m_targetAlien.first = newTarget.first;
		m_targetAlien.second = newTarget.second;

		if (!this->m_isTrainingMode && m_config.MM_IsMultiplayer && !m_config.MM_IsSameTarget)
		{
			Logger::LogEvent("Setting second target...",true,true);

			if (!m_vTargetSequencePlayer2.empty())
			{
				std::pair<int,int> newTargetPlayer2 =  m_vTargetSequencePlayer2.front();
				m_vTargetSequencePlayer2.pop_front();

				// Reset all the elements to normal except for the new target, which is set to Target Mode.
				for (int i = 0; i < MAXMATRIXSIZE; i++)
					for (int j = 0; j < MAXMATRIXSIZE; j++)
						if (alienMatrix[i][j] != NULL)
						{
							alienMatrix[i][j]->makeTarget(false,true); //reset all before setting the targets 

							if ((i == newTarget.first && j == newTarget.second) && (!(i == newTargetPlayer2.first && j == newTargetPlayer2.second)))
							   alienMatrix[i][j]->makeTarget(true,true);

							if ((i == newTargetPlayer2.first && j == newTargetPlayer2.second) && (!(i == newTarget.first && j == newTarget.second)))
							   alienMatrix[i][j]->makeTarget(true,false);
						}

				// Communicate the target.		
				m_targetAlienPlayer2.first = newTargetPlayer2.first;
				m_targetAlienPlayer2.second = newTargetPlayer2.second;
			}
			else
			{
				return false;
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

void BrainInvadersScreen::ResetTimer()
{
	levelTimer.reset();
}

int BrainInvadersScreen::alienMatrixActualRowsCount()
{
	int result=0;

	for(int i = 0; i < MAXMATRIXSIZE; i++)
		if(alienMatrix[0][i] != NULL)
			result++;
		else break;

	return result;
}

int BrainInvadersScreen::alienMatrixActualColumnsCount()
{
	int result=0;

	for(int i = 0; i < MAXMATRIXSIZE; i++)
		if(alienMatrix[i][0] != NULL)
			result++;
		else break;

	return result; 
}

int BrainInvadersScreen::totalRowsAndColumns()
{
	return alienMatrixActualRowsCount() + alienMatrixActualColumnsCount();	
}

int BrainInvadersScreen::flashesPerRepetition()
{
	return totalRowsAndColumns();
}

void BrainInvadersScreen::PutOnHold()
{
	//cout<<"to pause"<<endl;
	m_cachedState = m_currentState;
	m_currentState = OnHold;

	//overlay->getChild("PanelName")->getChild("TextAreaScore")->setCaption("PAUSED");
	
	/*cout<<"===================================================="<<endl;
	cout<<"--------------------GAME PAUSED---------------------"<<endl;
	cout<<"===================================================="<<endl;*/
}

//This function applies a policy that for example slows down the game if the user is not performing well
void BrainInvadersScreen::AdjustMeanISI()
{
	std::stringstream ss;

	Logger::LogEvent("Adjust ISI time...",true,true);

	int shotsMissed = this->m_currentRepetition;

	double r = m_helper.GenerateRandomNumberUniformZeroOne();

	int percent = m_config.PercentISIAdapt;
	double delta = (m_config.MeanISI * double(percent)) / double(100);

	ss << "delta: " << delta;
	Logger::LogEvent(ss.str(),true,true);
	
	std::stringstream ss_action;

	if (r < 0.1) 
	{
		m_config.MeanISI -= delta;
		ss_action << "r < 0.1 Number of times target missed: " << shotsMissed << ". Decreased by 20% to: " << m_config.MeanISI << "\n";
        Logger::LogEvent(ss_action.str(),true,true);
	}
	else
	{
		if (shotsMissed >= 5)
		{
			m_config.MeanISI += delta;
			ss_action << "Case(>5) Number of times target missed: " << shotsMissed << ". Increased by 20% to: " << m_config.MeanISI << "\n";
			Logger::LogEvent(ss_action.str(),true,true);
		}
		else if (shotsMissed <=2)
		{   

			m_config.MeanISI -= delta;
			ss_action << "Case(<=2) Number of times target missed: " << shotsMissed << ". Decrased by 20% to: " << m_config.MeanISI << "\n";
			Logger::LogEvent(ss_action.str(),true,true);
		}
		else
		{
			ss_action << "Case(3 or 4) Number of times target missed: " << shotsMissed << ". Value not changed: " << m_config.MeanISI << "\n";
			Logger::LogEvent(ss_action.str(),true,true);
		}
		//else if last 4 levels  
	}
}

void BrainInvadersScreen::GenerateNewISISequence()
{
	m_vISISequence.clear();

	int maxValues = totalRowsAndColumns() * ((this->m_isTrainingMode) ? m_config.MaxTrainingTargets : m_config.MaxRepetitions(this->m_isTrainingMode)) + 100;

	std::stringstream ss;
	ss << "\nISI values:\n";
	ss << "[";
	for (int i=0;i<maxValues;i++)
	{
		double expDistMean = m_config.MeanISI; //- m_config.MinISI;
		if (expDistMean <=0) expDistMean = 0.001; //landa >0
		double result = m_helper.GenerateRandomNumberExpBuiltIn(expDistMean);

		result += m_config.MinISI; //because we substracted the min when calling the exp. function, we make sure the lower boundary is in effect
		//if (result < m_config.MinISI) result = m_config.MinISI;
		if (result > m_config.MaxISI) result = m_config.MaxISI;

		ss<< result << " ";
		m_vISISequence.push_back(result);
	}
	ss << "]";
	Logger::LogEvent(ss.str(),true,false);
}

Alien* BrainInvadersScreen::GetAlienAtVisualPosition(int row, int column)
{
	Alien* result = NULL;
	for (int i = 0; i < MAXMATRIXSIZE; i++)
	        {
				for (int j = 0; j < MAXMATRIXSIZE; j++)
				{
					Alien* alien = alienMatrix[i][j];
					if (alien != NULL
						&& alien->VisualGridPosRow == row
						&& alien->VisualGridPosColumn == column)
					{
					   result = alien;
					}
				}
			}

	return result;
}
