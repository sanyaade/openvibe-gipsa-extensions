/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "WinLooseScreen.h"
#include "GameScreen.h"

#include "BrainInvadersApplication.h"

using namespace BrainInvaders;

#include <Ogre.h>
#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	#include <Overlay\OgreFont.h>	
	#include <Overlay\OgreFontManager.h>
	#include <Overlay\OgreOverlayElement.h>
#else
	#include <OgreFont.h>
	#include <OgreFontManager.h>
	#include <OgreTextAreaOverlayElement.h>
#endif

#include <CEGUI.h>
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	#include <CEGUI/RendererModules/Ogre/Renderer.h>
#else
	#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#endif

#include <OIS.h>
using namespace Ogre;

#include <stdlib.h>
#include <stdlib.h> 

WinLooseScreen::WinLooseScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CEGUI::WindowManager *mWM, CEGUI::Window *m_poSheet, GameScreen *GS, int pauseBetweenLevels) 
	: SpaceInvadersScreen(ScrMan, mSM),
	m_pauseBetweenLevels(pauseBetweenLevels)
{
	m_poGUIWindowManager = mWM;
	gameScreen = GS;
	
	m_scoreP1=1;
	m_scoreP2=2;
	m_scoreP1new=1;
	m_scoreP2new=2;
	
	// load all the images for background screens (Win or Loose Screens) plus all the images for the scores
	// Main path is in resources.cfg
	const std::string l_sWonMenu = "Resources/GUI/brainInvadersWon.png"; //solo background WIN
	const std::string l_sLostMenu = "Resources/GUI/brainInvadersLost.png"; //solo background LOOSE

	const std::string l_sWonMenu_multi1 = "Resources/GUI/BI_multi1.png"; //multi1 background 
	const std::string l_sWonMenu_multi2 = "Resources/GUI/BI_multi2.png"; //multi2 background 
	const std::string l_sWonMenu_multi3 = "Resources/GUI/BI_multi3.png"; //multi3 background 
	const std::string l_sWonMenu_multi4 = "Resources/GUI/BI_multi4.png"; //multi4 background 

	const std::string l_sTextScore0 = "Resources/GUI/0.png"; //multi "+0" points image
	const std::string l_sTextScore2000 = "Resources/GUI/2000.png"; //multi "+2000 !" points image
	const std::string l_sTextScore5000 = "Resources/GUI/5000.png"; //multi "+5000 !!!" points image

	//----------- CREATE WINDOWS -------------//
	// background Won
	createOgreWindow(m_poSheet, l_sWonMenu, "WonMenu", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	// background Lost
	createOgreWindow(m_poSheet, l_sLostMenu, "LostMenu", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	//----------- MULTI 1 -------------//
	
	// background multi1 cooperation same target
	createOgreWindow(m_poSheet, l_sWonMenu_multi1, "WonMenu_multi1", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));
	
	// background multi2 cooperation not-same target
	createOgreWindow(m_poSheet, l_sWonMenu_multi2, "WonMenu_multi2", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));
			
	// background multi3 competition same target
	createOgreWindow(m_poSheet, l_sWonMenu_multi3, "WonMenu_multi3", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));
			
	// background multi4 competition not-same target
	createOgreWindow(m_poSheet, l_sWonMenu_multi4, "WonMenu_multi4", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	///////////////// PLAYER 1 SCORE ////////////// on left

	// TextScore0
	createOgreWindow(m_poSheet, l_sTextScore0, "TextScore0", CEGUI::UVector2(cegui_reldim(0.1f), cegui_reldim(0.6f)), CEGUI::USize(CEGUI::UDim(0.35f, 0.f), CEGUI::UDim(0.1f, 0.f)));

	// TextScore2000
	createOgreWindow(m_poSheet, l_sTextScore2000, "TextScore2000", CEGUI::UVector2(cegui_reldim(0.1f), cegui_reldim(0.6f)), CEGUI::USize(CEGUI::UDim(0.35f, 0.f), CEGUI::UDim(0.1f, 0.f)));

	// TextScore5000
	createOgreWindow(m_poSheet, l_sTextScore5000, "TextScore5000", CEGUI::UVector2(cegui_reldim(0.1f), cegui_reldim(0.6f)), CEGUI::USize(CEGUI::UDim(0.35f, 0.f), CEGUI::UDim(0.1f, 0.f)));

	///////////////// PLAYER 2 SCORE ////////////// on right

	// TextScore0p2
	createOgreWindow(m_poSheet, l_sTextScore0, "TextScore0p2", CEGUI::UVector2(cegui_reldim(0.5f), cegui_reldim(0.6f)), CEGUI::USize(CEGUI::UDim(0.35f, 0.f), CEGUI::UDim(0.1f, 0.f)));

	// TextScore2000p2
	createOgreWindow(m_poSheet, l_sTextScore2000, "TextScore2000p2", CEGUI::UVector2(cegui_reldim(0.5f), cegui_reldim(0.6f)), CEGUI::USize(CEGUI::UDim(0.35f, 0.f), CEGUI::UDim(0.1f, 0.f)));

	// TextScore5000p2
	createOgreWindow(m_poSheet, l_sTextScore5000, "TextScore5000p2", CEGUI::UVector2(cegui_reldim(0.5f), cegui_reldim(0.6f)), CEGUI::USize(CEGUI::UDim(0.35f, 0.f), CEGUI::UDim(0.1f, 0.f)));
}

void WinLooseScreen::createOgreWindow(CEGUI::Window *m_poSheet, 
	                             std::string imageResourceParth, 
								 std::string windowName, 
								 CEGUI::UVector2 windowPosition, 
								 CEGUI::USize windowSize)
{
	CEGUI::Window * window = m_poGUIWindowManager->createWindow("TaharezLook/StaticImage", windowName /*"FondMenu"*/);

	//anton: code rewritten
	window->setPosition(windowPosition);
	window->setSize(windowSize);

	std::string imageSetName = "Image" + windowName;

	//Logger::LogEvent("Adding: " + imageSetName + std::string(" ") + imageResourceParth, true, true);

	#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
		m_poSheet->addChild(window);
		CEGUI::ImageManager::getSingleton().addFromImageFile(imageSetName, imageResourceParth /*l_sFondMenu*/);
		window->setProperty("Image",imageSetName);
	#else
		m_poSheet->addChildWindow(window);
		CEGUI::ImagesetManager::getSingleton().createFromImageFile(imageSetName, imageResourceParth /*l_sFondMenu*/);
		window->setProperty("Image", "set:" + imageSetName + " image:full_image");
	#endif

	window->setProperty("FrameEnabled", "False");
	window->setProperty("BackgroundEnabled", "False");

	WinLooseWindows.push_back(window);
}



void WinLooseScreen::visible(bool isVisible)
{
	if (!Config::MM_IsMultiplayer) //SOLO
	{
	    // If this is a Win screen, show the win version
		
		//anton: code rewritten
		//m_poGUIWindowManager->getWindow("WonMenu")->setVisible(visible && winScreen);
		WinLooseWindows[0]->setVisible(isVisible && m_isLastGameWon);
		
		// If not, show the loose version.
		//m_poGUIWindowManager->getWindow("LostMenu")->setVisible(visible && !winScreen);
		WinLooseWindows[1]->setVisible(isVisible && !m_isLastGameWon);

		// Hide everything else
		/*m_poGUIWindowManager->getWindow("WonMenu_multi1")->setVisible(false);
		m_poGUIWindowManager->getWindow("WonMenu_multi2")->setVisible(false);
		m_poGUIWindowManager->getWindow("WonMenu_multi3")->setVisible(false);
		m_poGUIWindowManager->getWindow("WonMenu_multi4")->setVisible(false);
		m_poGUIWindowManager->getWindow("TextScore0")->setVisible(false);
		m_poGUIWindowManager->getWindow("TextScore2000")->setVisible(false);
		m_poGUIWindowManager->getWindow("TextScore5000")->setVisible(false);
		m_poGUIWindowManager->getWindow("TextScore0p2")->setVisible(false);
		m_poGUIWindowManager->getWindow("TextScore2000p2")->setVisible(false);
		m_poGUIWindowManager->getWindow("TextScore5000p2")->setVisible(false);
		*/
		for (int i = 2; i < 12; i++)
			WinLooseWindows[i]->setVisible(false);

		// WinLooseScreen clock update
		timerWinLoose.reset();

		//startedLoading = false;
		skipScreen = false;
	}
	else //Multiplayer mode
	{
		// Show background screen for the given multiplayer condition (mulit1, multi2, multi3, multi4)
		//anton: code rewritten
		/*m_poGUIWindowManager->getWindow("WonMenu_multi1")->setVisible(visible && Config::MM_IsCooperative  && Config::MM_IsSameTarget);
		m_poGUIWindowManager->getWindow("WonMenu_multi2")->setVisible(visible&& Config::MM_IsCooperative  && !Config::MM_IsSameTarget);
		m_poGUIWindowManager->getWindow("WonMenu_multi3")->setVisible(visible&& !Config::MM_IsCooperative  && Config::MM_IsSameTarget);
		m_poGUIWindowManager->getWindow("WonMenu_multi4")->setVisible(visible&& !Config::MM_IsCooperative  && !Config::MM_IsSameTarget);
		*/
		WinLooseWindows[2]->setVisible(isVisible && Config::MM_IsCooperative  && Config::MM_IsSameTarget);  //WonMenu_multi1
		WinLooseWindows[3]->setVisible(isVisible && Config::MM_IsCooperative  && !Config::MM_IsSameTarget); //WonMenu_multi2
		WinLooseWindows[4]->setVisible(isVisible && !Config::MM_IsCooperative  && Config::MM_IsSameTarget); //WonMenu_multi3
		WinLooseWindows[5]->setVisible(isVisible && !Config::MM_IsCooperative  && !Config::MM_IsSameTarget);//WonMenu_multi4

		// Compute the gain of each player
		int l_diffP1=(m_scoreP1new-m_scoreP1);
		int l_diffP2=(m_scoreP2new-m_scoreP2);

		bool l_Player1won5000 = (l_diffP1>4500); //if the P1's score is increase more than 4500 points, then show "+5000 !!!" points image
		bool l_Player1won2000 = (4501>l_diffP1&& l_diffP1>100); //if the P1's score is increase between 100 and 4500 points, then show "+2000 !" points image
		bool l_Player1won0 = (l_diffP1<101); //if the P1's score is increase less than 100 , then show "+0" points image
		bool l_Player2won5000 = (l_diffP2>4500); //if the P2's score is increase more than 4500 points, then show "+5000 !!!" points image
		bool l_Player2won2000 = (4501>l_diffP2 && l_diffP2>100);  //if the P2's score is increase between 100 and 4500 points, then show "+2000 !" points image
		bool l_Player2won0 = (l_diffP2<101);//if the P2's score is increase less than 100 , then show "+0" points image

		//////////////////////// MULTIPLAYER SCORE COMPUTATION AT A GIVEN REPETITION ///////////////////////
		//             CASE                            COOPERATION                    COMPETITION
		//  one target had been destroyed             +2000 for both     +5000 for the one /+0 for the other
		// both targets has been destroyed            +5000 for both                +2000 for both
		//  none target had been destroyed             +0 for both                    +0 for both

		// CONDITIONS FOR PLAYER 1 (amount of point won during previous level)
		
		//anton: code rewritten
		/*m_poGUIWindowManager->getWindow("TextScore5000")->setVisible(visible && l_Player1won5000);
		m_poGUIWindowManager->getWindow("TextScore2000")->setVisible(visible && l_Player1won2000);
		m_poGUIWindowManager->getWindow("TextScore0")->setVisible(visible && l_Player1won0);*/
		WinLooseWindows[6]->setVisible(isVisible && l_Player1won0); //TextScore0
		WinLooseWindows[7]->setVisible(isVisible && l_Player1won2000); //TextScore2000
		WinLooseWindows[8]->setVisible(isVisible && l_Player1won5000); //TextScore5000
		
		// CONDITIONS FOR PLAYER 2 (amount of point won during previous level)
		/*m_poGUIWindowManager->getWindow("TextScore5000p2")->setVisible(visible && l_Player2won5000);
		m_poGUIWindowManager->getWindow("TextScore2000p2")->setVisible(visible&& l_Player2won2000);
		m_poGUIWindowManager->getWindow("TextScore0p2")->setVisible(visible && l_Player2won0);*/
		WinLooseWindows[9]->setVisible(isVisible && l_Player2won0);     //TextScore0p2
		WinLooseWindows[10]->setVisible(isVisible && l_Player2won2000);  //TextScore2000p2
		WinLooseWindows[11]->setVisible(isVisible && l_Player2won5000); //TextScore5000p2

		// the current level is over, update the score for the next level
		m_scoreP1=m_scoreP1new;
		m_scoreP2=m_scoreP2new;

		// WinLooseScreen clock update
		timerWinLoose.reset();
		//startedLoading = false;
		skipScreen = false;

	}
}


//loads next level and switches back to the game
bool WinLooseScreen::update(double timeSinceLastUpdate)
{
	// Once done, wait if the screen has not been displayed long enough (so players can relax)
	if (timerWinLoose.getMilliseconds() > m_pauseBetweenLevels || skipScreen)
	{
		timerWinLoose.reset();
		//startedLoading = false;
		skipScreen = false;

		gameScreen->FinishLevel(); //loads next level, switches to main menu or continues the game
		
	}
	return true;
}

void WinLooseScreen::keyPressed(const OIS::KeyEvent& evt){
	if ( evt.key == OIS::KC_SPACE)
	{
		// If space is hit during loading (and we are done) we start
		skipScreen = true;
	}
}