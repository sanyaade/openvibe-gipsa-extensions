/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
* AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
* RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
* AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
* REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 

* This file is part of Brain Invaders.
* Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
* of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "GameScreen.h"
#include "BrainInvadersApplication.h"
#include "AlienBlock.h"
using namespace BrainInvaders;

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	#include "Overlay/OgreOverlaySystem.h"
	#include "Overlay/OgreOverlayContainer.h"
	#include "Overlay/OgreOverlayElement.h"
#else
	#include <OgreFont.h>
	#include <OgreFontManager.h>
	#include <OgreTextAreaOverlayElement.h>
#endif
using namespace Ogre;

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sstream>
#include <time.h>
#include <math.h>
#include <vector>
using namespace std;

#include "../tinyxml/tinyxml.h"
#include "../Logger.h"

GameScreen::GameScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config) : 
BrainInvadersScreen(ScrMan, mSM, vrpnPeripheral, config)

{
	LoadAssets();

	progressBar = new ProgressBar(m_sceneManager, m_config.Lives);

	SetUpOverlay();  

	m_bIsScoreTextInitialized = false;
}

void GameScreen::LoadAssets()
{
	// Loading in all sprites beforehand makes sure animations dont lag.
	// Assets are loaded from the file "Spaceinvader.material".
	// In the context of BI "Sprite", "Texture" and "Material" are the same.

	Entity *tempEntity = m_sceneManager->createEntity("temp", "cube.mesh");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_0_1_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_1_1_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_0_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_0_F");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_1_U");
	tempEntity->setMaterialName("Spaceinvader/Alien_2_1_F");
	m_sceneManager->destroyEntity("temp");
}

bool GameScreen::update(double timeSinceLastUpdate)
{
	bool moved = false;
	int milliTime = movementTimer.getMilliseconds();

	// Move all the blocks.
	if (!m_onHold)
	{
		for (int i = 0; i < MAXBLOCKS; i++){

			//update the blocks if not in a special condtion , usually updates during gamescreen states
			if(alienBlocks[i] != NULL && (m_currentState != BlackScreen && m_currentState != ShowTarget && m_currentState != WinScreen && m_currentState != LooseScreen && m_currentState != ShowExplosion))
			{
				bool temp = alienBlocks[i]->updateBlock(milliTime);
				moved = moved || temp;
			}

		}

	}

	// If there is a movement, play a sound.
	if (moved)
	{
		Logger::LogEvent("Alien grid moved",true,false);
		soundManager->move();
		movementTimer.reset();
	}

	// Switch over GameStates
	switch(m_currentState)
	{

	case BlackScreen:

		FixLayout( stateTimer.getMilliseconds() >= ((1000 * m_config.StartupLevelPauseTime) - 40 ) );

		// wait specific time and depending on the game logic choose the next state 

		// Go to Pause Time
		if (stateTimer.getMilliseconds() >= 1000*m_config.StartupLevelPauseTime)
		{
			// For training we need to select the target.

			if (m_isTrainingMode)
			{
				//Set a new target
				bool targetSetSuccessfully = GetNextTarget();

				// If No target then end the training
				if (!targetSetSuccessfully)
				{
					biApplication->communicationHandler->SetBeginProcessing();
					m_currentState = FinishTrain;
					stateTimer.reset();
					break;
				}
			}
			else
			if (m_config.MM_IsMultiplayer)
			{
				//Set a new target
				bool targetSetSuccessfully = GetNextTarget();
			}

			stateTimer.reset();

			m_currentState = SendTargetInfo;
			visible(true);

			// Show the current Target because we leave black screen
			for (int i = 0; i < MAXMATRIXSIZE; i++)
				for (int j = 0; j < MAXMATRIXSIZE; j++)
					if (alienMatrix[i][j] != NULL)
					{
						alienMatrix[i][j]->ShowTargetMode();
					}
			Logger::LogEvent("Show Target",true,false);

			m_bIsScoreTextInitialized = false;  //for the next level

		}
		break;

	//this state seems redundant, it only prints the target data to the console
	case SendTargetInfo:

		if (stateTimer.getMilliseconds() >= 150)
		{
			stateTimer.reset();

			//start logging
			Logger::LogEvent("\n\n======Start New Repetition ======\n",true,false);
			std::stringstream ss;
			ss << "Send Target Info\n\tRow: " << m_targetAlien.second << "\n\tColumn: " << m_targetAlien.first;
			Logger::LogEvent(ss.str(),true,false);
			//end logging

			m_currentState = ShowTarget;

		}
		break;

	case ShowTarget:	
		{
			// If it is the first repetion then give the user a time to focus 
			// Here we are coming either from the very beginning or after a shot (so the explosion texture is still visible)

			double delay = (m_currentRepetition==0) ? 2 : 0;

			if (stateTimer.getMilliseconds() >= 1000 * delay)
			{
				stateTimer.reset();

				biApplication->communicationHandler->SetFinalized();

				// Make sure all is unflashed (also stops Target display)
				for (int i = 0; i < MAXMATRIXSIZE; i++)
					for (int j = 0; j < MAXMATRIXSIZE; j++)
						if (alienMatrix[i][j] != NULL)
							alienMatrix[i][j]->UnFlash();//aliens change to unflash sprite

				m_currentState = Flash;//start flashing again

				FlashNext();//picks which row or column to flash next
			}
		}
		break;

	case Flash: //Wait flashTime for the flash to complete and then unflash it

		if (stateTimer.getMilliseconds() >= 1000 * flashTime)
		{
			stateTimer.reset();

			//mark the end of SetRow() or SetColumn() from FlashNext()
			biApplication->communicationHandler->SetFinalized();

			this->UnFlashGroup(); //Flashing finished so proceed with unflash
			m_vFlashSequence.pop_front();

			//Are we at the end of the repetition.
			bool l_bMoreRowsToFlash = (!(m_flashesSinceLastRepetition >= flashesPerRepetition()));

			if (l_bMoreRowsToFlash) //within a repetition 
			{
				// If there is a next flash, go to ISI state to remove the flash from the screen
				m_currentState = ISI;
			}
			else// Else handle a new repetition
			{
				cout << "Starting new repetion\n";

				m_flashesSinceLastRepetition = 0;

				GenerateFlashSequence(); //refill flash sequence, because it is reduced after each repetion to a 0 count

				m_currentRepetition++;

				if (m_isTrainingMode) //training mode repetition
				{ 
					// For training we need to go back to Black Screen State (= target selection)
					m_currentState = ISI;//keep flashing

					if (m_currentRepetition >= m_config.MaxTrainingRepetitionsPerTarget)
					{
						m_currentRepetition = 0;
						m_currentState = BlackScreen;//go black screen to act as pause and show the new target
					}
				}
				else //online mode repetition
				{
					// Start preparing next repetition.
					Helper::delay(50);//System::Time::sleep(50);
					biApplication->communicationHandler->SetRepetionCompeted();
					Helper::delay(50);//System::Time::sleep(50);
					biApplication->communicationHandler->SetFinalized();

					//check if it is the time for shooting
					if (m_currentRepetition % m_config.RepetitionsPerLife == 0)
					{
						m_currentState = Shoot;
					}
					else //we are here if 1 life is more than 1 repetion
					{
						m_currentState = ISI;//keep flashing
						cout << "\nNew repetion, not shooting, continue flashing.\n";
					}
				}
			}

		}
		break;

	case ISI: // Wait ISITime between falshes. Flash next alien group when the ISITime expired.

		if (stateTimer.getMilliseconds() >= 1000 * ISITime) //wait ISI(time between flashes) before flashing
		{	
			//Debugging
			/*std::stringstream ss;
			ss << "ISI time used" << ISITime << "\n";
			Logger::LogEvent(ss.str(),true,true);*/

			//set next ISI time
			if (m_flashesSinceLastRepetition + 1 == this->alienMatrixActualRowsCount())
			{
				//We make the time between flashing all rows and all columns to be fixed.
				//We knows that rows are first and m_flashesSinceLastRepetition is being reset every repetion.
				ISITime = m_config.PauseBetweenRowsAndColumns;
			}
			else
			{
				ISITime = GetNextISI();
			}

			m_currentState = Flash; //set that from now on we are in a flash

			stateTimer.reset();

			//Set the flash on the screen
			FlashNext(); //acquire flash target,set the flash duration, and visualize the flash
		}
		break;

	case OnHold:
		//cout << "games paused due to artifact/bad signal ...";

		if (ProcessVRPNMessages() && m_onHold==false) //exit OnHold state
		{
			m_currentState = m_cachedState;

			std::stringstream ss;
			if (!m_config.MM_IsMultiplayer)
				ss << "score: " << m_score;
			else 
				ss << "score: " << m_score << "  " << m_scorePlayer2;
			
			UpdateScoreText(ss.str());

			/*cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
			cout<<"--------------------GAME STARTED--------------------"<<endl;
			cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;*/
		}
		else
		{
			Helper::delay(30);
			//cout<<"No message"<<endl;
		}
		break;

	//Get the result, set the exploding texture
	case Shoot:
		{   
			bool validShotDetected = ProcessVRPNTargeting(); //this function is called many times, bacuase the "Shoot" state does not change until the timeout below expire or result has been received

			if (stateTimer.getMilliseconds() >= (m_config.VRPNTargetReceivedTimeout * 1000) || validShotDetected) //wait for OpenVibe to respond
			{
				stateTimer.reset();

				if (!validShotDetected)
				{
					Logger::LogEvent("Error: No working VRPN, shooting Alien at RANDOM instead!!!", true, true);
				}

				biApplication->communicationHandler->SetFinalized();

				//state if we have not won or lost (we have more lives to try)
				m_currentState = ShowExplosion;				

				// Shoot the selected Alien - whether provided from OpenVibe or random
				m_bIsTargetShot = ShootAlien();
			}
		}
		break;

	//During this state the explosion texture stays
	case ShowExplosion:
	{
		double delay = m_config.PauseAfterDestruction * 1000;

		if (stateTimer.getMilliseconds() >= delay)
		{
		        stateTimer.reset();

				//log delay of destruction
				std::stringstream ss;
				ss << "Destruction delay (milliseconds): " << delay << endl;
				Logger::LogEvent(ss.str(), true, false);
				//end log delay

				//state if we have not won or lost (we have more lives to try)
				m_currentState = SendTargetInfo;				
				
				if (m_bIsTargetShot)
				{
					m_currentState = WinScreen;
				}
				else //non-target alien shot
				{
					// Start a new repetition, go to LooseScreen if this is the last one.
					if (m_currentRepetition >= m_config.MaxRepetitions(m_isTrainingMode))
					{
						Logger::LogEvent("Level lost.",true,true);
						m_currentState = LooseScreen;
					}
				}

				//detects and sends to OV that level has been completed
				if (m_currentState == WinScreen || m_currentState == LooseScreen)
				{
					biApplication->communicationHandler->BI_SetLevelCompleted();
					Helper::delay(50);//System::Time::sleep(50);
					biApplication->communicationHandler->SetFinalized();

					m_totalLevelsPlayed++;
					m_isFirstTargetDestroyedCoop2Targets = false;

					Logger::LogEvent(string("Total levels played: ") + std::to_string((long long)m_totalLevelsPlayed),true,true);			
				}
		}
	}
	break;

	case FinishTrain:

		// Finishes the training of the game.
		if (stateTimer.getMilliseconds() >= 150)
		{
			stateTimer.reset();
			biApplication->communicationHandler->SetFinalized();
			biApplication->FinishGame();
		}
		break;

	case WinScreen:
		
		// Loads new level and shows winning screen.
		if (stateTimer.getMilliseconds() >= 150)
		{
			stateTimer.reset();
			m_currentLevel++;

			//Show Load Screen -> FinishLevel() -> processLevel() -> Show Game Screen (back here)
			biApplication->ActivateLoadScreen(true,m_score,m_scorePlayer2); // 2 LAST ARGUMENTS TO DISPLAY THE SCORE ON THE LOADING SCREEN
		}
		break;

	case LooseScreen:
		
		// Resets the current level and shortly shows a loosing screen
		if (stateTimer.getMilliseconds() >=  150)
		{
			stateTimer.reset();

			//Show Load Screen -> FinishLevel() -> processLevel() -> Show Game Screen (back here)
			biApplication->ActivateLoadScreen(false,m_score,m_scorePlayer2);// 2 LAST ARGUMENTS TO DISPLAY THE SCORE ON THE LOADING SCREEN
		}
		break;

	default:
		break;
	}

	//PutOnHold() checks if the last message is to pause (to put on hold) and then set the current state to "OnHold"
	//While in the OnHold state the game checks if the flag for resuiming is raised. If so it reverts to the planned state.
	//If again we get pause/on hold message then we do nothing - actually we wait in the "OnHold" state where it is safe to wait and where we check the queue for the resume message/flag.
	//if m_onHold == true then messages/flags are now checked in the "OnHold" state, so we don't check here
	if (!m_onHold) 
	{	
		bool stateChanged = ProcessVRPNMessages();
		if (stateChanged && m_onHold)
		{
			PutOnHold();
		}
	}

	return true;
}

bool GameScreen::ShootAlien() //Calculates the score
{
	if (!m_config.MM_IsMultiplayer) //single player
	{
		string type = "";
		// If we do not have a proper target provided by CalculateTarget(), we pick a random one and tell the progress bar we did a false shot.
		if (m_toShoot == NULL)
		{
			progressBar->nextRepetition(false);

			type = "RANDOM ";
			int randRow;
			int randColumn;

			while(m_toShoot == NULL || !m_toShoot->alive)
			{
				randRow = rand() % alienMatrixActualColumnsCount();
				randColumn = rand() %    alienMatrixActualRowsCount();

				//Find the alien in the visual grid in position [randRow, randColumn]
				m_toShoot = GetAlienAtVisualPosition(randRow,randColumn);
			}
		}
		else //Target available
		{
			type = "vrpn supplied ";
			progressBar->nextRepetition(true);
		}

		//logging
		std::stringstream ss;
		ss << "Shooting " << type << "alien at: row: " << m_toShoot->VisualGridPosRow << " column: " << m_toShoot->VisualGridPosColumn;
		Logger::LogEvent(ss.str(),true,true);
		ss.str("");//clear stream

		// Shoot the alien.
		int alienType = m_toShoot->destroy(1);

		// Adjust the core and give an exploding sound.
		if (alienType == -1)
		{
			ss << "Error: the type of the alien is not recognized!" << endl;
			Logger::LogEvent(ss.str(),true,true);
			ss.str("");//clear stream

			return false;
		}

		soundManager->explode();

		// If target hit, calculate new score (SOLO)
		if (alienType == Alien::AlienType::Target)
		{
			// Get points dependent on the amount of attempts.
			double multiplicator = (double)std::pow(0.5, (m_currentRepetition - 1));
			//multiplicator goes: 2, 1, 0.5, 0.250 
			int scoreToAdd = int (16000 * multiplicator); // adds => 32 000, 16 000, 8 000, 4 000

			ss << "Adding to current score: " << scoreToAdd << " out of (32 000, 16 000, 8 000, 4 000 ...) for repetion: " << m_currentRepetition << endl;
			Logger::LogEvent(ss.str(), true, true);
			ss.str("");//clear stream

			m_score += scoreToAdd;
		}
		else
		{
			m_score -= 50;
		}

		// Update the score
		ss << "score: " << m_score;

		UpdateScoreText(ss.str());

		//send feedback to OpenVibe whether the user shot the target alien
		if (alienType == Alien::AlienType::Target)
			biApplication->communicationHandler->BI_SetPositiveFeedback();
		else 
			biApplication->communicationHandler->BI_SetNegativeFeedback();

		Helper::delay(50);
		biApplication->communicationHandler->SetFinalized();
		Helper::delay(50);
		//end of feedback

		return alienType == Alien::AlienType::Target;
	}
	else //multiplayer (multi1, multi2, multi3, or multi4)
	{
		bool targetAlienShot = false; //does not matther which player
		string type = "RANDOM ";

		std::stringstream ss;

		//If we do not have a proper target for Player 1 provided by CalculateTarget(), we pick a random one
		if (m_toShoot == NULL)
		{
			int randRow;
			int randColumn;

			while(m_toShoot == NULL || !m_toShoot->alive)
			{
				randRow = rand() % alienMatrixActualColumnsCount();
				randColumn = rand() %    alienMatrixActualRowsCount();

				//Find the alien in the visual grid in position [randRow, randColumn]
				m_toShoot = GetAlienAtVisualPosition(randRow,randColumn);
			}

			Logger::LogEvent("No target received for player1. Performing RANDOM shot.", true, true);
		}
		else //Target available
		{
			type = "vrpn supplied ";
		}

		//If we do not have a proper target for Player2 provided by CalculateTarget(), we pick a random one
		if (m_toShootPlayer2 == NULL)
		{
			int randRow;
			int randColumn;

			while(m_toShootPlayer2 == NULL || !m_toShootPlayer2->alive)
			{
				randRow = rand() % alienMatrixActualColumnsCount();
				randColumn = rand() %    alienMatrixActualRowsCount();

				//Find the alien in the visual grid in position [randRow, randColumn]
				m_toShootPlayer2 = GetAlienAtVisualPosition(randRow,randColumn);
			}

			std::stringstream ss;
			ss << "No target received for Player2. Performing RANDOM shot." << endl;
			Logger::LogEvent(ss.str(),true,true);
			ss.str("");//clear stream
		}

		ss << "Shooting " << type << "alien at: row: " << m_toShoot->VisualGridPosRow << " column: " << m_toShoot->VisualGridPosColumn;
		Logger::LogEvent(ss.str(),true,true);
		ss.str("");//clear stream


		bool isSameRow=m_toShoot->VisualGridPosRow==m_toShootPlayer2->VisualGridPosRow;
		bool isSameColumn=m_toShoot->VisualGridPosColumn==m_toShootPlayer2->VisualGridPosColumn;
		
		// Shoot the alien
		int alienType = -1;
		int alienTypePlayer2 = -1;

		//set explosion texture
		if (isSameRow && isSameColumn)
		{
			alienType = m_toShoot->destroy(1); // destroy() update the target type to -1
			alienTypePlayer2 = alienType; //because this is the same target, alienTypePlayer2 is the same type of alienType
		}
		else
		{
			alienType = m_toShoot->destroy(2);
			alienTypePlayer2 = m_toShootPlayer2->destroy(3);
		}

		bool isFirstTargetDestroyed =  (alienType == Alien::AlienType::Target || alienTypePlayer2 == Alien::AlienType::Target);
		bool isSecondTargetDestroyed = (alienTypePlayer2 == Alien::AlienType::Target2 || alienType == Alien::AlienType::Target2);
		bool areBothTargetsDestroyed = (isFirstTargetDestroyed && isSecondTargetDestroyed);
		bool noTagetDestroyed = (!isFirstTargetDestroyed && !isSecondTargetDestroyed);

		if (alienType != -1 || alienTypePlayer2 != -1) soundManager->explode();

		/////////// Table 1 : MULTIPLAYER SCORE COMPUTATION AT A GIVEN REPETITION ///////////////////////
		//             CASE                            COOPERATION                    COMPETITION
		//  one target had been destroyed             +2000 for both     +5000 for the one /+0 for the other
		// both targets has been destroyed            +5000 for both                +2000 for both
		//  none target had been destroyed             +0 for both                    +0 for both

		// DEBUG : SHOW TYPE
		cout << "Target type p1 " << alienType << ", p2 " << alienTypePlayer2 << endl;

		/////////// Table 2 : MULTIPLAYER MESSAGE TO SENT AFTER DESTRUCTION ///////////////////////
		// In the end, send the information of which target was destroy:
		// cases for AlienDestruction_MessageToSend
		// -1 ERROR: no message sent
		// 0 : Only distractor(s) was/were destroyed during this life
		// 1 : Only Player1's target was destroyed
		// 2 : Only Player2' target was destroyed
		// 3 : Both Player1's and Player2's targets were destroyed
		int AlienDestruction_MessageToSend=0;

		/////////////////// COOPERATIVE - score computation (mode=multi1 OR mode=multi2) //////////////////
		if (m_config.MM_IsCooperative)
		{

			if (m_config.MM_IsSameTarget) //COOPERATIVE same target - we check who shot it and we increase his score (mode=multi1)
			{

				//cout << "p1 is TA" << (alienType == Alien::AlienType::Target) << endl;
				if (alienType == Alien::AlienType::Target) // COOP SAME: P1 destroys THE target (both +2000)
				{
					m_score += 2000;
					m_scorePlayer2 += 2000;
					cout << "You are playing cooperative with 1 target. Player 1 shot the target. +2000 for both" << endl;
					AlienDestruction_MessageToSend=1; // see Table 2
					targetAlienShot = true;
					
				}
				//cout << "p2 is TA" << (alienTypePlayer2 == Alien::AlienType::Target) << endl;
				if (alienTypePlayer2 == Alien::AlienType::Target)  // COOP SAME: P2 destroys THE target (both +2000)
				{
					m_score += 2000;
					m_scorePlayer2 += 2000;
					cout << "You are playing cooperative with 1 targets. Player 2 shot the target. +2000 for both" << endl;
					AlienDestruction_MessageToSend=2; // see Table 2
					targetAlienShot = true;

				}
				//cout << "p1 and p2 are TA" << ((alienTypePlayer2 == Alien::AlienType::Target) && (alienType == Alien::AlienType::Target)) << endl;
				if ((alienTypePlayer2 == Alien::AlienType::Target) && (alienType == Alien::AlienType::Target)) // COOP SAME: P1 and P2 destroy THE target (BONUS both +1000)
				{
					cout << "Both players has destroyed the target. BONUS !!!!! +1000 for both" << endl;
					m_score += 1000;
					m_scorePlayer2 += 1000;
					AlienDestruction_MessageToSend=3; // see Table 2
					targetAlienShot = true;
					
				}
				
			}

			else //COOPERATIVE not-same target - we check who shot it and we increase his score (mode=multi2)
			{

				if ((alienType == Alien::AlienType::Target)||(alienTypePlayer2 == Alien::AlienType::Target)) // if P1 or P2 destroys TA1, +2000 to both
				{
					m_score += 2000;
					m_scorePlayer2 += 2000;
					if (alienType == Alien::AlienType::Target){
					cout << "You are playing cooperative with 2 targets. Player 1 shot HIS target. +2000 for both" << endl;
					}
					if (alienTypePlayer2 == Alien::AlienType::Target){
					cout << "You are playing cooperative with 2 targets. Player 2 shot Player 1's target. +2000 for both" << endl;
					}
					AlienDestruction_MessageToSend=1; // see Table 2
					targetAlienShot = true;
				}
				if ((alienTypePlayer2 == Alien::AlienType::Target2)||(alienType == Alien::AlienType::Target2))  // if P1 or P2 destroys TA2, +2000 to both
				{
					m_score += 2000;
					m_scorePlayer2 += 2000;
					if (alienType == Alien::AlienType::Target2){
					cout << "You are playing cooperative with 2 targets. Player 1 shot Player 2's target. +2000 for both" << endl;
					}
					if (alienTypePlayer2 == Alien::AlienType::Target2){
					cout << "You are playing cooperative with 2 targets. Player 2 shot HIS target. +2000 for both" << endl;
					}
					AlienDestruction_MessageToSend=2; // see Table 2
					targetAlienShot = true;
				}

				if (((alienTypePlayer2 == Alien::AlienType::Target2) && (alienType == Alien::AlienType::Target)) || ((alienTypePlayer2 == Alien::AlienType::Target) && (alienType == Alien::AlienType::Target2))) 
				{ // if both players succeded to simultaneously destroy the targets, bonus points (+1000)
					cout << "Both Target are simultaneously destroyed. BONUS !!!!! +1000 for both" << endl;
					m_score += 1000;
					m_scorePlayer2 += 1000;
					AlienDestruction_MessageToSend=3; // see Table 2
					targetAlienShot = true;
				}
				
			}
		}
		else /////////////////// COMPETITIVE - score computation (mode=multi3 OR mode=multi4) //////////////////
		{
			if (m_config.MM_IsSameTarget) //COMPETITIVE same target (multi3) - we check who shot it and we increase his score
			{
				if (alienType == Alien::AlienType::Target) // if the player 1 manage to destroy THE target, give P1 5000 points
				{
					m_score += 5000;
					cout << "You are playing competitive with 1 target. Player 1 shot the target. Player1 current score is: " << m_score << endl;
					AlienDestruction_MessageToSend=1; // see Table 2
					targetAlienShot = true;
				}
				if (alienTypePlayer2 == Alien::AlienType::Target)  // if the player 1 manage to destroy THE target, give P2 5000 points
				{
					m_scorePlayer2 += 5000;
					cout << "You are playing competitive with 1 target. Player 2 shot the target. Player2 current score is: " << m_scorePlayer2 << endl;
					AlienDestruction_MessageToSend=2; // see Table 2
					targetAlienShot = true;
				}

				if ((alienTypePlayer2 == Alien::AlienType::Target) && (alienType == Alien::AlienType::Target))//if both P1 and P2 destroyed the target give -3000 to both
				{
					m_score -= 3000;
					m_scorePlayer2 -= 3000;
					cout << "BOTH players have destroyed the target, PENALITY !!! -3000 points" << endl;
					AlienDestruction_MessageToSend=3; // see Table 2
					targetAlienShot = true;
				}
			}
			else //COMPETITIVE not same target (multi4)
			{
				if ((alienType == Alien::AlienType::Target) || (alienTypePlayer2 == Alien::AlienType::Target)) // if the P1 or P2 shot TA1, give P1 5000 points
				{
					m_score += 5000;
					if (alienType == Alien::AlienType::Target){
					cout << "Player 1 shot his target. Player1 current score is: " << m_score << endl;
					}
					if (alienTypePlayer2 == Alien::AlienType::Target){
					cout << "Player 2 shot Player's 1 target. Player1 current score is: " << m_score << endl;
					}
					AlienDestruction_MessageToSend=1; // see Table 2
					targetAlienShot = true;
				}

				if ((alienTypePlayer2 == Alien::AlienType::Target2)||(alienType == Alien::AlienType::Target2)) // if the P1 or P2 shot TA2, give P2 5000 points
				{
					m_scorePlayer2 += 5000;
					if (alienType == Alien::AlienType::Target2){
					cout << "Player 1 shot Player 2's target. Player2 current score is: " << m_scorePlayer2 << endl;
					}
					if (alienTypePlayer2 == Alien::AlienType::Target2){
					cout << "Player 2 shot Player 2's target. Player2 current score is: " << m_scorePlayer2 << endl;
					}
					AlienDestruction_MessageToSend=2; // see Table 2
					targetAlienShot = true;
				}

				if (((alienTypePlayer2 == Alien::AlienType::Target2) && (alienType == Alien::AlienType::Target)) || ((alienTypePlayer2 == Alien::AlienType::Target) && (alienType == Alien::AlienType::Target2)))
					//if both P1 and P2 destroyed the targets simultaneously: Give MALUS -3000 to both
				{
					m_score -= 3000;
					m_scorePlayer2 -= 3000;
					cout << "BOTH players have destroyed their target, PENALITY !!!" << endl;
					AlienDestruction_MessageToSend=3; // see Table 2
					targetAlienShot = true;
				}
			}
		}

		//Start Feedback 
		//For Martine's experiment we reuse these 4 messages BI_SetPositiveFeedback, BI_SetNegativeFeedback, BI_SetAlienDestroyedFeedback and BI_SetNoTargetDestroyed to mark when exactly aliens were destroyed
		if (AlienDestruction_MessageToSend==3) //in the same time
		{
			biApplication->communicationHandler->BI_SetAlienDestroyedFeedback(); //code 111 (ov ???)
		}
		else if (AlienDestruction_MessageToSend==1)
		{
			biApplication->communicationHandler->BI_SetPositiveFeedback();// code 105 (ov 781)
		}
		else if (AlienDestruction_MessageToSend==2)
		{
			biApplication->communicationHandler->BI_SetNegativeFeedback();// code 106 (ov 782)
		}
		else if (AlienDestruction_MessageToSend==0)
		{
			biApplication->communicationHandler->BI_SetNoTargetDestroyed();//code 112 (ov ???)
		}
		
		Helper::delay(50);
		biApplication->communicationHandler->SetFinalized();
		Helper::delay(50);
		//End Feedback
		
		//Display on screen
		ss << "score: " << m_score << "  " << m_scorePlayer2;
		
		UpdateScoreText(ss.str());

		if (!targetAlienShot)
			progressBar->nextRepetition(true);

		//////////////////// THE FOLLOWING PART IS WHEN THERE ARE MORE THAN 1 LIFE, can be bugged ////////////////////////
		//policy: in cooperative 2 target - they advance together
		if (m_config.MM_IsCooperative && !m_config.MM_IsSameTarget && targetAlienShot == true) //with two targets
		{
			if (areBothTargetsDestroyed || m_isFirstTargetDestroyedCoop2Targets) 
			{
				cout << "You are playing cooperative with 2 targets. Both targets are now destroyed. You both gain score. Your current scores are Player1: " << m_score << " Player2: " << m_scorePlayer2 << endl;
				return true; //both targets shot
			}
			else 
			{
				m_isFirstTargetDestroyedCoop2Targets = true;
				cout << "You are playing cooperative with 2 targets. One target alien shot, one more to go." << endl;
				return false; //the game will continue
			}
		}

		return targetAlienShot; //if true the win screen will load next
	}
	
}

void GameScreen::visible(bool isVisible)
{
	// We also want invisibility if we're at a Black Screen.
	bool areAliensVisible = !(m_currentState == BlackScreen || !isVisible);

	for (int i = 0; i < MAXMATRIXSIZE; i++){
		for (int j = 0; j < MAXMATRIXSIZE; j++){
			if (alienMatrix[i][j] != NULL)
				alienMatrix[i][j]->setVisible(areAliensVisible);
		}
	}

	// Show the progressBar (lets hide it in training Mode)
	progressBar->visible(areAliensVisible && !m_isTrainingMode);

	//anton: code rewritten
	//if (areAliensVisible  && !m_isTrainingMode)
	//{
	//	//Logger::LogEvent("Show overlay",true,true);
	//	m_overlay->show();
	//}
	//else
	//	//overlay->hide();

	//	// Reset the timers, since they probably ran out.
	//	if (isVisible)
	//	{
	//		Logger::LogEvent("Reset timers", true, true);
	//		stateTimer.reset();
	//		movementTimer.reset();
	//	}

	if ((!areAliensVisible || m_isTrainingMode) && isVisible)
	{
		//Logger::LogEvent("Reset timers", true, true);
		stateTimer.reset();
		movementTimer.reset();
	}
}

void GameScreen::keyPressed(const OIS::KeyEvent& evt)
{
	if ( evt.key == OIS::KC_ESCAPE)
	{
		// Go to main menu if Escape is hit.
		biApplication->LeaveGame();

		if (m_config.ManageOpenVibeEnabled)
		{
			Logger::LogEvent("Stopping OpenVibe ...",true,true);
			/*#if defined TARGET_OS_Windows
			m_openvibe.StopScenario();
			#endif*/
		}
	}

	if ( evt.key == OIS::KC_RBRACKET) // Cheat Button to kill the  target. Usefull for level testing.
	{
		//1. Find the target
		pair<int, int> l_Target;

		int actualRowCount = alienMatrixActualRowsCount();
		int actualColumnCount = alienMatrixActualColumnsCount();
		int matrixElementCount = actualRowCount * actualColumnCount;

		for (int i = 0; i < actualRowCount; i++)
		{
			for (int j = 0; j < actualColumnCount; j++)
			{
				if (alienMatrix[j][i]->getType() == Alien::AlienType::Target)
				{
					l_Target.first = j;
					l_Target.second = i;
				}
			}
		}

		//2. Set the target as the alien shot by the user
		m_toShoot = alienMatrix[l_Target.first][l_Target.second];

		//3. Shoot the selected Alien
		m_bIsTargetShot = ShootAlien(); //Calculates the score

		//4. Next state
		m_currentState = ShowExplosion;
	}

	//Manually resume game if you need to
	//This sends the reset signal to OpenVibe, OpenVibe sends back a resume game flag from the Potato
	if ( evt.key == OIS::KC_R && m_currentState == OnHold)
	{
		biApplication->communicationHandler->SetResetFlag();
		Helper::delay(50);
		biApplication->communicationHandler->SetFinalized();
		Helper::delay(50);
	}
}

bool GameScreen::ProcessLevel(int level,state gameState)
{
	Logger::LogEvent("Process level.",true,true);

	//Start clean-up
	// Clean old level
	for(int i = 0; i < MAXBLOCKS; i++)
	{
		if(alienBlocks[i] != NULL)
			delete alienBlocks[i];
	}

	// Make sure the matrices are clean so we dont use any old memory
	CleanMatrices();
	//End clean-up

	//Start new level:

	// ISI generation, adapt ISI, set first value
	if (!m_firstPlay && m_config.ISIAdaptationEnabled) 
	{	
		AdjustMeanISI();
	}
	GenerateNewISISequence();
	ISITime = GetNextISI();

	// Load level from file
	if (m_config.MM_IsMultiplayer && m_totalLevelsPlayed == m_config.MM_LevelCount) 
	{
		Logger::LogEvent("Maximum number of levels reached for mode Multiplayer.",true,true);
		return false;
	}

	bool levelLoaded = LoadLevel(level);
	if (!levelLoaded) return false; //stop here if not more levels

	// Shuffling the matrix to ensure random flashing and such
	ShuffleMatrix();

	// Start a new repetition cycle.
	m_currentRepetition = 0; //restart repetition counter

	m_bResetTabP300 = true;
	m_bResetTabP300Player2 = true;

	progressBar->reset(m_config.Lives);
	m_currentState = gameState; //setting initial state

	// We must regeneate the flashes according to the new matrix size in the just loaded level
	GenerateFlashSequence();

	if (m_isTrainingMode)
	{
		// We need targets to train on.
		m_vTargetSequence = GenerateTargetSequence(m_config.MaxTrainingTargets);
	}
	else
	if  (m_config.MM_IsMultiplayer) //because we use the training level and tre training level does not have a target, we need to generate it
	{
		m_vTargetSequence = GenerateTargetSequence(1);//currently we use the training level each time in Multiplayer

		if (!m_config.MM_IsSameTarget)
		{
			m_vTargetSequencePlayer2 = GenerateTargetSequence(1);

			//regenerate target for second player until the second target is not flashed together with the first trarget
			while ( (m_vTargetSequencePlayer2.front().first == m_vTargetSequence.front().first) || (m_vTargetSequencePlayer2.front().second == m_vTargetSequence.front().second) )
			{
				m_vTargetSequencePlayer2 = GenerateTargetSequence(1);
			}
		}

		//add a second score in the beginning
		std::stringstream ss;
		ss << "score: " << m_score << "  " << m_scorePlayer2;
		UpdateScoreText(ss.str());
	}

	m_firstPlay = false;
	//end new level 

	return true;
}

void GameScreen::Initialize(bool isTraining)
{
	Logger::LogEvent("Initializing GameScreen ...",true,false);

	m_flashesSinceLastRepetition = 0;

	m_onHold=false;

	flashTime = m_config.FlashNonTargetTime;

	m_isTrainingMode = isTraining;

	m_firstPlay = true;

	state gameState = BlackScreen; //set default state

	m_currentLevel = 1;

	m_totalLevelsPlayed = 0;

	Logger::LogEvent(m_config.GetConfiguration(),true,false);

	if (m_config.ManageOpenVibeEnabled)
	{
		/*#if defined TARGET_OS_Windows
		m_openvibe.StartScenario();
		#endif*/
		Logger::LogEvent("Waiting for OpenVibe to start ...",true,true);

		//Wait and then resume
		m_onHold = true;
		m_cachedState = BlackScreen; //return state
		gameState = OnHold;
	}

	// Load in the first level, so we can start playing immediately.
	ProcessLevel(m_currentLevel,gameState);//sets state to = BlackScreen

	if (m_config.ManageOpenVibeEnabled)
	{
		Helper::delay(3000);
		//Reinitialize VRPN
		if (m_poVrpnPeripheral!=NULL) delete m_poVrpnPeripheral;
		m_poVrpnPeripheral = new CAbstractVrpnPeripheral(this->m_config.VrpnPeripheral);
		m_poVrpnPeripheral->init();
	}

	//Multiplayer looging and setting the condition multi1,2,3,4
	if (m_config.MM_IsMultiplayer)
	{
		cout<< endl;
		cout<< "Checking game mode just before starting the game: " << endl;
		cout << "IsCooperative = " << ((m_config.MM_IsCooperative) ? "yes" : "no")  << endl;
		cout << "IsSameTarget = " << ((m_config.MM_IsSameTarget) ? "yes" : "no")  << endl;

		if (m_config.MM_IsCooperative && m_config.MM_IsSameTarget) biApplication->communicationHandler->BI_SetMultiplayerCooperativeSync(); else
			if (m_config.MM_IsCooperative && !m_config.MM_IsSameTarget) biApplication->communicationHandler->BI_SetMultiplayerCooperativeNoSync(); else
				if (!m_config.MM_IsCooperative && m_config.MM_IsSameTarget) biApplication->communicationHandler->BI_SetMultiplayerCompetitiveSync(); else
					if (!m_config.MM_IsCooperative && !m_config.MM_IsSameTarget) biApplication->communicationHandler->BI_SetMultiplayerCompetitiveNoSync();

		Helper::delay(50);
		biApplication->communicationHandler->SetFinalized();
		Helper::delay(50);
	}
}

void GameScreen::FinishLevel()
{
	// Make sure all blocks and the Matrix are cleaned up.
	for(int i = 0; i < MAXBLOCKS; i++)
	{
		if(alienBlocks[i] != NULL)
			delete alienBlocks[i];
	}

	CleanMatrices();

	// Try to load the next Level;
	bool levelLoadedSuccessfully = ProcessLevel(m_currentLevel,BlackScreen);

	if (!levelLoadedSuccessfully) //if no more levels it goes to main menu
	{
		std::stringstream ss;
		ss << "Final score Player 1: " << m_score;
		if (m_config.MM_IsMultiplayer)
		{
			ss << std::endl << "Final score Player2: " << m_scorePlayer2;
		}
		Logger::LogEvent(ss.str(),true,true);

		// If loadLevel is unsuccesful load first level and return to main menu.
		biApplication->FinishGame();
	}
	else //returns to game
	{
		biApplication->activateScreen(BrainInvaders::BrainInvadersApplication::MainGame);
	}
}

GameScreen::~GameScreen()
{
	delete progressBar;
}

void GameScreen::SetUpOverlay()
{
	//docs: http://www.ogre3d.org/tikiwiki/Manual+Resource+Loading#Introduction_5

	//Logger::LogEvent("Setting up overlay ...",true,true);

	//anton: code rewritten
	#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
		// on Ogre 1.9, overlay system needs to be manually created
		//Logger::LogEvent("Init OverlaySystem\n", true, true);
		Ogre::OverlaySystem* pOverlaySystem = new Ogre::OverlaySystem();
		m_sceneManager->addRenderQueueListener(pOverlaySystem); //anton: http://www.ogre3d.org/tikiwiki/tiki-index.php?page=GhadamonNotes#Porting_Notes
	#endif

	OverlayManager* overlayMgr = OverlayManager::getSingletonPtr();

	Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(overlayMgr->createOverlayElement("Panel", "PanelName"));
	panel->_setPosition((Ogre::Real) 0.05, (Ogre::Real) 0.925);
	panel->setDimensions(300, 120);

	TextAreaOverlayElement* textArea = static_cast<TextAreaOverlayElement*>(overlayMgr->createOverlayElement("TextArea", "TextAreaScore"));
	textArea->setMetricsMode(Ogre::GMM_PIXELS);
	textArea->setPosition(0, 0);
	textArea->setDimensions(300, 120);
	textArea->setCharHeight(24);
	textArea->setColour(ColourValue(1,1,0));
	
	//Logger::LogEvent("Creating GameFont ...", true, true);
	Ogre::FontPtr mFont = Ogre::FontManager::getSingleton().create("GameFont", "General");
	mFont->setType(Ogre::FT_TRUETYPE);
	mFont->setSource("Smirnof.ttf");
	mFont->setTrueTypeSize(20);
	mFont->setTrueTypeResolution(96);

	// set the font name to the font resource that you just created.
	//Logger::LogEvent("Setting up GameFont ...", true, true);
	textArea->setFontName("GameFont");

	//add elements
	m_overlay = overlayMgr->create("OverlayName");
	m_overlay->add2D(panel);

	panel->addChild(textArea);

	//set text to the score textbox
	std::stringstream ss;
	ss << "score: " << m_score;
	UpdateScoreText(ss.str());

	//show overlay
	m_overlay->show();
}

void GameScreen::UpdateScoreText(std::string text)
{
	//Logger::LogEvent("UpdateScoreText: " + text,true,true);
	m_overlay->getChild("PanelName")->getChild("TextAreaScore")->setCaption(text.c_str());
}

std::string GameScreen::GetScoreText()
{
	std::string text = m_overlay->getChild("PanelName")->getChild("TextAreaScore")->getCaption();
	return text;
}

void GameScreen::FixLayout(bool show)
{
	//anton: fixes a bug in Ogre that prevents Brain Invaders from showing the overlay(the score) in the beginning
	//The place where you call ShowLayout() is important
	//The fix is to set the value again
	//http://www.ogre3d.org/forums/viewtopic.php?f=2&t=58047&start=0

	if (!m_isTrainingMode
		&& 
		show
		&& !m_bIsScoreTextInitialized)
	{
		UpdateScoreText(GetScoreText());

		m_bIsScoreTextInitialized = true;
	}
}



