/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#ifndef __OpenViBEApplication_KeyboardScreen_H__
#define __OpenViBEApplication_KeyboardScreen_H__

#include "SpaceInvadersScreen.h"
#include "BrainInvadersApplication.h"
#include "Alien.h"
#include "AlienBlock.h"
#include "SoundManager.h"
#include "ProgressBar.h"
#include "../Config.h"
#include "../Helper.h"
#include "BrainInvadersScreen.h"
#include <random>
using namespace BrainInvaders;

#include <Ogre.h>
using namespace Ogre;

#define MAXMATRIXSIZE 25
#define MAXBLOCKS 36

namespace BrainInvaders {

	/*!
	 * \author Anton Andreev (Gipsa)
	 * \date 2013-05-12
	 * \brief The Game Screen of Brain Invaders.
	 *
	 * \details On-screen keybaord 
	 * 
	 * 
	 */
	class KeyboardScreen : public BrainInvadersScreen
	{

	public:

		KeyboardScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CAbstractVrpnPeripheral * vrpnPeripheral, Config config);
		
		void LoadAssets();

		/**
		* \brief Updates the Game Screen, handling all Game logic and Managing Flashing and BCI input.
		* \return indicates whether to continue.
		*/
		virtual bool update(double timeSinceLastUpdate);

		/**
		* \brief Sets the screens visibility.
		* \param isVisible Indicates whether the screen should be visible.
		*/
		virtual void visible(bool isVisible);

		/**
		* \brief Handles Keyboard events occuring in this screen.
		* \param evt The Keyboard Event to be processed.
		*/
		virtual void keyPressed(const OIS::KeyEvent& evt);
	};
};
#endif