/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "Alien.h"
using namespace BrainInvaders;

#include <Ogre.h>        ////////////////////////difference
using namespace Ogre; 

#include <string>
#include <sstream>
using namespace std;

Alien::Alien(Ogre::SceneManager *mSM, string pName, int pType)
{
	name = pName;
	type = pType;
	realType = type;
	alienEntity = mSM->createEntity(name,"cube.mesh");
	
	frame = 0;
	flashing = false;
	alive = true;

	alienEntity->setMaterialName(getMaterialName());//material is the texture

	VisualGridPosRow = -1;
	VisualGridPosColumn = -1;
}

Alien::~Alien()
{	
	//We want to get rid of all Entities attached to this object.
	SceneNode::ObjectIterator itObject = alienNode->getAttachedObjectIterator();
	// Iterate through all objects and destroy them.
	while ( itObject.hasMoreElements() )
	{
		MovableObject* pObject = static_cast<MovableObject*>(itObject.getNext());
		alienNode->getCreator()->destroyMovableObject( pObject );
	}
	//delete alienEntity;
}

void Alien::setSceneNode(SceneNode *pSN)
{
	alienNode = pSN;
	alienNode->scale((Ogre::Real)0.65, (Ogre::Real)0.65, (Ogre::Real)0.65);
	alienNode->showBoundingBox(false);
	alienNode->attachObject(alienEntity);
	// Save the scale for resizing purposes
	scale = alienNode->getScale();
}

void Alien::ShowTargetMode()
{
	std::stringstream ss;
	ss << "Spaceinvader/Alien_" << type << "_T";
	alienEntity->setMaterialName(ss.str());
}

void Alien::Flash()
{
	flashing = true;
	// Adjust the scale if this is a Target Alien
	if (type == Target || type == Target2)
		alienNode->setScale(1.5*scale);
	// We only flash ALiens that are still alive.
	if (alive)
		alienEntity->setMaterialName(getMaterialName());
	//std::cout<<"Flash Alien Fin"<<std::endl;
}

void Alien::UnFlash()
{
	flashing = false;
	// Reset scale if this is a Target Alien
	if (type == Target || type == Target2)
		alienNode->setScale(scale);
	// We only (un)flash Aliens that are still alive.
	if (alive)
		alienEntity->setMaterialName(getMaterialName());
	//std::cout<<"Unflash Alien Fin"<<std::endl;
}

void Alien::animate()
{
	// We animate if the Alien is alive, else we make it invisible (ensuring an explosion lasts a single animation frame)
	if (alive){
		// NOTE: For more complex animations this function needs to be adjusted to support more frames.
		frame = (frame + 1) % 2;
		alienEntity->setMaterialName(getMaterialName());
	}
	else{
		setVisible(false);
	}
}

string Alien::getMaterialName()
{
	std::stringstream ss;
	// Create a string of the form "Spaceinvader/Alien_<int:TypeValue>_<int:FrameValue>_<String:FlashValue (either U or F)>"
	if (flashing)
		ss << "Spaceinvader/Alien_" << type << "_" << frame << "_F";
	else
		ss << "Spaceinvader/Alien_" << type << "_" << frame << "_U";
	return ss.str();
}

void Alien::setVisible(bool visible)
{
	alienNode->setVisible(visible && alive);
}

int Alien::destroy(int TextureExplosion)
{
	// isPlayer1==1 or else --> default texture for the explosion
	// isPlayer1==2 --> player 1's texture for the explosion
	// isPlayer1==3 --> player 2's texture for the explosion
	
	if(!alive)
		return -1;

	// Rescale explosion - currently hardcoded 
	alienNode->setScale(5*scale);
	
	// Make sure the Alien is dead.
	alive = false;

	// Set texture to explosion.
	if (TextureExplosion==2) // show multiplayer explosion texture of player 1
	{
		alienEntity->setMaterialName("Spaceinvader/Explosion2");
	}
	else if(TextureExplosion==3) // show multiplayer explosion texture of player 2
	{
		alienEntity->setMaterialName("Spaceinvader/Explosion3");
	}
	else //if solo or both players destroyed the target, show default explosion
	{
		alienEntity->setMaterialName("Spaceinvader/Explosion");
	}
	// Return the type so GameScreen knows how to handle points.
	return type;
}

int Alien::getType(){
	return type;
}

// for training
void Alien::makeTarget(bool isTarget,bool isFirstPlayer){
	// Use this to switch between target and normal states for training purposes.
	if (isTarget)
	{
		if (isFirstPlayer)
			type = Target;
		else type = Target2;
	}
	else
		type = realType;
}
