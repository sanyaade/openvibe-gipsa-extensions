/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#include "BrainInvadersApplication.h"
#include "SpaceInvadersScreen.h"
using namespace BrainInvaders;

#include <Ogre.h>
#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
#include <Overlay\OgreFont.h>	
#include <Overlay\OgreFontManager.h>
#include <Overlay\OgreOverlayElement.h>
#else
#include <OgreFont.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>
#endif
using namespace Ogre;

#include <stdlib.h>

SpaceInvadersScreen::SpaceInvadersScreen(BrainInvadersApplication *scrMan, SceneManager *mSM)
{
	biApplication = scrMan;
	m_sceneManager = mSM;
}

void SpaceInvadersScreen::visible(bool visible)
{

}

bool SpaceInvadersScreen::update(double timeSinceLastUpdate)
{
	return true;
}

void SpaceInvadersScreen::keyPressed(const OIS::KeyEvent& evt)
{
}