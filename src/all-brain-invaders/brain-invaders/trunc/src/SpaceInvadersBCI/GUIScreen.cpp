#include "GUIScreen.h"
#include "BrainInvadersApplication.h"
using namespace BrainInvaders;

#include <Ogre.h>
/* Project: Brain Invaders, P300 BCI game developed by Gipsa-lab 
 * AUTHORS AND CONTRIBUTORS: Andreev A., Goyat M., Van Veen G., Varnet L.  
 * RESEARCH TEAM: Andreev A., Barachant A., Congedo M., Van Veen G. 
 * AKNOWLEDGEMENTS: This project has been partially funded by ANR Project OpenViBE2, RoBIK and by AFM (Association Fran�aise contre les Myopathies). 
 * REFERENCES: Congedo M., Goyat M., Tarrin N., Varnet L., Rivet B., Ionescu G., Jrad N., Phlypo R., Acquadro M., Jutten C. (2011) �Brain Invaders�: a prototype of an open-source P300-based video game working with the OpenViBE platform Proceedings of the 5th International BCI Conference, Graz, Austria, 280-283 
 
 * This file is part of Brain Invaders.
 * Brain Invaders is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * Brain Invaders is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with Brain Invaders. If not, see http://www.gnu.org/licenses/.*/

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	#include <Overlay\OgreFont.h>	
	#include <Overlay\OgreFontManager.h>
	#include <Overlay\OgreOverlayElement.h>
#else
	#include <OgreFont.h>
	#include <OgreFontManager.h>
	#include <OgreTextAreaOverlayElement.h>
#endif
	#include <CEGUI.h>
	#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	#include <CEGUI/RendererModules/Ogre/Renderer.h>
	#else
	#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#endif
#include <OIS.h>
using namespace Ogre;

#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
	typedef CEGUI::UVector2 USize;
};
#endif

#include <stdlib.h>
#include <stdlib.h> 

#include "../Logger.h"

GUIScreen::GUIScreen(BrainInvadersApplication *ScrMan, SceneManager *mSM, CEGUI::WindowManager *mWM, CEGUI::Window *m_poSheet) : SpaceInvadersScreen(ScrMan, mSM)
{
	m_poGUIWindowManager = mWM;
	
	// Main path is in resources.cfg
	const std::string l_sFondMenu = "Resources/GUI/brainInvadersMain.png";
	const std::string l_sTextMenuSpace = "Resources/GUI/startText.png";
	const std::string l_sGUIMenu_multi1 = "Resources/GUI/BI_multi1.png";
	const std::string l_sGUIMenu_multi2 = "Resources/GUI/BI_multi2.png";
	const std::string l_sGUIMenu_multi3 = "Resources/GUI/BI_multi3.png";
	const std::string l_sGUIMenu_multi4 = "Resources/GUI/BI_multi4.png";
	
	//----------- CREATE WINDOWS -------------//

	// background window
	createOgreWindow(m_poSheet, l_sFondMenu, "FondMenu", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	//----------- MULTI GUI SCREEN -------------//
	
	// background multi1 cooperation same target
	createOgreWindow(m_poSheet, l_sGUIMenu_multi1, "GUIMenu_multi1", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	// background multi2 cooperation not-same target
	createOgreWindow(m_poSheet, l_sGUIMenu_multi2, "GUIMenu_multi2", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	// background multi3 competition same target
	createOgreWindow(m_poSheet, l_sGUIMenu_multi3, "GUIMenu_multi3", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	// background multi4 competition not-same target
	createOgreWindow(m_poSheet, l_sGUIMenu_multi4, "GUIMenu_multi4", CEGUI::UVector2(cegui_reldim(0.0f), cegui_reldim(0.0f)), CEGUI::USize(CEGUI::UDim(1.0f, 0.f), CEGUI::UDim(1.0f, 0.f)));

	// text menu 2
	createOgreWindow(m_poSheet, l_sTextMenuSpace, "TextMenuSpace", CEGUI::UVector2(cegui_reldim(0.25f), cegui_reldim(0.8f)), CEGUI::USize(CEGUI::UDim(0.5f, 0.f), CEGUI::UDim(0.1f, 0.f)));
}

void GUIScreen::createOgreWindow(CEGUI::Window *l_poSheet, 
	                             std::string imageResourceParth, 
								 std::string windowName, 
								 CEGUI::UVector2 windowPosition, 
								 CEGUI::USize windowSize)
{
	CEGUI::Window * window = m_poGUIWindowManager->createWindow("TaharezLook/StaticImage", windowName /*"FondMenu"*/);

	//anton: code rewritten
	window->setPosition(windowPosition);
	window->setSize(windowSize);

	std::string imageSetName = "Image" + windowName;

	//Logger::LogEvent("Adding: " + imageSetName + std::string(" ") + imageResourceParth, true, true);

	#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
		l_poSheet->addChild(window);
		CEGUI::ImageManager::getSingleton().addFromImageFile(imageSetName, imageResourceParth /*l_sFondMenu*/);
		window->setProperty("Image",imageSetName);
	#else
		l_poSheet->addChildWindow(window);
		CEGUI::ImagesetManager::getSingleton().createFromImageFile(imageSetName, imageResourceParth /*l_sFondMenu*/);
		window->setProperty("Image", "set:" + imageSetName + " image:full_image");
	#endif

	window->setProperty("FrameEnabled", "False");
	window->setProperty("BackgroundEnabled", "False");

	GUIWindows.push_back(window);
}

void GUIScreen::visible(bool visible)
{
	startMenuVisible = true;
	elapsedTime = 0;

	//anton: code rewritten
	//m_poGUIWindowManager->getWindow("FondMenu")->setVisible(visible);
	//m_poGUIWindowManager->getWindow("TextMenuSpace")->setVisible(visible);
	GUIWindows[0]->setVisible(visible);
	GUIWindows[5]->setVisible(visible);

	for (int i = 1; i < 5; i++)
		GUIWindows[i]->setVisible(false);
	/*m_poGUIWindowManager->getWindow("GUIMenu_multi1")->setVisible(false);
	m_poGUIWindowManager->getWindow("GUIMenu_multi2")->setVisible(false);
	m_poGUIWindowManager->getWindow("GUIMenu_multi3")->setVisible(false);
	m_poGUIWindowManager->getWindow("GUIMenu_multi4")->setVisible(false);	*/
}

bool GUIScreen::update(double timeSinceLastUpdate)
{
	elapsedTime += timeSinceLastUpdate;

	// Lets blink the TextMenu. Gives it a more "gamey" feel.

	//anton: code rewritten
	//m_poGUIWindowManager->getWindow("FondMenu")->setVisible(!Config::MM_IsMultiplayer );
	GUIWindows[0]->setVisible(!Config::MM_IsMultiplayer);

	/*m_poGUIWindowManager->getWindow("GUIMenu_multi1")->setVisible(Config::MM_IsMultiplayer && Config::MM_IsCooperative  && Config::MM_IsSameTarget);
	m_poGUIWindowManager->getWindow("GUIMenu_multi2")->setVisible(Config::MM_IsMultiplayer && Config::MM_IsCooperative  && !Config::MM_IsSameTarget);
	m_poGUIWindowManager->getWindow("GUIMenu_multi3")->setVisible(Config::MM_IsMultiplayer && !Config::MM_IsCooperative  && Config::MM_IsSameTarget);
	m_poGUIWindowManager->getWindow("GUIMenu_multi4")->setVisible(Config::MM_IsMultiplayer && !Config::MM_IsCooperative  && !Config::MM_IsSameTarget);
	*/

	GUIWindows[1]->setVisible(Config::MM_IsMultiplayer && Config::MM_IsCooperative  && Config::MM_IsSameTarget);
	GUIWindows[2]->setVisible(Config::MM_IsMultiplayer && Config::MM_IsCooperative  && !Config::MM_IsSameTarget);
	GUIWindows[3]->setVisible(Config::MM_IsMultiplayer && !Config::MM_IsCooperative  && Config::MM_IsSameTarget);
	GUIWindows[4]->setVisible(Config::MM_IsMultiplayer && !Config::MM_IsCooperative  && !Config::MM_IsSameTarget);

	if(elapsedTime > 0.8)
	{
		elapsedTime = 0;
		startMenuVisible = !startMenuVisible;
		//anton: code rewritten
		//m_poGUIWindowManager->getWindow("TextMenuSpace")->setVisible(startMenuVisible);
		GUIWindows[5]->setVisible(startMenuVisible);
	}
	return true;
}

void GUIScreen::keyPressed(const OIS::KeyEvent& evt)
{
	if ( evt.key == OIS::KC_ESCAPE)
	{
		// Exit the game completely at escape.
		biApplication->ExitGame();
	}
	if ( evt.key == OIS::KC_SPACE)
	{
		// Space Starts a new Game in on-line mode
		biApplication->StartGame(false);
	}
	if ( evt.key == OIS::KC_T)
	{
		// The T starts the Training round.
		biApplication->StartGame(true);
	}
	if ( evt.key == OIS::KC_K){
		// The K starts the Keyboard screen
		biApplication->activateScreen(BrainInvadersApplication::Keyboard);
	}
}