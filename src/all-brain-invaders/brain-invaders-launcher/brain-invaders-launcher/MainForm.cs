﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

using Microsoft.Win32; // it's required for reading/writing into the registry:
using System.Diagnostics;
using System.Text;

using System.Windows.Forms.DataVisualization.Charting;
using System.Threading;
using System.Threading.Tasks;
using System.IO.Ports;

namespace brain_invaders_launcher
{
    public partial class MainForm : Form
    {
        OpenFileDialog openFileDialog1;
        string configFilePath;
        Random rng;
        bool IsAdaptive;

        OpenVibeTaggingWrapper tagger;
        CancellationToken ctTestTagger;
        CancellationTokenSource ctsTestTagger;

        string lastComPortUsedForTagger;

        private static string brainInvadersStartScript = "ov-brain-invaders.cmd";

        public MainForm()
        {
            InitializeComponent();
            comboBoxModality.SelectedIndexChanged += new EventHandler(comboBoxModality_SelectedIndexChanged);

            this.Text += " " + Helper.Version;
            comboBoxTaggingTestMethod.SelectedIndex = 1;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            rng = new Random();
            openFileDialog1 = new OpenFileDialog();

            string configGuess = Directory.GetCurrentDirectory() + "\\share\\brain-invaders.conf";

            //check in the same folder chain
            if (File.Exists(configGuess))
            {
                ReadFile(configGuess);
                openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory() + "\\share";

                //Log file
                this.textBoxLogDisc.Text = Directory.GetCurrentDirectory() +"\\share\\openvibe-applications\\brain-invaders\\brain-invaders\\GameSession.txt";
            }

            //check using the install folder
            else
            {
                string InstallPath = OpenVibeController.DetectOpenVibeInstallFolder();
                if (InstallPath != "" && Directory.Exists(InstallPath))
                {
                    ReadFile(InstallPath + "\\share\\brain-invaders.conf");
                    openFileDialog1.InitialDirectory = InstallPath + "\\share";

                    this.textBoxLogDisc.Text = InstallPath + "\\share\\openvibe-applications\\brain-invaders\\brain-invaders\\GameSession.txt";
                }
                else
                {
                    MessageBox.Show("Valid Openvibe install path could not be detected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            textBoxOpenVibeFolder.Text = OpenVibeController.DetectOpenVibeInstallFolder();
            comboBoxModality.SelectedIndex = 0;
            //buttonRandomize_Click(null, null);

            Image[] pics = { global::brain_invaders_launcher.Properties.Resources.cool1, global::brain_invaders_launcher.Properties.Resources.cool2, global::brain_invaders_launcher.Properties.Resources.cool3, global::brain_invaders_launcher.Properties.Resources.cool4, global::brain_invaders_launcher.Properties.Resources.cool5, global::brain_invaders_launcher.Properties.Resources.cool6, global::brain_invaders_launcher.Properties.Resources.cool7 }; 
            Random random = new Random();
            this.pictureBox1.Image = pics[random.Next(0, pics.Count() - 1)];

            comboBoxP300Channel.SelectedIndex = 6;

            //set path for the P300 comparison - for the reference
            textBoxP300Reference.Text = textBoxOpenVibeFolder.Text + @"\share\openvibe\scenarios\gipsa-lab\MDM\P300\brain-invaders-mdm-adaptive\online-adaptive-mdm.xml";
        }

        /// <summary>
        /// Parse BI the brain-invaders.conf
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool ParseFile(string file)
        {
            System.Globalization.CultureInfo oldCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            string[] lines = file.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            int i = 0;
            string[] field_value;
            try
            {
                foreach (string line in lines)
                {
                    i++;
                    if (string.IsNullOrEmpty(line) || line[0] == '#' || line.Trim()[0] == '\r' || line.Trim()[0] == '\n' || line.Trim() == "") continue;

                    field_value = Regex.Split(line, ":");
                    field_value[0] = field_value[0].Trim();
                    field_value[1] = field_value[1].Trim();

                    //if (System.Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag == (new System.Globalization.CultureInfo("fr-FR")).IetfLanguageTag) 
                    //        field_value[1] = field_value[1].Replace(".", ","); //french localization correction

                    if (field_value[0] == "CommProtocol" && field_value[1].ToLower() == "software") this.comboBoxTagging.SelectedIndex = 0;
                    if (field_value[0] == "CommProtocol" && field_value[1].ToLower() == "serial") this.comboBoxTagging.SelectedIndex = 1;
                    if (field_value[0] == "CommProtocol" && field_value[1].ToLower() == "8bit") this.comboBoxTagging.SelectedIndex = 2;
                    if (field_value[0] == "CommProtocol" && field_value[1].ToLower() == "robik") this.comboBoxTagging.SelectedIndex = 3;

                    if (field_value[0] == "ComPortName") comboBoxComPort.Text = field_value[1].ToUpper();
                    if (field_value[0] == "ShuffleEnabled" && field_value[1].ToLower() == "true") checkBoxShuffledEnabled.Checked = true;

                    if (field_value[0] == "VrpnPeripheral") textBoxVrpnPeripheral.Text = field_value[1].ToLower();

                    if (field_value[0] == "LogToDisk" && field_value[1].ToLower() == "true") checkBoxLogDisc.Checked = true;
                    if (field_value[0] == "LogToConsole" && field_value[1].ToLower() == "true") checkBoxLogConsole.Checked = true;

                    if (field_value[0] == "Lives") numericUpDownLives.Value = Convert.ToInt32(field_value[1]);
                    if (field_value[0] == "MaxTrainingTargets") numericUpDownMaxTrainingTargets.Value = Convert.ToInt32(field_value[1]);

                    if (field_value[0] == "RepetitionsPerLife") this.numericUpDownRepetitionsPerLife.Value = Convert.ToInt32(field_value[1]);
                    if (field_value[0] == "MaxTrainingRepetitionsPerTarget") this.numericUpDownMaxTrainingRepetitionsPerTarget.Value = Convert.ToInt32(field_value[1]);

                    if (field_value[0] == "FlashTargetTime") numericUpDownFlashTarget.Value = Convert.ToDecimal(field_value[1]);
                    if (field_value[0] == "FlashNonTargetTime") numericUpDownFlashNonTarget.Value = Convert.ToDecimal(field_value[1]);
                    if (field_value[0] == "MinISI") numericUpDownMinISI.Value = Convert.ToDecimal(field_value[1]);
                    if (field_value[0] == "MaxISI") numericUpDownMaxISI.Value = Convert.ToDecimal(field_value[1]);
                    if (field_value[0] == "MeanISI") numericUpDownMeanISI.Value = Convert.ToDecimal(field_value[1]);

                    if (field_value[0] == "PauseAfterDestruction")
                        numericUpDownPauseAfterDestruction.Value = Convert.ToDecimal(field_value[1]);
                    if (field_value[0] == "PauseBetweenLevels") numericUpDownPauseBetweenLevels.Value = Convert.ToDecimal(field_value[1]);
                    if (field_value[0] == "PauseBetweenRowsAndColumns")
                        numericUpDownPauseBetweenRowsAndColumns.Value = Convert.ToDecimal(field_value[1]);

                    if (field_value[0] == "VRPNTargetReceivedTimeout")
                        numericUpDownVRPNTargetReceivedTimeout.Value = Convert.ToDecimal(field_value[1]);

                    if (field_value[0] == "OGREDebugLoggingEnabled" && field_value[1].ToLower() == "true") checkBoxOgreDebug.Checked = true;

                    if (field_value[0] == "ISIAdaptationEnabled" && field_value[1].ToLower() == "true") checkBoxISIAdaptationEnabled.Checked = true;
                    if (field_value[0] == "PercentISIAdapt") numericUpDownISIAdaptation.Value = Convert.ToDecimal(field_value[1]);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Parsing error at line " + i + "\\" + lines + "\\ has failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = oldCulture;
            }
        }

        /// <summary>
        /// Generate a brain-invaders.conf
        /// </summary>
        /// <returns></returns>
        private string GenerateFile()
        {
            System.Globalization.CultureInfo oldCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            string file="";

            file += "CommProtocol: ";
            switch (comboBoxTagging.SelectedIndex)
            {
                case 0: file += "software" + Environment.NewLine; break;
                case 1: file += "serial" + Environment.NewLine; break;
                case 2: file += "8bit" + Environment.NewLine; break;
                case 3: file += "robik" + Environment.NewLine; break;
            }

            file += "ComPortName: " + comboBoxComPort.Text + Environment.NewLine;

            file += "ShuffleEnabled: " + checkBoxShuffledEnabled.Checked.ToString().ToLower() + Environment.NewLine;
            file += "VrpnPeripheral: " + textBoxVrpnPeripheral.Text + Environment.NewLine;
            file += "ShowOgreSetup: " + checkBoxVideoSetup.Checked.ToString().ToLower() + Environment.NewLine;
            file += "LogToDisk: " + checkBoxLogDisc.Checked.ToString().ToLower() + Environment.NewLine;
            file += "LogToConsole: " + checkBoxLogConsole.Checked.ToString().ToLower() + Environment.NewLine;
            file += Environment.NewLine;
            file += "Lives: " + numericUpDownLives.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "MaxTrainingTargets: " + numericUpDownMaxTrainingTargets.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += Environment.NewLine;
            file += "FlashTargetTime: " + numericUpDownFlashTarget.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "FlashNonTargetTime: " + numericUpDownFlashNonTarget.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "MinISI: " + numericUpDownMinISI.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "MaxISI: " + numericUpDownMaxISI.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "MeanISI: " + numericUpDownMeanISI.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += Environment.NewLine;
            file += "PauseAfterDestruction: " + numericUpDownPauseAfterDestruction.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "PauseBetweenLevels: " + numericUpDownPauseBetweenLevels.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "PauseBetweenRowsAndColumns: " + numericUpDownPauseBetweenRowsAndColumns.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "VRPNTargetReceivedTimeout: " + numericUpDownVRPNTargetReceivedTimeout.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "OGREDebugLoggingEnabled: " + checkBoxOgreDebug.Checked.ToString().ToLower() + Environment.NewLine;

            file += "ISIAdaptationEnabled: " + checkBoxISIAdaptationEnabled.Checked.ToString().ToLower() + Environment.NewLine;
            file += "PercentISIAdapt: " + numericUpDownISIAdaptation.Value.ToString().Replace(",", ".") + Environment.NewLine;

            file += "RepetitionsPerLife: " + this.numericUpDownRepetitionsPerLife.Value.ToString().Replace(",", ".") + Environment.NewLine;
            file += "MaxTrainingRepetitionsPerTarget: " + this.numericUpDownMaxTrainingRepetitionsPerTarget.Value.ToString().Replace(",", ".") + Environment.NewLine;

            file += Environment.NewLine;

            System.Threading.Thread.CurrentThread.CurrentCulture = oldCulture;

            return file;
        }

        private bool CheckInputData()
        {
            return true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // Use this since we are a WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Use this since we are a console app
                System.Environment.Exit(1);
            }
        }

        /// <summary>
        /// Load a brain-invaders.conf
        /// </summary>
        /// <param name="filename"></param>
        private void ReadFile(string filename)
        {
            if (File.Exists(filename))
            {
                StreamReader rdr=null;
                try
                {
                    rdr = new StreamReader(filename);
                    string str = rdr.ReadToEnd();

                    ClearForm();
                    bool parseOK = ParseFile(str);

                    if (parseOK)
                    {
                        toolStripStatusLabel1.Text = "Config file loaded successfully.";
                        textBoxFilePath.Text = filename;
                        this.configFilePath = filename;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not read brain-invaders.conf file! Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (rdr!=null)
                       rdr.Close();
                }
            }
            else
            {
                MessageBox.Show("brain-invaders.conf does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void openConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            //openFileDialog1.Filter = "conf files (*.conf)|All files (*.*)|*.*";
            //openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ReadFile(openFileDialog1.FileName);   
            }
        }

        private void ClearForm()
        {
            this.comboBoxTagging.Text = "";

            comboBoxComPort.Text = "";
            checkBoxShuffledEnabled.Checked = false;

            textBoxVrpnPeripheral.Text = "";

            checkBoxLogDisc.Checked = false;
            checkBoxLogConsole.Checked = false;
            checkBoxVideoSetup.Checked = false;

            numericUpDownLives.Value = 0;
            numericUpDownMaxTrainingTargets.Value = 0;

            numericUpDownFlashTarget.Value = 0;
            numericUpDownFlashNonTarget.Value = 0;
            numericUpDownMinISI.Value = 0;
            numericUpDownMaxISI.Value = 0;
            numericUpDownMeanISI.Value = 0;

            numericUpDownPauseAfterDestruction.Value = 0;
            numericUpDownPauseBetweenLevels.Value = 0;
            numericUpDownPauseBetweenRowsAndColumns.Value = 0;
            checkBoxOgreDebug.Checked = false;

            numericUpDownISIAdaptation.Value = 0;
            checkBoxISIAdaptationEnabled.Checked = false;

            numericUpDownRepetitionsPerLife.Value = 0;
            numericUpDownMaxTrainingRepetitionsPerTarget.Value = 0;
        }

        /// <summary>
        /// Wrtite to disk brain-invaders.conf
        /// </summary>
        /// <param name="filename"></param>
        private void WriteFile(string filename)
        {
            try
            {
                string content = GenerateFile();

                File.WriteAllText(filename, content);

                //if (!checkBoxVideoSetup.Checked)
                //    SaveOgreFile();

                toolStripStatusLabel1.Text = "Config file saved successfully.";
            }
            catch (Exception ex)
            {
                toolStripStatusLabel1.Text = "Failed to save file!";
                MessageBox.Show("Error: could not save your new configuration! Old one will be used. Most likely you do not have administrative rights to modify brain-invaders.conf.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void saveConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.configFilePath=="")
               saveConfigAsToolStripMenuItem_Click(null,null);
            else
               WriteFile(this.configFilePath);            
        }

        /// <summary>
        /// Button located on the first tab and used only to start BI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveStart_Click(object sender, EventArgs e)
        {
            try
            {
                bool ok = VerifyExperimentConfiguration("", false, true);
                listBoxLog.Items.Insert(0, "[ " + DateTime.Now.ToLongTimeString() + " ] " + "Configuration verification " + ((ok) ? "successful." : "NOT successful!"));
                if (!ok) return;

                StartBrainInvaders();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Executing Brain Invaders has failed! Error: "+ ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void StartBrainInvaders()
        {
            WriteFile(this.configFilePath);

            FileInfo f = new FileInfo(this.configFilePath);
            DirectoryInfo dir = Directory.GetParent(f.Directory.ToString());

            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = dir.FullName + "\\" + brainInvadersStartScript;

            #region get and set env variables
            //env varaibles are stored already in the OV controller
            if (OpenVibeController.EnvironmentVariables != null)
            {
                foreach (string key in OpenVibeController.EnvironmentVariables.Keys)
                    psi.EnvironmentVariables.Add(key, OpenVibeController.EnvironmentVariables[key]);
            }
            psi.UseShellExecute = false;//to use EnvironmentVariables
            #endregion

            psi.WorkingDirectory = dir.FullName;

            System.Diagnostics.Process.Start(psi);

            checkBoxVideoSetup.Checked = false; //so that video configuration is started only once
        }

        private void onlineDocumentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenLinkInBrowser("https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/BrainInvadersConfiguration");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm af = new AboutForm();
            af.Show();
        }

        private void saveConfigAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            //saveFileDialog1.Filter = "Your extension here (*.EXT)|*.ext|All Files (*.*)|*.*";
            //saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.FileName = "brain-invaders.conf";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                WriteFile(saveFileDialog1.FileName);
                this.configFilePath = saveFileDialog1.FileName;
                this.textBoxFilePath.Text = saveFileDialog1.FileName;
            } 
        }

        private void ReadOgreFile()
        {
            FileInfo f = new FileInfo(this.configFilePath);
            string filename = f.Directory + "\\openvibe-applications\\brain-invaders\\brain-invaders\\ogre.cfg";

            groupBoxScreen.Enabled = false;

            if (File.Exists(filename))
            {
                StreamReader rdr = null;
                try
                {
                    rdr = new StreamReader(filename);
                    string str = rdr.ReadToEnd();

                    string[] lines = str.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                        //Regex.Split(str, "(\n)+");
 
                    foreach (string line in lines)
                    {
                        if (string.IsNullOrEmpty(line) || line[0] == '#' || line[0] == '\r' || line[0] == '\n') continue;

                        string[] field_value = Regex.Split(line, "=");
                        if (field_value.Length < 2) continue;
                        field_value[0] = field_value[0].Trim();
                        field_value[1] = field_value[1].Trim();
                    }

                    groupBoxScreen.Enabled = true;   
                }
                catch (Exception ex)
                {
                   
                }
                finally
                {
                    if (rdr != null)
                        rdr.Close();
                }
            }
        }

        private void buttonDetectSerialToUsb_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Checking COM ports ...";

            //string[] usbToSerialDevices =  ComPortHelper.GetUSBCOMDevices();
            string usbToSerialDevice = ComPortHelper.AutodetectArduinoPort();

            string[] allAvailablePortNames = System.IO.Ports.SerialPort.GetPortNames();

            if (usbToSerialDevice!=null)
            {
                comboBoxComPort.SelectedItem = usbToSerialDevice;
                toolStripStatusLabel1.Text = "Serial to USB " + usbToSerialDevice + " port was detected and selected.";
            }
            else
            if (allAvailablePortNames.Length > 0)
            {
                comboBoxComPort.SelectedItem = allAvailablePortNames[0];
                toolStripStatusLabel1.Text = allAvailablePortNames[0] + " port has been detected and selected. Serial to USB COM ports not detected!";
            }
            else
            {
                MessageBox.Show("No COM ports detected!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                toolStripStatusLabel1.Text = "Checking COM ports completed!";
            }
        }

        /// <summary>
        /// This code is used by several buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            OpenVibeController.EnvironmentVariables = new System.Collections.Specialized.StringDictionary();

            #region log on screen
            listBoxLog.Items.Insert(0, "[ " + DateTime.Now.ToLongTimeString() + " ] " + "Button \""+button.Text+"\" clicked!");
            #endregion

            //set scenario based on provided 2 scenarios and button clicked Online or Training
            string scenario = "";
            
            if (button.Text == button1.Text || button.Text == button3.Text)
            {
                scenario = this.textBoxTrainScenario.Text;
            }
            else
            {
                scenario = this.textBoxOnlineScerario.Text;
            }

            #region set savepath

            //usage in OpenVibe: $env{savepath}/test.ov
            string savepath = GetOpenVibeDataOutputFolder();
            OpenVibeController.EnvironmentVariables.Add(textBox3.Text, savepath.Replace("\\","/"));
            listBoxLog.Items.Insert(0, "[ " + DateTime.Now.ToLongTimeString() + " ] " + "Saving to path: " + savepath);
            textBox4.Text = savepath;

            #endregion

            //perfrom checks
            bool ok = VerifyExperimentConfiguration(savepath, checkBoxStartOV.Checked, checkBoxStartBI.Checked);
            listBoxLog.Items.Insert(0, "[ " + DateTime.Now.ToLongTimeString() + " ] " + "Configuration verification " + ((ok) ? "successful." : "NOT successful!"));
            if (!ok) return;

            WriteFile(this.configFilePath);

            //set NREP variable to be used in OpenVibe - it sets after how many repetitions a result is returned to BI
            OpenVibeController.EnvironmentVariables.Add("NREP", this.numericUpDownRepetitionsPerLife.Value.ToString());

            //set is adaptive environment
            this.textBoxIsAdaptive.Text = IsAdaptive.ToString();
            OpenVibeController.EnvironmentVariables.Add(textBox6.Text, IsAdaptive.ToString());

            #region execute
            //start OV
            if (checkBoxStartOV.Checked)
            {
                OpenVibeController.Scenario = scenario;
                OpenVibeController.OpenVibeDesignerWorkingFolder = textBoxOpenVibeFolder.Text;
                OpenVibeController.Start(true);
            }

            //start BI
            if (checkBoxStartBI.Checked)
            {
                StartBrainInvaders();
            }
            #endregion
        }

        private string GetOpenVibeDataOutputFolder()
        {
            string savepath = "";
            string subjectFolder = textBoxOutputFolder.Text;
            subjectFolder = (subjectFolder.EndsWith("\\")) ? subjectFolder.Substring(0, subjectFolder.Length - 1) : subjectFolder;
            savepath = subjectFolder + "\\" + textBoxUserID.Text + "\\Session" + textBoxSessionNumber.Text;//+"\\Phase"+phase.ToString();

            return savepath;
        }

        private bool VerifyExperimentConfiguration(string savepath,bool executeOV, bool executeBI)
        {
            #region verify OpenVibe source folder
            if (!Directory.Exists(this.textBoxOpenVibeFolder.Text))
            {
                MessageBox.Show("OpenVibe folder does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            #endregion

            //check if all folders exists: OV, scenarios, UserID and Session are correct
            if (executeBI == true)
            {
                if (!System.IO.File.Exists(this.configFilePath))
                {
                    MessageBox.Show("No config file detected!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                #region check executable

                FileInfo f = new FileInfo(this.configFilePath);
                DirectoryInfo dir = Directory.GetParent(f.Directory.ToString());

                string FileName = dir.FullName + "\\" + brainInvadersStartScript;

                if (!(File.Exists(FileName)))
                {
                    MessageBox.Show("Brain Invaders executable could not be detected: " + FileName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                #endregion
            }

            #region verify output folder
            if (executeOV && savepath != "" && textBoxOutputFolder.Text!="")
            {
                if (Directory.Exists(savepath))
                {
                    DialogResult dialogResult = MessageBox.Show("The output folder: " + savepath + " already exists. It is possible that you overwrite an existing record! Do you wish to continue? If not then change your output folder, subject id or session id.", "Warning", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }
                else
                {
                    try
                    {
                        Directory.CreateDirectory(savepath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Could not create output folder! Your user id, session id and output path must be valid DOS names!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
            }
            #endregion

            #region verify COM port
            if (executeBI && comboBoxTagging.SelectedItem.ToString().ToLower().Contains("serial"))
            {
                string usbToSerialDevice = ComPortHelper.AutodetectArduinoPort();

                if (usbToSerialDevice != null && usbToSerialDevice != comboBoxComPort.Text)
                {
                    var dialogResult = MessageBox.Show("The currently selected serial COM port might be wrong! Try auto-detect and verify. If you are using a COM to USB converter, then make sure that it is connected at this stage. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dialogResult == DialogResult.No)
                    {
                        return false;
                    }
                }
            }
            #endregion

            #region verify scenarios
            if (executeOV)
            {
                if (IsAdaptive == false && !File.Exists(textBoxTrainScenario.Text)) //adaptive does not need train
                {
                    MessageBox.Show("Train scenario file does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (!File.Exists(textBoxOnlineScerario.Text))
                {
                    MessageBox.Show("Online scenario file does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            #endregion

            #region Check AS configuration for GTEC

            if (executeOV)
            {
                string openVibeConfigFileName = "";
                string openVibeConfigFileName1 = OpenVibeController.DetectOpenVibeInstallFolder() + "\\${CustomConfigurationApplication}";
                if (File.Exists(openVibeConfigFileName1)) openVibeConfigFileName = openVibeConfigFileName1;
                string openVibeConfigFileName2 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\openvibe\\openvibe-acquisition-server.conf";
                if (File.Exists(openVibeConfigFileName2)) openVibeConfigFileName = openVibeConfigFileName2;
                string openVibeConfigFileName3 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Roaming\\openvibe\\openvibe-acquisition-server.conf";
                if (File.Exists(openVibeConfigFileName3)) openVibeConfigFileName = openVibeConfigFileName3;

                if (openVibeConfigFileName != "" && File.Exists(openVibeConfigFileName))
                {
                    string text = File.ReadAllText(openVibeConfigFileName);
                    string[] lines = text.Split('\n');

                    foreach (string s in lines)
                    {
                        //AcquisitionServer_Driver_GTecGUSBamp_Header = ExperimentID 0 SubjectAge 18 SubjectGender 9 SamplingFrequency 512 Channels 16 Names ;;;;;;;;;;;;;;;; Gains 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
                        if (s.StartsWith("AcquisitionServer_Driver_GTecGUSBamp_Header") && !s.Contains("SamplingFrequency 512"))
                        {
                            var dialogResult = MessageBox.Show("The sampling frequency configured for the GTEC amplifier seems not to be the default 512Hz. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (dialogResult == DialogResult.No)
                            {
                                return false;
                            }
                        }

                        //AcquisitionServer_Driver_GTecGUSBamp_TriggerInputEnabled = 0
                        if (comboBoxTagging.SelectedIndex == 1 && s.StartsWith("AcquisitionServer_Driver_GTecGUSBamp_TriggerInputEnabled") && s.Contains("= 0"))
                        {
                            var dialogResult = MessageBox.Show("You have selected hardware tagging, but the OpenVibe AS is not configured to accept Trigger input. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (dialogResult == DialogResult.No)
                            {
                                return false;
                            }
                        }

                    }
                }
            }
            #endregion

            #region check output folder
            if (!IsAdaptive)
            {
                bool requiresOutputFolderForTrain = File.ReadAllText(textBoxTrainScenario.Text).Contains("$env{savepath}");

                if ((!string.IsNullOrEmpty(textBoxOutputFolder.Text) && requiresOutputFolderForTrain)     //both configured for output
                    || (string.IsNullOrEmpty(textBoxOutputFolder.Text) && !requiresOutputFolderForTrain)) //both not configured
                {
                    //all OK
                }
                else
                {
                    var dialogResultTrain = MessageBox.Show("You have a potential problem with the data output folder for your recordings in Train scenario. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dialogResultTrain == DialogResult.No)
                    {
                        return false;
                    }
                }
            }

            bool requiresOutputFolderForOnline = File.ReadAllText(textBoxOnlineScerario.Text).Contains("$env{savepath}");
            if ((!string.IsNullOrEmpty(textBoxOutputFolder.Text) && requiresOutputFolderForOnline)      //both configured for output
                || (string.IsNullOrEmpty(textBoxOutputFolder.Text) && !requiresOutputFolderForOnline))  //both not configured
            {
                //all OK
            }
            else
            {
                var dialogResultTrain = MessageBox.Show("You have a potential problem with the data output folder for your recordings in Online scenario. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResultTrain == DialogResult.No)
                {
                    return false;
                }
            }
            #endregion

            //return true if all checks were OK
            return true;
        }

        private void buttonRandomize_Click(object sender, EventArgs e)
        {
            IsAdaptive = (rng.Next(2) == 1) ? true : false;

            toolStripStatusLabel1.Text = "Randomized at: " + DateTime.Now.ToLongTimeString();

            listBoxLog.Items.Insert(0, "[ "+DateTime.Now.ToLongTimeString()+" ] "+"Sequence randomized.");
        }

        private void buttonCopyLogClipbaord_Click(object sender, EventArgs e)
        {
            CopyListBoxToClipboard(this.listBoxLog);
        }

        public void CopyListBoxToClipboard(ListBox lb)
        {
            StringBuilder buffer = new StringBuilder();

            for (int i = 0; i < lb.Items.Count; i++)
            {
                buffer.Append(lb.Items[i].ToString());
                buffer.Append("\n");
            }

            Clipboard.SetText(buffer.ToString());
        }

        void comboBoxModality_SelectedIndexChanged(object sender, EventArgs e)
        {
            string intermediate = @"\share\openvibe\scenarios\gipsa-lab\MDM\P300";

            IsAdaptive = false;

            switch (comboBoxModality.SelectedIndex)
            {
                case 0:
                    comboBoxTagging.SelectedIndex = 1; //serial port
                    textBoxTrainScenario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-with-training\train-mdm.xml";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-with-training\online-mdm.xml";
                    break;

                case 1:
                    IsAdaptive = true;
                    comboBoxTagging.SelectedIndex = 1; //serial port
                    textBoxTrainScenario.Text = "";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-adaptive\online-adaptive-mdm.xml";
                    break;

                case 2:
                    IsAdaptive = true;
                    comboBoxTagging.SelectedIndex = 1; //serial port
                    textBoxTrainScenario.Text = "";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-adaptive-latency-correction\online-adaptive-mdm-latency.xml";
                    break;

                case 3:
                    comboBoxTagging.SelectedIndex = 0; //software tagging
                    textBoxTrainScenario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-training-software-tagging\train-mdm-st.xml";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-training-software-tagging\online-mdm-st.xml";
                    break;

                case 4: //Emotiv
                    comboBoxTagging.SelectedIndex = 0; //software tagging
                    textBoxTrainScenario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-training-software-tagging\train-mdm-st-emotiv.xml";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-training-software-tagging\online-mdm-st-emotiv.xml";
                    break;

                case 5: //Brain Invaders P300 using MDM Adaptive - 4 playes - hardware tagging
                    IsAdaptive = true;
                    comboBoxTagging.SelectedIndex = 1; //serial port
                    textBoxTrainScenario.Text = "";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-adaptive-4players\online-adaptive-mdm_4p.xml";
                    break;

                case 6: //Brain Invaders P300 using MDM Adaptive - 4 playes - hardware tagging
                    IsAdaptive = true;
                    comboBoxTagging.SelectedIndex = 1; //serial port
                    textBoxTrainScenario.Text = "";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-adaptive-2p32\online-adaptive-mdm_2p32.xml";
                    break;

                case 7: //Brain Invaders P300 using MDM Adaptive - Full Multiplayer - hardware tagging
                    IsAdaptive = true;
                    comboBoxTagging.SelectedIndex = 1; //serial port
                    textBoxTrainScenario.Text = "";
                    textBoxOnlineScerario.Text = textBoxOpenVibeFolder.Text + intermediate + @"\brain-invaders-mdm-adaptive-multi-player\online-mdm-adaptive-multiplayer.xml";
                    break;
            }

            if (IsAdaptive)
            {
                button1.Visible = false;
                button2.Visible = false;
                buttonStartAdaptive.Visible = true;
            }
            else
            {
                button1.Visible = true;
                button2.Visible = true;
                buttonStartAdaptive.Visible = false;
            }
        }

        private void buttonTestConfiguration_Click(object sender, EventArgs e)
        {
            string savepath = GetOpenVibeDataOutputFolder();
            bool ok = VerifyExperimentConfiguration(savepath, checkBoxStartOV.Checked, checkBoxStartBI.Checked);
            listBoxLog.Items.Insert(0, "[ " + DateTime.Now.ToLongTimeString() + " ] " + "Configuration verification " + ((ok) ? "successful." : "NOT successful!"));

            if (ok)
            {
                MessageBox.Show("No (other) issues were detected while validating Brain Invaders configuration!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonTestTagging_Click(object sender, EventArgs e)
        {
            if (buttonTestTagging.Text == "Cancel")
            {
                ctsTestTagger.Cancel();
                buttonTestTagging.Text = "Test loop same value";
            }
            else
            {
                //OpenVibeTaggingWrapper p = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.Software,"openvibeExternalStimulations");
                //OpenVibeTaggingWrapper p = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort,"888");

                if (this.comboBoxTaggingTestMethod.Text == "Serial Port")
                {
                    if (tagger == null || (lastComPortUsedForTagger != comboBoxComPort.Text)) //we are caching the tagger
                    {
                        tagger = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.SerialPort, comboBoxComPort.Text);
                        lastComPortUsedForTagger = comboBoxComPort.Text;
                    }
                }
                else if (this.comboBoxTaggingTestMethod.Text == "8 bit parallel Port")
                {
                    if (tagger == null || tagger.GetProtocol() != OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort)
                        tagger = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort, numericUpDownPPAddress.Value.ToString());//default "888"
                }
                else
                {
                    MessageBox.Show("Tagging method could not be selected or it is unavailable!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (tagger.GetInitErrorMessage() != "")
                {
                    MessageBox.Show(tagger.GetInitErrorMessage(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    //for (int i = 0; i < numericUpDownTestValueCount.Value; i++)
                    //{
                    //    tagger.TestWriteValue((int)numericUpDownTestValue.Value);//user specified value
                    //    System.Threading.Thread.Sleep((int)numericUpDownTestValueTimeKeep.Value);

                    //    tagger.TestWriteValue(0);
                    //    System.Threading.Thread.Sleep((int)numericUpDownTestPauseTime.Value);
                    //}

                    ctsTestTagger = new CancellationTokenSource();
                    ctTestTagger = ctsTestTagger.Token;

                    Task.Factory.StartNew(() =>
                    {
                        for (int i = 0; i < numericUpDownTestValueCount.Value; i++)
                        {
                            if (ctTestTagger.IsCancellationRequested)
                            {
                                // another thread decided to cancel
                                Console.WriteLine("task canceled");
                                break;
                            }

                            tagger.TestWriteValue((int)numericUpDownTestValue.Value);//user specified value
                            System.Threading.Thread.Sleep((int)numericUpDownTestValueTimeKeep.Value);

                            tagger.TestWriteValue(0);
                            System.Threading.Thread.Sleep((int)numericUpDownTestPauseTime.Value);

                            toolStripStatusLabel1.Text = "Number of outputs: " + (i+1).ToString();
                        }

                        buttonTestTagging.Text = "Test";
                        toolStripStatusLabel1.Text = "Ready!";
                       

                    }, ctTestTagger);

                    buttonTestTagging.Text = "Cancel";
                }
            }
        }

        private void buttonCompareP300_Click(object sender, EventArgs e)
        {
            try
            {
                int channelSelected = int.Parse(comboBoxP300Channel.Text) - 1;

                if (!File.Exists(textBoxP300Reference.Text))
                {
                    MessageBox.Show("Reference P300 file does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (!File.Exists(textBoxP300CurrentSubject.Text))
                {
                    MessageBox.Show("Current Subject P300 file does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.chartReference.Series.Clear();

                // Data arrays
                if (this.chartReference.Titles.Count == 0)
                {
                    this.chartReference.Titles.Add("Comparison of P300 average");
                }

                //=========================================
                Series series = this.chartReference.Series.Add("P300 average reference");

                series.ChartType = SeriesChartType.Spline;
                series.Color = Color.Blue;
                series.BorderWidth = 2;
                //==========================================
                Series series2 = this.chartReference.Series.Add("P300 average current subject");

                series2.ChartType = SeriesChartType.Spline;

                series2.BorderWidth = 2;
                series.Color = Color.Red;

                //Load first series
                var reader = new StreamReader(File.OpenRead(textBoxP300Reference.Text));

                int i = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    if (i == channelSelected)
                    {
                        var values = line.Split(',');

                        foreach (var v in values)
                            series.Points.AddY(v);
                    }

                    i++;

                }

                var reader2 = new StreamReader(File.OpenRead(textBoxP300CurrentSubject.Text));

                i = 0;

                while (!reader2.EndOfStream)
                {
                    string line2 = reader2.ReadLine();

                    if (line2.StartsWith("P1"))
                        break;
                }

                while (!reader2.EndOfStream)
                {
                    var line = reader2.ReadLine();

                    

                    if (i == channelSelected)
                    {
                        line = line.Replace("[", "").Replace("]", "");
                        line = line.Substring(1, line.Length - 1);

                        var values = line.Split(' ');

                        foreach (var v in values)
                            series2.Points.AddY(v);
                    }


                    i++;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void buttonBrowseP300Reference_Click(object sender, EventArgs e)
        {
            var FD = new System.Windows.Forms.OpenFileDialog();

            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                 textBoxP300Reference.Text = FD.FileName;
            }
        }

        private void buttonBrowseP300CurrentSubject_Click(object sender, EventArgs e)
        {
            var FD = new System.Windows.Forms.OpenFileDialog();
            if (FD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxP300CurrentSubject.Text = FD.FileName;              
            }
        }

        private void buttonSendOne_Click(object sender, EventArgs e)
        {
            if (this.comboBoxTaggingTestMethod.Text == "Serial Port")
            {
                if (tagger == null || (lastComPortUsedForTagger != comboBoxComPort.Text)) //we are caching the tagger
                {
                    tagger = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.SerialPort, comboBoxComPort.Text);
                    lastComPortUsedForTagger = comboBoxComPort.Text;
                }
            }
            else if (this.comboBoxTaggingTestMethod.Text == "8 bit parallel Port")
            {
                if (tagger == null || tagger.GetProtocol() != OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort)
                    tagger = new OpenVibeTaggingWrapper(OpenVibeTaggingWrapper.eProtocol.EightBitParallelPort, numericUpDownPPAddress.Value.ToString());//default "888"
            }
            else
            {
                MessageBox.Show("Tagging method could not be selected or it is unavailable!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tagger.GetInitErrorMessage() != "")
            {
                var result = MessageBox.Show(tagger.GetInitErrorMessage(), "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                tagger.TestWriteValue((int)numericUpDownTestValue.Value);

                toolStripStatusLabel1.Text = "Value " + numericUpDownTestValue.Value + " has been sent!";
            }
        }

        private void buttonSendValueManaged_Click(object sender, EventArgs e)
        {
            byte[] unsigned = { Convert.ToByte(numericUpDownManagedValue.Value) };

            SerialPort serialPort = new SerialPort(comboBoxComPort.Text, 115200, Parity.None, 8, StopBits.One);

            try
            {
                serialPort.Open();

                serialPort.Write(unsigned, 0, unsigned.Length);

                serialPort.Close();

                toolStripStatusLabel1.Text = "Value " + numericUpDownManagedValue.Value + " has been sent successfully.";
            }
            catch (Exception ex)
            {
                string message = "Error: " + ex.Message;
                if (tagger!=null)
                    message += " Restart Launcher and use directly this tagging method. Do not use the above one before this one.";

                MessageBox.Show( message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonAutoDetectPP_Click(object sender, EventArgs e)
        {
            ushort address = Helper.FindPPAddress();

            if (address != 0)
                numericUpDownPPAddress.Value = address;
            else
            {
                MessageBox.Show("Could not detect parallel port address automatically!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
