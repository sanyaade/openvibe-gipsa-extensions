﻿namespace brain_invaders_launcher
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveConfigAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineDocumentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTipShuffling = new System.Windows.Forms.ToolTip(this.components);
            this.buttonDetectSerialToUsb = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.checkBoxLogConsole = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxShuffledEnabled = new System.Windows.Forms.CheckBox();
            this.comboBoxComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTagging = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxScreen = new System.Windows.Forms.GroupBox();
            this.checkBoxVideoSetup = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.numericUpDownVRPNTargetReceivedTimeout = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.numericUpDownPauseBetweenLevels = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.numericUpDownPauseAfterDestruction = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.numericUpDownPauseBetweenRowsAndColumns = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDownMaxTrainingRepetitionsPerTarget = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRepetitionsPerLife = new System.Windows.Forms.NumericUpDown();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.numericUpDownLives = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMaxTrainingTargets = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.numericUpDownISIAdaptation = new System.Windows.Forms.NumericUpDown();
            this.label39 = new System.Windows.Forms.Label();
            this.checkBoxISIAdaptationEnabled = new System.Windows.Forms.CheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDownMeanISI = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDownMaxISI = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDownMinISI = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownFlashNonTarget = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownFlashTarget = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.checkBoxOgreDebug = new System.Windows.Forms.CheckBox();
            this.textBoxLogDisc = new System.Windows.Forms.TextBox();
            this.checkBoxLogDisc = new System.Windows.Forms.CheckBox();
            this.textBoxVrpnPeripheral = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSaveStart = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboBoxModality = new System.Windows.Forms.ComboBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBoxIsAdaptive = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textBoxNrep = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonTestConfiguration = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.buttonStartAdaptive = new System.Windows.Forms.Button();
            this.buttonCopyLogClipbaord = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.buttonRandomize = new System.Windows.Forms.Button();
            this.checkBoxStartBI = new System.Windows.Forms.CheckBox();
            this.checkBoxStartOV = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.textBoxOutputFolder = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxOnlineScerario = new System.Windows.Forms.TextBox();
            this.textBoxTrainScenario = new System.Windows.Forms.TextBox();
            this.textBoxOpenVibeFolder = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxUserID = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxSessionNumber = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.comboBoxP300Channel = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.buttonBrowseP300CurrentSubject = new System.Windows.Forms.Button();
            this.buttonBrowseP300Reference = new System.Windows.Forms.Button();
            this.buttonCompareP300 = new System.Windows.Forms.Button();
            this.textBoxP300CurrentSubject = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBoxP300Reference = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.chartReference = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.buttonAutoDetectPP = new System.Windows.Forms.Button();
            this.numericUpDownPPAddress = new System.Windows.Forms.NumericUpDown();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.numericUpDownManagedValue = new System.Windows.Forms.NumericUpDown();
            this.label54 = new System.Windows.Forms.Label();
            this.buttonSendValueManaged = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.buttonSendOne = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.buttonTestTagging = new System.Windows.Forms.Button();
            this.numericUpDownTestValueTimeKeep = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTestPauseTime = new System.Windows.Forms.NumericUpDown();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.numericUpDownTestValue = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.numericUpDownTestValueCount = new System.Windows.Forms.NumericUpDown();
            this.comboBoxTaggingTestMethod = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new brain_invaders_launcher.OvalPictureBox();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBoxScreen.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVRPNTargetReceivedTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPauseBetweenLevels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPauseAfterDestruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPauseBetweenRowsAndColumns)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxTrainingRepetitionsPerTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepetitionsPerLife)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLives)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxTrainingTargets)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownISIAdaptation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMeanISI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxISI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinISI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlashNonTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlashTarget)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartReference)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPAddress)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownManagedValue)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestValueTimeKeep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestPauseTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestValueCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(821, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openConfigToolStripMenuItem,
            this.saveConfigToolStripMenuItem,
            this.saveConfigAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openConfigToolStripMenuItem
            // 
            this.openConfigToolStripMenuItem.Name = "openConfigToolStripMenuItem";
            this.openConfigToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.openConfigToolStripMenuItem.Text = "Open config";
            this.openConfigToolStripMenuItem.Click += new System.EventHandler(this.openConfigToolStripMenuItem_Click);
            // 
            // saveConfigToolStripMenuItem
            // 
            this.saveConfigToolStripMenuItem.Name = "saveConfigToolStripMenuItem";
            this.saveConfigToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.saveConfigToolStripMenuItem.Text = "Save config";
            this.saveConfigToolStripMenuItem.Click += new System.EventHandler(this.saveConfigToolStripMenuItem_Click);
            // 
            // saveConfigAsToolStripMenuItem
            // 
            this.saveConfigAsToolStripMenuItem.Name = "saveConfigAsToolStripMenuItem";
            this.saveConfigAsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.saveConfigAsToolStripMenuItem.Text = "Save config as";
            this.saveConfigAsToolStripMenuItem.Click += new System.EventHandler(this.saveConfigAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlineDocumentationToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // onlineDocumentationToolStripMenuItem
            // 
            this.onlineDocumentationToolStripMenuItem.Name = "onlineDocumentationToolStripMenuItem";
            this.onlineDocumentationToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.onlineDocumentationToolStripMenuItem.Text = "Online Documentation";
            this.onlineDocumentationToolStripMenuItem.Click += new System.EventHandler(this.onlineDocumentationToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 599);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(821, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Ready!";
            // 
            // toolTipShuffling
            // 
            this.toolTipShuffling.IsBalloon = true;
            this.toolTipShuffling.ShowAlways = true;
            // 
            // buttonDetectSerialToUsb
            // 
            this.buttonDetectSerialToUsb.Location = new System.Drawing.Point(237, 52);
            this.buttonDetectSerialToUsb.Name = "buttonDetectSerialToUsb";
            this.buttonDetectSerialToUsb.Size = new System.Drawing.Size(94, 32);
            this.buttonDetectSerialToUsb.TabIndex = 13;
            this.buttonDetectSerialToUsb.Text = "Try auto-detect";
            this.toolTipShuffling.SetToolTip(this.buttonDetectSerialToUsb, "It will search for Serial to USB ports and select the first one. If none are avai" +
        "lable then it will select the first available com port in the system.");
            this.buttonDetectSerialToUsb.UseVisualStyleBackColor = true;
            this.buttonDetectSerialToUsb.Click += new System.EventHandler(this.buttonDetectSerialToUsb_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Log to console:";
            this.toolTipShuffling.SetToolTip(this.label13, "Logs events to the DOS console.");
            // 
            // checkBoxLogConsole
            // 
            this.checkBoxLogConsole.AutoSize = true;
            this.checkBoxLogConsole.Location = new System.Drawing.Point(112, 188);
            this.checkBoxLogConsole.Name = "checkBoxLogConsole";
            this.checkBoxLogConsole.Size = new System.Drawing.Size(15, 14);
            this.checkBoxLogConsole.TabIndex = 11;
            this.toolTipShuffling.SetToolTip(this.checkBoxLogConsole, "Should be disabled as it might affect performance.");
            this.checkBoxLogConsole.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 153);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Log to disk:";
            this.toolTipShuffling.SetToolTip(this.label12, "Logs events to a log file.");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Shuffling enabled:";
            this.toolTipShuffling.SetToolTip(this.label3, "Should be enabled for optimal performance.");
            // 
            // checkBoxShuffledEnabled
            // 
            this.checkBoxShuffledEnabled.AutoSize = true;
            this.checkBoxShuffledEnabled.Location = new System.Drawing.Point(112, 100);
            this.checkBoxShuffledEnabled.Name = "checkBoxShuffledEnabled";
            this.checkBoxShuffledEnabled.Size = new System.Drawing.Size(15, 14);
            this.checkBoxShuffledEnabled.TabIndex = 0;
            this.toolTipShuffling.SetToolTip(this.checkBoxShuffledEnabled, "Should be enabled for optimal performance.");
            this.checkBoxShuffledEnabled.UseVisualStyleBackColor = true;
            // 
            // comboBoxComPort
            // 
            this.comboBoxComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComPort.FormattingEnabled = true;
            this.comboBoxComPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8"});
            this.comboBoxComPort.Location = new System.Drawing.Point(112, 52);
            this.comboBoxComPort.Name = "comboBoxComPort";
            this.comboBoxComPort.Size = new System.Drawing.Size(119, 21);
            this.comboBoxComPort.TabIndex = 5;
            this.toolTipShuffling.SetToolTip(this.comboBoxComPort, "Select the COM port that is connected to your Serial to USB converter (Arduino) o" +
        "r your amplifier.");
            // 
            // comboBoxTagging
            // 
            this.comboBoxTagging.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTagging.FormattingEnabled = true;
            this.comboBoxTagging.Items.AddRange(new object[] {
            "Software Tagging",
            "Serial Port",
            "Parallel Port",
            "Robik "});
            this.comboBoxTagging.Location = new System.Drawing.Point(112, 13);
            this.comboBoxTagging.Name = "comboBoxTagging";
            this.comboBoxTagging.Size = new System.Drawing.Size(119, 21);
            this.comboBoxTagging.TabIndex = 2;
            this.toolTipShuffling.SetToolTip(this.comboBoxTagging, "Click Help->Online Documentation for more information.");
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(2, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(817, 569);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBoxScreen);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.textBoxFilePath);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.buttonSaveStart);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(809, 543);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Brain Invaders";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBoxScreen
            // 
            this.groupBoxScreen.Controls.Add(this.checkBoxVideoSetup);
            this.groupBoxScreen.Controls.Add(this.label28);
            this.groupBoxScreen.Location = new System.Drawing.Point(5, 402);
            this.groupBoxScreen.Name = "groupBoxScreen";
            this.groupBoxScreen.Size = new System.Drawing.Size(337, 39);
            this.groupBoxScreen.TabIndex = 29;
            this.groupBoxScreen.TabStop = false;
            this.groupBoxScreen.Text = "Video";
            // 
            // checkBoxVideoSetup
            // 
            this.checkBoxVideoSetup.AutoSize = true;
            this.checkBoxVideoSetup.Location = new System.Drawing.Point(138, 18);
            this.checkBoxVideoSetup.Name = "checkBoxVideoSetup";
            this.checkBoxVideoSetup.Size = new System.Drawing.Size(15, 14);
            this.checkBoxVideoSetup.TabIndex = 5;
            this.checkBoxVideoSetup.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Start Video setup:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.numericUpDownVRPNTargetReceivedTimeout);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.numericUpDownPauseBetweenLevels);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.numericUpDownPauseAfterDestruction);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.numericUpDownPauseBetweenRowsAndColumns);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Location = new System.Drawing.Point(348, 282);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(455, 159);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pauses";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(364, 123);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(64, 13);
            this.label30.TabIndex = 40;
            this.label30.Text = "(in seconds)";
            // 
            // numericUpDownVRPNTargetReceivedTimeout
            // 
            this.numericUpDownVRPNTargetReceivedTimeout.DecimalPlaces = 3;
            this.numericUpDownVRPNTargetReceivedTimeout.Location = new System.Drawing.Point(187, 121);
            this.numericUpDownVRPNTargetReceivedTimeout.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownVRPNTargetReceivedTimeout.Name = "numericUpDownVRPNTargetReceivedTimeout";
            this.numericUpDownVRPNTargetReceivedTimeout.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownVRPNTargetReceivedTimeout.TabIndex = 39;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(9, 123);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(151, 13);
            this.label29.TabIndex = 38;
            this.label29.Text = "VRPN target received timeout:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(364, 88);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "(in milliseconds)";
            // 
            // numericUpDownPauseBetweenLevels
            // 
            this.numericUpDownPauseBetweenLevels.Location = new System.Drawing.Point(187, 89);
            this.numericUpDownPauseBetweenLevels.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownPauseBetweenLevels.Name = "numericUpDownPauseBetweenLevels";
            this.numericUpDownPauseBetweenLevels.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownPauseBetweenLevels.TabIndex = 36;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(114, 13);
            this.label21.TabIndex = 35;
            this.label21.Text = "Pause between levels:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(364, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 34;
            this.label22.Text = "(in seconds)";
            // 
            // numericUpDownPauseAfterDestruction
            // 
            this.numericUpDownPauseAfterDestruction.DecimalPlaces = 3;
            this.numericUpDownPauseAfterDestruction.Location = new System.Drawing.Point(187, 55);
            this.numericUpDownPauseAfterDestruction.Name = "numericUpDownPauseAfterDestruction";
            this.numericUpDownPauseAfterDestruction.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownPauseAfterDestruction.TabIndex = 33;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 26);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(172, 13);
            this.label23.TabIndex = 29;
            this.label23.Text = "Pause between rows and columns:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(364, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 32;
            this.label24.Text = "(in seconds)";
            // 
            // numericUpDownPauseBetweenRowsAndColumns
            // 
            this.numericUpDownPauseBetweenRowsAndColumns.DecimalPlaces = 3;
            this.numericUpDownPauseBetweenRowsAndColumns.Location = new System.Drawing.Point(187, 24);
            this.numericUpDownPauseBetweenRowsAndColumns.Name = "numericUpDownPauseBetweenRowsAndColumns";
            this.numericUpDownPauseBetweenRowsAndColumns.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownPauseBetweenRowsAndColumns.TabIndex = 30;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(119, 13);
            this.label25.TabIndex = 31;
            this.label25.Text = "Pause after destruction:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownMaxTrainingRepetitionsPerTarget);
            this.groupBox3.Controls.Add(this.numericUpDownRepetitionsPerLife);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.numericUpDownLives);
            this.groupBox3.Controls.Add(this.numericUpDownMaxTrainingTargets);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(5, 264);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(337, 132);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Repetions";
            // 
            // numericUpDownMaxTrainingRepetitionsPerTarget
            // 
            this.numericUpDownMaxTrainingRepetitionsPerTarget.Location = new System.Drawing.Point(138, 98);
            this.numericUpDownMaxTrainingRepetitionsPerTarget.Name = "numericUpDownMaxTrainingRepetitionsPerTarget";
            this.numericUpDownMaxTrainingRepetitionsPerTarget.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownMaxTrainingRepetitionsPerTarget.TabIndex = 21;
            // 
            // numericUpDownRepetitionsPerLife
            // 
            this.numericUpDownRepetitionsPerLife.Location = new System.Drawing.Point(138, 47);
            this.numericUpDownRepetitionsPerLife.Name = "numericUpDownRepetitionsPerLife";
            this.numericUpDownRepetitionsPerLife.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownRepetitionsPerLife.TabIndex = 20;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(9, 98);
            this.label42.MaximumSize = new System.Drawing.Size(120, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(118, 26);
            this.label42.TabIndex = 19;
            this.label42.Text = "Max training repetitions per target:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(9, 45);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 13);
            this.label41.TabIndex = 18;
            this.label41.Text = "Repetitions  per life:";
            // 
            // numericUpDownLives
            // 
            this.numericUpDownLives.Location = new System.Drawing.Point(138, 18);
            this.numericUpDownLives.Name = "numericUpDownLives";
            this.numericUpDownLives.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownLives.TabIndex = 17;
            // 
            // numericUpDownMaxTrainingTargets
            // 
            this.numericUpDownMaxTrainingTargets.Location = new System.Drawing.Point(138, 72);
            this.numericUpDownMaxTrainingTargets.Name = "numericUpDownMaxTrainingTargets";
            this.numericUpDownMaxTrainingTargets.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownMaxTrainingTargets.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Max training targets:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Lives:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.numericUpDownISIAdaptation);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.checkBoxISIAdaptationEnabled);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.numericUpDownMeanISI);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.numericUpDownMaxISI);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.numericUpDownMinISI);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.numericUpDownFlashNonTarget);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.numericUpDownFlashTarget);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(348, 44);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(455, 232);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Flashes";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(315, 201);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(21, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "(%)";
            // 
            // numericUpDownISIAdaptation
            // 
            this.numericUpDownISIAdaptation.Location = new System.Drawing.Point(138, 199);
            this.numericUpDownISIAdaptation.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownISIAdaptation.Name = "numericUpDownISIAdaptation";
            this.numericUpDownISIAdaptation.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownISIAdaptation.TabIndex = 32;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 201);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 13);
            this.label39.TabIndex = 31;
            this.label39.Text = "ISI Adapatation:";
            // 
            // checkBoxISIAdaptationEnabled
            // 
            this.checkBoxISIAdaptationEnabled.AutoSize = true;
            this.checkBoxISIAdaptationEnabled.Location = new System.Drawing.Point(138, 175);
            this.checkBoxISIAdaptationEnabled.Name = "checkBoxISIAdaptationEnabled";
            this.checkBoxISIAdaptationEnabled.Size = new System.Drawing.Size(15, 14);
            this.checkBoxISIAdaptationEnabled.TabIndex = 30;
            this.checkBoxISIAdaptationEnabled.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 175);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(124, 13);
            this.label38.TabIndex = 29;
            this.label38.Text = "ISI Adapatation enabled:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(315, 145);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "(in seconds)";
            // 
            // numericUpDownMeanISI
            // 
            this.numericUpDownMeanISI.DecimalPlaces = 3;
            this.numericUpDownMeanISI.Location = new System.Drawing.Point(138, 146);
            this.numericUpDownMeanISI.Name = "numericUpDownMeanISI";
            this.numericUpDownMeanISI.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownMeanISI.TabIndex = 27;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Mean ISI:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(315, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "(in seconds)";
            // 
            // numericUpDownMaxISI
            // 
            this.numericUpDownMaxISI.DecimalPlaces = 3;
            this.numericUpDownMaxISI.Location = new System.Drawing.Point(138, 112);
            this.numericUpDownMaxISI.Name = "numericUpDownMaxISI";
            this.numericUpDownMaxISI.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownMaxISI.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 84);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Min ISI:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(315, 79);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "(in seconds)";
            // 
            // numericUpDownMinISI
            // 
            this.numericUpDownMinISI.DecimalPlaces = 3;
            this.numericUpDownMinISI.Location = new System.Drawing.Point(138, 81);
            this.numericUpDownMinISI.Name = "numericUpDownMinISI";
            this.numericUpDownMinISI.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownMinISI.TabIndex = 21;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 114);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Max ISI:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(315, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "(in seconds)";
            // 
            // numericUpDownFlashNonTarget
            // 
            this.numericUpDownFlashNonTarget.DecimalPlaces = 3;
            this.numericUpDownFlashNonTarget.Location = new System.Drawing.Point(138, 49);
            this.numericUpDownFlashNonTarget.Name = "numericUpDownFlashNonTarget";
            this.numericUpDownFlashNonTarget.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownFlashNonTarget.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Flash duration target:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(315, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "(in seconds)";
            // 
            // numericUpDownFlashTarget
            // 
            this.numericUpDownFlashTarget.DecimalPlaces = 3;
            this.numericUpDownFlashTarget.Location = new System.Drawing.Point(138, 19);
            this.numericUpDownFlashTarget.Name = "numericUpDownFlashTarget";
            this.numericUpDownFlashTarget.Size = new System.Drawing.Size(171, 20);
            this.numericUpDownFlashTarget.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Flash duration non target:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.checkBoxOgreDebug);
            this.groupBox1.Controls.Add(this.textBoxLogDisc);
            this.groupBox1.Controls.Add(this.buttonDetectSerialToUsb);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.checkBoxLogConsole);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.checkBoxLogDisc);
            this.groupBox1.Controls.Add(this.textBoxVrpnPeripheral);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.checkBoxShuffledEnabled);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxComPort);
            this.groupBox1.Controls.Add(this.comboBoxTagging);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(5, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 214);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(173, 189);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(115, 13);
            this.label37.TabIndex = 16;
            this.label37.Text = "OGRE debug enabled:";
            // 
            // checkBoxOgreDebug
            // 
            this.checkBoxOgreDebug.AutoSize = true;
            this.checkBoxOgreDebug.Location = new System.Drawing.Point(294, 188);
            this.checkBoxOgreDebug.Name = "checkBoxOgreDebug";
            this.checkBoxOgreDebug.Size = new System.Drawing.Size(15, 14);
            this.checkBoxOgreDebug.TabIndex = 15;
            this.checkBoxOgreDebug.UseVisualStyleBackColor = true;
            // 
            // textBoxLogDisc
            // 
            this.textBoxLogDisc.Location = new System.Drawing.Point(138, 153);
            this.textBoxLogDisc.Name = "textBoxLogDisc";
            this.textBoxLogDisc.ReadOnly = true;
            this.textBoxLogDisc.Size = new System.Drawing.Size(193, 20);
            this.textBoxLogDisc.TabIndex = 14;
            // 
            // checkBoxLogDisc
            // 
            this.checkBoxLogDisc.AutoSize = true;
            this.checkBoxLogDisc.Location = new System.Drawing.Point(112, 156);
            this.checkBoxLogDisc.Name = "checkBoxLogDisc";
            this.checkBoxLogDisc.Size = new System.Drawing.Size(15, 14);
            this.checkBoxLogDisc.TabIndex = 9;
            this.checkBoxLogDisc.UseVisualStyleBackColor = true;
            // 
            // textBoxVrpnPeripheral
            // 
            this.textBoxVrpnPeripheral.Location = new System.Drawing.Point(112, 124);
            this.textBoxVrpnPeripheral.Name = "textBoxVrpnPeripheral";
            this.textBoxVrpnPeripheral.Size = new System.Drawing.Size(219, 20);
            this.textBoxVrpnPeripheral.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 127);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Vrpn Peripheral:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tagging Method:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "COM port:";
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Location = new System.Drawing.Point(131, 13);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.ReadOnly = true;
            this.textBoxFilePath.Size = new System.Drawing.Size(672, 20);
            this.textBoxFilePath.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Config file:";
            // 
            // buttonSaveStart
            // 
            this.buttonSaveStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveStart.Location = new System.Drawing.Point(270, 465);
            this.buttonSaveStart.Name = "buttonSaveStart";
            this.buttonSaveStart.Size = new System.Drawing.Size(287, 43);
            this.buttonSaveStart.TabIndex = 22;
            this.buttonSaveStart.Text = "Save and start Brain Invaders";
            this.buttonSaveStart.UseVisualStyleBackColor = true;
            this.buttonSaveStart.Click += new System.EventHandler(this.buttonSaveStart_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(809, 543);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Experiment Setup";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboBoxModality);
            this.groupBox9.Location = new System.Drawing.Point(6, 7);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(786, 44);
            this.groupBox9.TabIndex = 13;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Modality";
            // 
            // comboBoxModality
            // 
            this.comboBoxModality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxModality.FormattingEnabled = true;
            this.comboBoxModality.Items.AddRange(new object[] {
            "1) Brain Invaders P300 using MDM Training/Online - hardware tagging (FREQUENTLY U" +
                "SED)",
            "2) Brain Invaders P300 using MDM Adaptive - no training - hardware tagging (FREQU" +
                "ENTLY USED)",
            "3) Brain Invaders P300 using MDM Adaptive - latency-correction - no training - ha" +
                "rdware tagging",
            "4) Brain Invaders P300 using MDM Training/Online - software tagging",
            "5) Brain Invaders P300 on EMOTIV device  using MDM Training/Online - software tag" +
                "ging",
            "6) Brain Invaders P300 using MDM Adaptive - 4 playes - hardware tagging",
            "7) Brain Invaders P300 using MDM Adaptive - 2 players - 32 electrods each",
            "8) Brain Invaders P300 using MDM Adaptive - Full Multiplayer - hardware tagging (" +
                "FREQUENTLY USED)"});
            this.comboBoxModality.Location = new System.Drawing.Point(9, 14);
            this.comboBoxModality.Name = "comboBoxModality";
            this.comboBoxModality.Size = new System.Drawing.Size(771, 21);
            this.comboBoxModality.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBoxIsAdaptive);
            this.groupBox8.Controls.Add(this.label43);
            this.groupBox8.Controls.Add(this.textBoxNrep);
            this.groupBox8.Controls.Add(this.textBox6);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.textBox4);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.textBox3);
            this.groupBox8.Location = new System.Drawing.Point(477, 178);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(315, 144);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Environmnet variables (used by OV and BI)";
            // 
            // textBoxIsAdaptive
            // 
            this.textBoxIsAdaptive.Location = new System.Drawing.Point(108, 111);
            this.textBoxIsAdaptive.Name = "textBoxIsAdaptive";
            this.textBoxIsAdaptive.Size = new System.Drawing.Size(65, 20);
            this.textBoxIsAdaptive.TabIndex = 14;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(105, 67);
            this.label43.MaximumSize = new System.Drawing.Size(210, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(210, 39);
            this.label43.TabIndex = 13;
            this.label43.Text = "Equals \"Repetitions per life\" and it is send to the OpenVibe scenario, so that th" +
    "e script knows when to send  response to BI.";
            // 
            // textBoxNrep
            // 
            this.textBoxNrep.Location = new System.Drawing.Point(20, 67);
            this.textBoxNrep.Name = "textBoxNrep";
            this.textBoxNrep.Size = new System.Drawing.Size(74, 20);
            this.textBoxNrep.TabIndex = 11;
            this.textBoxNrep.Text = "NREP";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(20, 111);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(74, 20);
            this.textBox6.TabIndex = 10;
            this.textBox6.Text = "isadaptive";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(105, 24);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 13);
            this.label32.TabIndex = 9;
            this.label32.Text = "Value";
            this.label32.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(108, 40);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(201, 20);
            this.textBox4.TabIndex = 8;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(17, 24);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 7;
            this.label31.Text = "Name";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(20, 40);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(74, 20);
            this.textBox3.TabIndex = 6;
            this.textBox3.Text = "savepath";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonTestConfiguration);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this.buttonStartAdaptive);
            this.groupBox7.Controls.Add(this.buttonCopyLogClipbaord);
            this.groupBox7.Controls.Add(this.listBoxLog);
            this.groupBox7.Controls.Add(this.buttonRandomize);
            this.groupBox7.Controls.Add(this.checkBoxStartBI);
            this.groupBox7.Controls.Add(this.checkBoxStartOV);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.button2);
            this.groupBox7.Controls.Add(this.button3);
            this.groupBox7.Controls.Add(this.button4);
            this.groupBox7.Location = new System.Drawing.Point(6, 227);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(465, 310);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Execution";
            // 
            // buttonTestConfiguration
            // 
            this.buttonTestConfiguration.Location = new System.Drawing.Point(148, 19);
            this.buttonTestConfiguration.Name = "buttonTestConfiguration";
            this.buttonTestConfiguration.Size = new System.Drawing.Size(130, 28);
            this.buttonTestConfiguration.TabIndex = 34;
            this.buttonTestConfiguration.Text = "Test configuration";
            this.buttonTestConfiguration.UseVisualStyleBackColor = true;
            this.buttonTestConfiguration.Click += new System.EventHandler(this.buttonTestConfiguration_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(9, 144);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(25, 13);
            this.label45.TabIndex = 33;
            this.label45.Text = "Log";
            // 
            // buttonStartAdaptive
            // 
            this.buttonStartAdaptive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartAdaptive.Location = new System.Drawing.Point(10, 104);
            this.buttonStartAdaptive.Name = "buttonStartAdaptive";
            this.buttonStartAdaptive.Size = new System.Drawing.Size(438, 33);
            this.buttonStartAdaptive.TabIndex = 32;
            this.buttonStartAdaptive.Text = "Start adaptive";
            this.buttonStartAdaptive.UseVisualStyleBackColor = true;
            this.buttonStartAdaptive.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonCopyLogClipbaord
            // 
            this.buttonCopyLogClipbaord.Location = new System.Drawing.Point(327, 274);
            this.buttonCopyLogClipbaord.Name = "buttonCopyLogClipbaord";
            this.buttonCopyLogClipbaord.Size = new System.Drawing.Size(121, 30);
            this.buttonCopyLogClipbaord.TabIndex = 31;
            this.buttonCopyLogClipbaord.Text = "Copy log to clipboard";
            this.buttonCopyLogClipbaord.UseVisualStyleBackColor = true;
            this.buttonCopyLogClipbaord.Click += new System.EventHandler(this.buttonCopyLogClipbaord_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(9, 160);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxLog.Size = new System.Drawing.Size(439, 108);
            this.listBoxLog.TabIndex = 30;
            // 
            // buttonRandomize
            // 
            this.buttonRandomize.Location = new System.Drawing.Point(284, 19);
            this.buttonRandomize.Name = "buttonRandomize";
            this.buttonRandomize.Size = new System.Drawing.Size(164, 28);
            this.buttonRandomize.TabIndex = 8;
            this.buttonRandomize.Text = "Randomize for this subject";
            this.buttonRandomize.UseVisualStyleBackColor = true;
            this.buttonRandomize.Visible = false;
            this.buttonRandomize.Click += new System.EventHandler(this.buttonRandomize_Click);
            // 
            // checkBoxStartBI
            // 
            this.checkBoxStartBI.AutoSize = true;
            this.checkBoxStartBI.Checked = true;
            this.checkBoxStartBI.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStartBI.Location = new System.Drawing.Point(81, 23);
            this.checkBoxStartBI.Name = "checkBoxStartBI";
            this.checkBoxStartBI.Size = new System.Drawing.Size(61, 17);
            this.checkBoxStartBI.TabIndex = 7;
            this.checkBoxStartBI.Text = "Start BI";
            this.checkBoxStartBI.UseVisualStyleBackColor = true;
            // 
            // checkBoxStartOV
            // 
            this.checkBoxStartOV.AutoSize = true;
            this.checkBoxStartOV.Checked = true;
            this.checkBoxStartOV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStartOV.Location = new System.Drawing.Point(9, 24);
            this.checkBoxStartOV.Name = "checkBoxStartOV";
            this.checkBoxStartOV.Size = new System.Drawing.Size(66, 17);
            this.checkBoxStartOV.TabIndex = 6;
            this.checkBoxStartOV.Text = "Start OV";
            this.checkBoxStartOV.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 35);
            this.button1.TabIndex = 2;
            this.button1.Text = "Start train phase";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(131, 59);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 35);
            this.button2.TabIndex = 3;
            this.button2.Text = "Start online phase";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(245, 59);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(90, 35);
            this.button3.TabIndex = 4;
            this.button3.Text = "Train Phase 2";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(354, 59);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 35);
            this.button4.TabIndex = 5;
            this.button4.Text = "Online Phase 2";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox1);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.textBoxOutputFolder);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.textBoxOnlineScerario);
            this.groupBox6.Controls.Add(this.textBoxTrainScenario);
            this.groupBox6.Controls.Add(this.textBoxOpenVibeFolder);
            this.groupBox6.Location = new System.Drawing.Point(6, 57);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(465, 164);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "OpenVibe";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(257, 141);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(190, 20);
            this.textBox1.TabIndex = 35;
            this.textBox1.Text = "$env{savepath}/record-$core{time}.ov";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(9, 141);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(250, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "You must put in your Openvibe writer box this string:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(9, 118);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 13);
            this.label36.TabIndex = 7;
            this.label36.Text = "Output folder";
            // 
            // textBoxOutputFolder
            // 
            this.textBoxOutputFolder.Location = new System.Drawing.Point(95, 118);
            this.textBoxOutputFolder.Name = "textBoxOutputFolder";
            this.textBoxOutputFolder.Size = new System.Drawing.Size(352, 20);
            this.textBoxOutputFolder.TabIndex = 6;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 85);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 13);
            this.label35.TabIndex = 5;
            this.label35.Text = "Online scenario";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 59);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(74, 13);
            this.label34.TabIndex = 4;
            this.label34.Text = "Train scenario";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 23);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 13);
            this.label33.TabIndex = 3;
            this.label33.Text = "OpenVibe folder";
            // 
            // textBoxOnlineScerario
            // 
            this.textBoxOnlineScerario.Location = new System.Drawing.Point(95, 85);
            this.textBoxOnlineScerario.Name = "textBoxOnlineScerario";
            this.textBoxOnlineScerario.Size = new System.Drawing.Size(353, 20);
            this.textBoxOnlineScerario.TabIndex = 2;
            // 
            // textBoxTrainScenario
            // 
            this.textBoxTrainScenario.Location = new System.Drawing.Point(95, 55);
            this.textBoxTrainScenario.Name = "textBoxTrainScenario";
            this.textBoxTrainScenario.Size = new System.Drawing.Size(352, 20);
            this.textBoxTrainScenario.TabIndex = 1;
            // 
            // textBoxOpenVibeFolder
            // 
            this.textBoxOpenVibeFolder.Location = new System.Drawing.Point(95, 20);
            this.textBoxOpenVibeFolder.Name = "textBoxOpenVibeFolder";
            this.textBoxOpenVibeFolder.Size = new System.Drawing.Size(352, 20);
            this.textBoxOpenVibeFolder.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.textBoxUserID);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.textBoxSessionNumber);
            this.groupBox5.Location = new System.Drawing.Point(477, 57);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(315, 115);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Subject";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 7;
            this.label26.Text = "Subject ID";
            // 
            // textBoxUserID
            // 
            this.textBoxUserID.Location = new System.Drawing.Point(100, 20);
            this.textBoxUserID.Name = "textBoxUserID";
            this.textBoxUserID.Size = new System.Drawing.Size(117, 20);
            this.textBoxUserID.TabIndex = 0;
            this.textBoxUserID.Text = "Anton";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 62);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Session number";
            // 
            // textBoxSessionNumber
            // 
            this.textBoxSessionNumber.Location = new System.Drawing.Point(100, 59);
            this.textBoxSessionNumber.Name = "textBoxSessionNumber";
            this.textBoxSessionNumber.Size = new System.Drawing.Size(117, 20);
            this.textBoxSessionNumber.TabIndex = 1;
            this.textBoxSessionNumber.Text = "1";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.comboBoxP300Channel);
            this.tabPage4.Controls.Add(this.label53);
            this.tabPage4.Controls.Add(this.buttonBrowseP300CurrentSubject);
            this.tabPage4.Controls.Add(this.buttonBrowseP300Reference);
            this.tabPage4.Controls.Add(this.buttonCompareP300);
            this.tabPage4.Controls.Add(this.textBoxP300CurrentSubject);
            this.tabPage4.Controls.Add(this.label52);
            this.tabPage4.Controls.Add(this.textBoxP300Reference);
            this.tabPage4.Controls.Add(this.label51);
            this.tabPage4.Controls.Add(this.chartReference);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(809, 543);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Signal Check";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // comboBoxP300Channel
            // 
            this.comboBoxP300Channel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxP300Channel.FormattingEnabled = true;
            this.comboBoxP300Channel.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16"});
            this.comboBoxP300Channel.Location = new System.Drawing.Point(62, 7);
            this.comboBoxP300Channel.Name = "comboBoxP300Channel";
            this.comboBoxP300Channel.Size = new System.Drawing.Size(38, 21);
            this.comboBoxP300Channel.TabIndex = 11;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(10, 10);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(46, 13);
            this.label53.TabIndex = 10;
            this.label53.Text = "Channel";
            // 
            // buttonBrowseP300CurrentSubject
            // 
            this.buttonBrowseP300CurrentSubject.Location = new System.Drawing.Point(673, 87);
            this.buttonBrowseP300CurrentSubject.Name = "buttonBrowseP300CurrentSubject";
            this.buttonBrowseP300CurrentSubject.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseP300CurrentSubject.TabIndex = 8;
            this.buttonBrowseP300CurrentSubject.Text = "Browse";
            this.buttonBrowseP300CurrentSubject.UseVisualStyleBackColor = true;
            this.buttonBrowseP300CurrentSubject.Click += new System.EventHandler(this.buttonBrowseP300CurrentSubject_Click);
            // 
            // buttonBrowseP300Reference
            // 
            this.buttonBrowseP300Reference.Location = new System.Drawing.Point(673, 48);
            this.buttonBrowseP300Reference.Name = "buttonBrowseP300Reference";
            this.buttonBrowseP300Reference.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseP300Reference.TabIndex = 7;
            this.buttonBrowseP300Reference.Text = "Browse";
            this.buttonBrowseP300Reference.UseVisualStyleBackColor = true;
            this.buttonBrowseP300Reference.Click += new System.EventHandler(this.buttonBrowseP300Reference_Click);
            // 
            // buttonCompareP300
            // 
            this.buttonCompareP300.Location = new System.Drawing.Point(9, 116);
            this.buttonCompareP300.Name = "buttonCompareP300";
            this.buttonCompareP300.Size = new System.Drawing.Size(91, 30);
            this.buttonCompareP300.TabIndex = 6;
            this.buttonCompareP300.Text = "Compare";
            this.buttonCompareP300.UseVisualStyleBackColor = true;
            this.buttonCompareP300.Click += new System.EventHandler(this.buttonCompareP300_Click);
            // 
            // textBoxP300CurrentSubject
            // 
            this.textBoxP300CurrentSubject.Location = new System.Drawing.Point(9, 90);
            this.textBoxP300CurrentSubject.Name = "textBoxP300CurrentSubject";
            this.textBoxP300CurrentSubject.Size = new System.Drawing.Size(642, 20);
            this.textBoxP300CurrentSubject.TabIndex = 5;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 74);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(244, 13);
            this.label52.TabIndex = 4;
            this.label52.Text = "Current subject P300 average matrix (ex. MDM.txt)";
            // 
            // textBoxP300Reference
            // 
            this.textBoxP300Reference.Location = new System.Drawing.Point(9, 51);
            this.textBoxP300Reference.Name = "textBoxP300Reference";
            this.textBoxP300Reference.Size = new System.Drawing.Size(642, 20);
            this.textBoxP300Reference.TabIndex = 2;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 34);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(458, 13);
            this.label51.TabIndex = 1;
            this.label51.Text = "Reference P300 average matrix (ex. \\brain-invaders-mdm-adaptive\\config\\mdm-generi" +
    "c-P1.csv)";
            // 
            // chartReference
            // 
            chartArea1.Name = "ChartArea1";
            this.chartReference.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartReference.Legends.Add(legend1);
            this.chartReference.Location = new System.Drawing.Point(9, 152);
            this.chartReference.Name = "chartReference";
            this.chartReference.Size = new System.Drawing.Size(785, 337);
            this.chartReference.TabIndex = 0;
            this.chartReference.Text = "chartReference";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox12);
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(809, 543);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Diagnostics";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBox5);
            this.groupBox12.Controls.Add(this.label55);
            this.groupBox12.Controls.Add(this.buttonAutoDetectPP);
            this.groupBox12.Controls.Add(this.numericUpDownPPAddress);
            this.groupBox12.Location = new System.Drawing.Point(493, 13);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(180, 268);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Parallel Port config";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(7, 123);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(163, 129);
            this.textBox5.TabIndex = 12;
            this.textBox5.Text = resources.GetString("textBox5.Text");
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 46);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(45, 13);
            this.label55.TabIndex = 2;
            this.label55.Text = "Address";
            // 
            // buttonAutoDetectPP
            // 
            this.buttonAutoDetectPP.Location = new System.Drawing.Point(7, 83);
            this.buttonAutoDetectPP.Name = "buttonAutoDetectPP";
            this.buttonAutoDetectPP.Size = new System.Drawing.Size(75, 23);
            this.buttonAutoDetectPP.TabIndex = 1;
            this.buttonAutoDetectPP.Text = "Auto-detect";
            this.buttonAutoDetectPP.UseVisualStyleBackColor = true;
            this.buttonAutoDetectPP.Click += new System.EventHandler(this.buttonAutoDetectPP_Click);
            // 
            // numericUpDownPPAddress
            // 
            this.numericUpDownPPAddress.Location = new System.Drawing.Point(90, 44);
            this.numericUpDownPPAddress.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.numericUpDownPPAddress.Name = "numericUpDownPPAddress";
            this.numericUpDownPPAddress.Size = new System.Drawing.Size(62, 20);
            this.numericUpDownPPAddress.TabIndex = 0;
            this.numericUpDownPPAddress.Value = new decimal(new int[] {
            888,
            0,
            0,
            0});
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.numericUpDownManagedValue);
            this.groupBox11.Controls.Add(this.label54);
            this.groupBox11.Controls.Add(this.buttonSendValueManaged);
            this.groupBox11.Location = new System.Drawing.Point(7, 287);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(244, 76);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Tagging with serial port using System.IO.Ports";
            // 
            // numericUpDownManagedValue
            // 
            this.numericUpDownManagedValue.Location = new System.Drawing.Point(70, 31);
            this.numericUpDownManagedValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownManagedValue.Name = "numericUpDownManagedValue";
            this.numericUpDownManagedValue.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownManagedValue.TabIndex = 5;
            this.numericUpDownManagedValue.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(19, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 13);
            this.label54.TabIndex = 1;
            this.label54.Text = "Value";
            // 
            // buttonSendValueManaged
            // 
            this.buttonSendValueManaged.Location = new System.Drawing.Point(142, 28);
            this.buttonSendValueManaged.Name = "buttonSendValueManaged";
            this.buttonSendValueManaged.Size = new System.Drawing.Size(75, 23);
            this.buttonSendValueManaged.TabIndex = 0;
            this.buttonSendValueManaged.Text = "Send";
            this.buttonSendValueManaged.UseVisualStyleBackColor = true;
            this.buttonSendValueManaged.Click += new System.EventHandler(this.buttonSendValueManaged_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.buttonSendOne);
            this.groupBox10.Controls.Add(this.textBox2);
            this.groupBox10.Controls.Add(this.buttonTestTagging);
            this.groupBox10.Controls.Add(this.numericUpDownTestValueTimeKeep);
            this.groupBox10.Controls.Add(this.numericUpDownTestPauseTime);
            this.groupBox10.Controls.Add(this.label50);
            this.groupBox10.Controls.Add(this.label49);
            this.groupBox10.Controls.Add(this.label48);
            this.groupBox10.Controls.Add(this.label47);
            this.groupBox10.Controls.Add(this.numericUpDownTestValue);
            this.groupBox10.Controls.Add(this.label46);
            this.groupBox10.Controls.Add(this.numericUpDownTestValueCount);
            this.groupBox10.Controls.Add(this.comboBoxTaggingTestMethod);
            this.groupBox10.Location = new System.Drawing.Point(7, 13);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(479, 268);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Tagging using BI native C++ library";
            // 
            // buttonSendOne
            // 
            this.buttonSendOne.Location = new System.Drawing.Point(336, 165);
            this.buttonSendOne.Name = "buttonSendOne";
            this.buttonSendOne.Size = new System.Drawing.Size(129, 23);
            this.buttonSendOne.TabIndex = 1;
            this.buttonSendOne.Text = "Send one time";
            this.buttonSendOne.UseVisualStyleBackColor = true;
            this.buttonSendOne.Click += new System.EventHandler(this.buttonSendOne_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(22, 203);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(443, 49);
            this.textBox2.TabIndex = 11;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            // 
            // buttonTestTagging
            // 
            this.buttonTestTagging.Location = new System.Drawing.Point(336, 136);
            this.buttonTestTagging.Name = "buttonTestTagging";
            this.buttonTestTagging.Size = new System.Drawing.Size(129, 23);
            this.buttonTestTagging.TabIndex = 0;
            this.buttonTestTagging.Text = "Test loop same value";
            this.buttonTestTagging.UseVisualStyleBackColor = true;
            this.buttonTestTagging.Click += new System.EventHandler(this.buttonTestTagging_Click);
            // 
            // numericUpDownTestValueTimeKeep
            // 
            this.numericUpDownTestValueTimeKeep.Location = new System.Drawing.Point(412, 69);
            this.numericUpDownTestValueTimeKeep.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTestValueTimeKeep.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownTestValueTimeKeep.Name = "numericUpDownTestValueTimeKeep";
            this.numericUpDownTestValueTimeKeep.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownTestValueTimeKeep.TabIndex = 10;
            this.numericUpDownTestValueTimeKeep.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // numericUpDownTestPauseTime
            // 
            this.numericUpDownTestPauseTime.Location = new System.Drawing.Point(412, 102);
            this.numericUpDownTestPauseTime.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTestPauseTime.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownTestPauseTime.Name = "numericUpDownTestPauseTime";
            this.numericUpDownTestPauseTime.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownTestPauseTime.TabIndex = 9;
            this.numericUpDownTestPauseTime.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(309, 102);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(92, 13);
            this.label50.TabIndex = 8;
            this.label50.Text = "Keep value 0 (ms)";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(309, 71);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(83, 13);
            this.label49.TabIndex = 7;
            this.label49.Text = "Keep value (ms)";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(197, 71);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(35, 13);
            this.label48.TabIndex = 6;
            this.label48.Text = "Count";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(412, 34);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(0, 13);
            this.label47.TabIndex = 5;
            // 
            // numericUpDownTestValue
            // 
            this.numericUpDownTestValue.Location = new System.Drawing.Point(104, 69);
            this.numericUpDownTestValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownTestValue.Name = "numericUpDownTestValue";
            this.numericUpDownTestValue.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownTestValue.TabIndex = 4;
            this.numericUpDownTestValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(19, 71);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(79, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Value to repeat";
            // 
            // numericUpDownTestValueCount
            // 
            this.numericUpDownTestValueCount.Location = new System.Drawing.Point(238, 69);
            this.numericUpDownTestValueCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTestValueCount.Name = "numericUpDownTestValueCount";
            this.numericUpDownTestValueCount.Size = new System.Drawing.Size(56, 20);
            this.numericUpDownTestValueCount.TabIndex = 2;
            this.numericUpDownTestValueCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // comboBoxTaggingTestMethod
            // 
            this.comboBoxTaggingTestMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTaggingTestMethod.FormattingEnabled = true;
            this.comboBoxTaggingTestMethod.Items.AddRange(new object[] {
            "8 bit parallel Port",
            "Serial Port",
            "Software Tagging"});
            this.comboBoxTaggingTestMethod.Location = new System.Drawing.Point(22, 24);
            this.comboBoxTaggingTestMethod.Name = "comboBoxTaggingTestMethod";
            this.comboBoxTaggingTestMethod.Size = new System.Drawing.Size(195, 21);
            this.comboBoxTaggingTestMethod.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::brain_invaders_launcher.Properties.Resources.cool7;
            this.pictureBox1.Location = new System.Drawing.Point(477, 328);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(315, 209);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 621);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Brain Invaders Launcher";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBoxScreen.ResumeLayout(false);
            this.groupBoxScreen.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVRPNTargetReceivedTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPauseBetweenLevels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPauseAfterDestruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPauseBetweenRowsAndColumns)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxTrainingRepetitionsPerTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepetitionsPerLife)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLives)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxTrainingTargets)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownISIAdaptation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMeanISI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxISI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinISI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlashNonTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFlashTarget)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartReference)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPAddress)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownManagedValue)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestValueTimeKeep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestPauseTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestValueCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveConfigAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineDocumentationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolTip toolTipShuffling;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.GroupBox groupBoxScreen;
        private System.Windows.Forms.CheckBox checkBoxVideoSetup;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.NumericUpDown numericUpDownVRPNTargetReceivedTimeout;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown numericUpDownPauseBetweenLevels;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown numericUpDownPauseAfterDestruction;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown numericUpDownPauseBetweenRowsAndColumns;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDownLives;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxTrainingTargets;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDownMeanISI;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxISI;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDownMinISI;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownFlashNonTarget;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownFlashTarget;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxLogDisc;
        private System.Windows.Forms.Button buttonDetectSerialToUsb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkBoxLogConsole;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxLogDisc;
        private System.Windows.Forms.TextBox textBoxVrpnPeripheral;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxShuffledEnabled;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxComPort;
        private System.Windows.Forms.ComboBox comboBoxTagging;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSaveStart;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxSessionNumber;
        private System.Windows.Forms.TextBox textBoxUserID;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox checkBoxStartBI;
        private System.Windows.Forms.CheckBox checkBoxStartOV;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxOnlineScerario;
        private System.Windows.Forms.TextBox textBoxTrainScenario;
        private System.Windows.Forms.TextBox textBoxOpenVibeFolder;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxOutputFolder;
        private System.Windows.Forms.Button buttonRandomize;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.Button buttonCopyLogClipbaord;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox checkBoxOgreDebug;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.NumericUpDown numericUpDownISIAdaptation;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.CheckBox checkBoxISIAdaptationEnabled;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button buttonStartAdaptive;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboBoxModality;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxTrainingRepetitionsPerTarget;
        private System.Windows.Forms.NumericUpDown numericUpDownRepetitionsPerLife;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBoxNrep;
        private System.Windows.Forms.TextBox textBoxIsAdaptive;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private OvalPictureBox pictureBox1;
        private System.Windows.Forms.Button buttonTestConfiguration;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button buttonTestTagging;
        private System.Windows.Forms.ComboBox comboBoxTaggingTestMethod;
        private System.Windows.Forms.NumericUpDown numericUpDownTestValueTimeKeep;
        private System.Windows.Forms.NumericUpDown numericUpDownTestPauseTime;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.NumericUpDown numericUpDownTestValue;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown numericUpDownTestValueCount;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox textBoxP300CurrentSubject;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBoxP300Reference;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartReference;
        private System.Windows.Forms.Button buttonCompareP300;
        private System.Windows.Forms.Button buttonBrowseP300CurrentSubject;
        private System.Windows.Forms.Button buttonBrowseP300Reference;
        private System.Windows.Forms.ComboBox comboBoxP300Channel;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button buttonSendOne;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button buttonSendValueManaged;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.NumericUpDown numericUpDownManagedValue;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button buttonAutoDetectPP;
        private System.Windows.Forms.NumericUpDown numericUpDownPPAddress;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox5;
    }
}

