﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using MicrosoftResearch.Infer.Models;
using MicrosoftResearch.Infer;
using MicrosoftResearch.Infer.Distributions;
using MicrosoftResearch.Infer.Maths;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using System.Linq;

using BayesPointMachine;

[Serializable]
struct SerializationObject
{
    public int nClass;
    public int nFeatures;
    public double noisePrec;
    public VectorGaussian[] wInitTrainModel;
}

class PlatformInvokeTest
{
    static BPM bpmIncremental;
    static int nClass = 2;
    static double noisePrec;
    static int totalFeatures;


    static PlatformInvokeTest()
    {
        Console.WriteLine("Bayes Point Machine initialized ...");
        bpmIncremental = null;
        noisePrec = 0.1; //default 0.1
    }

    [DllExporter.DllExport]
    public static double Train([MarshalAs (UnmanagedType.LPArray, SizeParamIndex = 1)]
        double[] values, int maxSize, int actualSize, int vectorsCount)
    {
        //Console.WriteLine("\n------- BPM Train Incremental -------");
        //Console.WriteLine("maxSize " + maxSize);
        //Console.WriteLine("actualSize " + actualSize);
        //Console.WriteLine("vectorsCount " + vectorsCount);

        totalFeatures = (actualSize / vectorsCount) - 1; //-1 for the class value
        if (totalFeatures <= vectorsCount) Console.WriteLine("[Bayes Point Machine .NET]: " + "Input feature vectors should be more than the dimensions of the feature space!");
        //Console.WriteLine("Features " + totalFeatures);

        if (bpmIncremental == null)
        {
            bpmIncremental = new BPM(nClass, totalFeatures, noisePrec);
            bpmIncremental.TrainingEngine.ShowProgress = false;
            bpmIncremental.TestEngine.ShowProgress = false;
            //bpmIncremental.TrainingEngine.NumberOfIterations = 500;
        }

        VectorGaussian[] wInfer = new VectorGaussian[nClass];

        #region create chunking to avoid errors related to matrix not being positive and definitive
        int minimumVectorLength = totalFeatures + 3;
        int nChunks = vectorsCount / minimumVectorLength;
        int vectorsPerChunk = vectorsCount / nChunks;
        #endregion

        Console.WriteLine("Vectors per chunk x number of features: " + vectorsPerChunk + " x " + totalFeatures);

        try
        {
            //main process loop
            for (int c = 0; c < nChunks; c++)
            {
                List<Vector>[] dataChunks = PrepareData(values, actualSize, nClass, vectorsCount, totalFeatures, c, vectorsPerChunk);
                wInfer = bpmIncremental.TrainIncremental(dataChunks);
            }

            //process vectors less than a chunk at the end
            //disabled because - if we have a number of vectors less than the number of features than we might get an exception
            //int vectorsLeft = vectorsCount - (nChunks * vectorsPerChunk);
            //if (vectorsLeft > 0)
            //{
            //    List<Vector>[] dataChunks = PrepareData(values, actualSize, nClass, vectorsCount, totalFeatures, nChunks + 1, vectorsPerChunk);
            //    wInfer = bpmIncremental.TrainIncremental(dataChunks);
            //}
        }
        catch (MicrosoftResearch.Infer.Maths.PositiveDefiniteMatrixException e)
        {
            Console.WriteLine("[Bayes Point Machine excpetion in .NET]: " + e.Message);
            return 2;
        }
        catch (Exception e)
        {
            Console.WriteLine("[Bayes Point Machine excpetion in .NET]: " + e.Message);
            return 1;
        }

        return 0;
    }

    public static List<Vector>[] PrepareData(double[] values, int count, int nClass, int vectorsCount, int featureCount, int nChunk, int vectorsPerChunk)
    {
        List<Vector>[] data = new List<Vector>[nClass];
        for (int c = 0; c < nClass; c++)
        {
            data[c] = new List<Vector>();
        }

        //Console.WriteLine("total features = " + featureCount);
        for (int i = nChunk * vectorsPerChunk; i < nChunk * vectorsPerChunk + vectorsPerChunk && i < vectorsCount; i++)
        //for (int i = 0; i < vectorsCount; i++)
        {
            double[] x = new double[featureCount];

            //extract the class label
            int classId = Convert.ToInt32(values[i * (featureCount + 1) + featureCount]);
            classId--; //relabelling from 1,2 to 0,1
            
            //Console.WriteLine("Class = " + classId);

            //extract the features in x vector
            for (int k = 0; k < x.Length; k++)
            {
                x[k] = values[i * (featureCount + 1) + k];
            }

            if (classId < 0) {Console.WriteLine("Invalid class index:" + classId); continue;}
            data[classId].Add(Vector.FromArray(x));
        }

        return data;
    }

    [DllExporter.DllExport]
    public static double Classify([MarshalAs
        (UnmanagedType.LPArray, SizeParamIndex = 1)]double[] values, int maxSize, int featuresCount)
    {
        //Console.WriteLine("==================");
        //Console.WriteLine("Classify");
        //Console.WriteLine("==================");
        //Console.WriteLine("\nPredictions:");
      
        double[] forProcessing = new double[featuresCount];
        for (int i=0;i<featuresCount;i++)
        {
            forProcessing[i] = values[i];
        }
        Vector[] testData = new Vector[1];
        testData[0] = Vector.FromArray(forProcessing);
        
        Discrete[] predictions = bpmIncremental.Test(testData);

        //foreach (Discrete pred in predictions)
        //    Console.WriteLine(pred);

        //we get the probability of the first class 
        return predictions[0].GetProbs()[0]; //OpenVibe compatibility classifiers 
    }

    [DllExporter.DllExport]
    public static string GetConfig(IntPtr dataBytes, int maxSize, int actualSize)
    {
        //Console.WriteLine("Loading config");

        byte[] model = new byte[actualSize];
        Marshal.Copy(dataBytes, model, 0, actualSize);
        //Console.WriteLine("received model size: "+model.Length);

        SerializationObject d = DeSerializeObject(model);
        noisePrec = d.noisePrec;
        totalFeatures = d.nFeatures;
        nClass = d.nClass;

        bpmIncremental = new BPM(nClass, totalFeatures, noisePrec);
        bpmIncremental.SaveData = d.wInitTrainModel;
        bpmIncremental.TrainingEngine.ShowProgress = false;
        bpmIncremental.TestEngine.ShowProgress = false;

        //Console.WriteLine("Loading config completed");

        return "not used";
    }

    [DllExporter.DllExport]
    public static int SetConfig(IntPtr dataBytes, int arraySize)
    {
        //Console.WriteLine("Saved config started");

        SerializationObject s;
        s.nClass = nClass;
        s.nFeatures = totalFeatures;
        s.noisePrec = noisePrec;
        s.wInitTrainModel = bpmIncremental.SaveData;

        byte[] result = SerializeObject(s);

        //Console.WriteLine(".net size" + (result.Length));

        if (arraySize < result.Length) throw new Exception("[Bayes Point Machine excpetion in .NET]: Buffer for model is not big enough!");

        Marshal.Copy(result, 0, dataBytes, result.Length);

        //Console.WriteLine("Saved config completed");

        return result.Length;
    }

    #region .NET helper methods
    public static byte[] SerializeObject(SerializationObject data)
    {
        MemoryStream stream = new MemoryStream();

        BinaryFormatter bFormatter = new BinaryFormatter();
        bFormatter.Serialize(stream, data);

        byte[] result = ReadToEnd(stream);
        stream.Close();

        return result;
    }

    public static SerializationObject DeSerializeObject(byte[] input)
    {
        SerializationObject objectToDeserialize;

        MemoryStream stream = new MemoryStream(input);

        BinaryFormatter bFormatter = new BinaryFormatter();
        objectToDeserialize = (SerializationObject)bFormatter.Deserialize(stream);
        stream.Close();
        return objectToDeserialize;
    }

    /// <summary>
    /// Simple stream to byte converter
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public static byte[] ReadToEnd(System.IO.Stream stream)
    {
        long originalPosition = 0;

        if (stream.CanSeek)
        {
            originalPosition = stream.Position;
            stream.Position = 0;
        }

        try
        {
            byte[] readBuffer = new byte[4096];

            int totalBytesRead = 0;
            int bytesRead;

            while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
            {
                totalBytesRead += bytesRead;

                if (totalBytesRead == readBuffer.Length)
                {
                    int nextByte = stream.ReadByte();
                    if (nextByte != -1)
                    {
                        byte[] temp = new byte[readBuffer.Length * 2];
                        Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                        Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                        readBuffer = temp;
                        totalBytesRead++;
                    }
                }
            }

            byte[] buffer = readBuffer;
            if (readBuffer.Length != totalBytesRead)
            {
                buffer = new byte[totalBytesRead];
                Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
            }
            return buffer;
        }
        finally
        {
            if (stream.CanSeek)
            {
                stream.Position = originalPosition;
            }
        }
    }
    #endregion
}
