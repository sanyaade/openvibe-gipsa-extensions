function y1 = bpmClassify(Ftest,Ftrain,Ytrain)

asm = NET.addAssembly('D:\dropbox-115\Dropbox\computer-126\bpm_matlab\BayesPointMachineClassifier.dll');

t = asm.AssemblyHandle.GetType('Gipsa.PlatformInvokeTest');
p = System.Activator.CreateInstance(t);

Nfeat = size(Ftrain,1);
Ntrial = size(Ftrain,2);


%set training data
d1 = NET.createArray('System.Double',(Nfeat+1)*Ntrial);

for i=1:Ntrial
    for j=1:Nfeat
        k = (i-1)*(Nfeat+1)+(j-1);
        d1.Set(k,Ftrain(j,i));
    end
    d1.Set((i-1)*(Nfeat+1)+Nfeat,Ytrain(i)+1);
end


%methods(p, '-full')

%double[] values, int maxSize, int actualSize, int vectorsCount
ret = p.Train(d1,(Nfeat+1)*Ntrial,(Nfeat+1)*Ntrial,Ntrial);

%set training data
Nfeat = size(Ftest,1);
Ntrial = size(Ftest,2);

for i=1:Ntrial
     d2 = NET.createArray('System.Double',Nfeat);
    for j=1:Nfeat
        d2.Set(j-1,Ftest(j,i));
    end
    y1(i) = p.Classify(d2,Nfeat,Nfeat);
end

%public double Classify(double[] values, int maxSize, int featuresCount)


