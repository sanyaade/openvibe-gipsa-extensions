﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using DllExporter;

namespace DummyLibrary
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DummyStruct
    {
        public short a;
        public ulong b;
        public byte c;
        public double d;
    }

    public delegate void Callback([MarshalAs(UnmanagedType.LPTStr)]string name);

    public class DummyClass
    {
        [DllExport]
        public static void DummyMethod() { }

        static void DummyMethod2() { }

        [DllExport]
        void DummyMethod3() { }

        [DllExport(EntryPoint = "SayHello")]
        [return:MarshalAs(UnmanagedType.LPTStr)]
        public static string Hello([MarshalAs(UnmanagedType.LPTStr)]string name)
        {
            return string.Format("Hello from .NET assembly, {0}!", name);
        }

        [DllExport]
        public static int Add([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)]int[] values, int count)
        {
            int result = 0;

            for (int i = 0; i < values.Length; i++)
                result += values[i];

            return result;
        }

        [DllExport]
        public static DummyStruct TestStruct() { return new DummyStruct { a = 1, b = 2, c = 3, d = 4 }; }

        [DllExport]
        public static void TestStructRef(ref DummyStruct dummyStruct)
        {
            dummyStruct.a += 5;
            dummyStruct.b += 6;
            dummyStruct.c += 7;
            dummyStruct.d += 8;
        }

        [DllExport]
        public static void DoCallback(Callback callback)
        {
            if (callback != null)
                callback(".NET assembly");
        }

        [DllExport]
        public static void Rainbow()
        {
            var oldColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write('R');
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write('a');
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write('i');
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write('n');
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write('b');
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write('o');
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write('w');

            Console.ForegroundColor = oldColor;
            Console.WriteLine();
        }
    }
}
