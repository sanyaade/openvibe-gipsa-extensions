clear all;

% Read the file
if isunix
    FILE = './data/data3.gdf';
else
    FILE = '.\data\data5.gdf';
end
[s h] = sload(FILE);
Fs = h.SampleRate;

%%%%% Parse Hardware trigger
% Chanel event
HardTrigger = s(:,end)./10^6;

% Parse channel event
% each flash has a value between 20 and 45
HardFlash = (HardTrigger >= 20) & (HardTrigger <= 45);

% Raising edge
HardFlash = [0; diff(HardFlash)==1];

% indices of each flash
ixHardFlash = find(HardFlash==1);

%%%%% Parse Software trigger
% Stimulation are recorded in the header h
% Flash codes are between 33025 and 33036

% indices of each Flash
ixSoftFlash = h.EVENT.POS( (h.EVENT.TYP >= 33025) & (h.EVENT.TYP <= 33036) );

% create a signal with 1 at the begining of each flash
SoftFlash = zeros(size(s,1),1);
SoftFlash(ixSoftFlash)=1;

%%%%% Plot Hardware and Software stimulation
figure;
t = 1/Fs:1/Fs:size(s,1)/Fs;
plot(t,HardFlash,'b','linewidth',2);
hold on;
plot(t,SoftFlash(1:size(s,1)),'r','linewidth',2);
ylim([-0.25 1.5]);xlim([t(1) t(end)]);
%%%%% Calculate the delay
% the delay is the diference of indices of each flash divided by the 
% sampling rate.
%ixHardFlash(1) = [];
%ixSoftFlash(1) = [];
delay = (ixHardFlash-ixSoftFlash)/Fs;
% plot the delays
figure;
td = t(ixHardFlash);
plot(td,delay*1000);



%%%%% Mean and std of the delay
md = mean(delay);
sdd = std(delay);

%%%%% Regression between delay and time to estimate the amont of drit, in 
% terms of ms/min
f = fit(td',delay,'poly1');
drift = (f.p1*60);

disp(['Mean delay is : ' num2str(md*1000) ' ms and std is ' num2str(sdd*1000) ' ms']);
disp(['Drift is : ' num2str(drift*1000) ' ms/min']);

%% 
ixHardFlash2 = ixHardFlash;
missing = diff(ixSoftFlash);
ix = find(missing>1.5*Fs);
missnumber = round(missing(ix)/Fs)-1;
ix = ix+1;
for i=1:length(ix)
    ixHardFlash2(ix(i):ix(i)+missnumber(i)-1) = [];
end

delay = (ixHardFlash2-ixSoftFlash(1:end-1))/Fs;
% plot the delays
figure;
subplot(2,1,1);
td = t(ixHardFlash2);
plot(td,delay*1000);


md = mean(delay);
sdd = std(delay);

%%%%% Regression between delay and time to estimate the amont of drit, in 
% terms of ms/min
f = fit(td',delay,'poly1');
drift = (f.p1*60);

disp(['Mean delay is : ' num2str(md*1000) ' ms and std is ' num2str(sdd*1000) ' ms']);
disp(['Drift is : ' num2str(drift*1000) ' ms/min']);

%drift removal
delay = delay-f.p1*td';
subplot(2,1,2);
plot(td,delay*1000);

md = mean(delay);
sdd = std(delay);

%%%%% Regression between delay and time to estimate the amont of drit, in 
% terms of ms/min
f = fit(td',delay,'poly1');
drift = (f.p1*60);

disp(['Mean delay is : ' num2str(md*1000) ' ms and std is ' num2str(sdd*1000) ' ms']);
disp(['Drift is : ' num2str(drift*1000) ' ms/min']);
