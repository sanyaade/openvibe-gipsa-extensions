Notes:

1) Project web-site: https://code.google.com/p/openvibe-gipsa-extensions/

2) This source can be compiled with the git source tree of OpenVibe:

git://scm.gforge.inria.fr/openvibe/openvibe.git

3) To compile openvibe-gipsa extensions dll you need to copy the *contents* of 

\src\openvibe-plugins-gipsa\trunc

to the source folder of OpenVibe: \externals\openvibe-plugins-gipsa 

and recompile OpenVibe.

4) Documentation and images for Bain Invaders are available at:

\src\all-brain-invaders\brain-invaders\trunc\doc

Images for Brain INvaders are in D:\Work\OpenVibe\gipsa-extensions\src\all-brain-invaders\brain-invaders\trunc\doc