# Welcome

**What's This?**: Our [team](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/Team) at [Gibsa-lab](http://www.gipsa-lab.fr) produces extensions for the [OpenVibe](http://openvibe.inria.fr)(OV) platform and independent applications.

Our current focus is on [P300 evoked potentials](http://en.wikipedia.org/wiki/P300_%28neuroscience%29) scenarios, which involves improving: digital signal processing, signal tagging and classification based on Riemannian geometry. We have developed a computer game called Brain Invaders where evoked potentials are used for shooting aliens :).

Software list:

* [OpenVibe-plugins-gipsa](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/OpenViBEPluginsGipsaDeployment) - extensions for the OV platform
* [Brain Invaders](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/BrainInvaders) - a real-time BCI game that uses P300. Features : 1-2 players, 5 game's modes, Fully adaptive, no training session required.
* [Enhanced P300 scenarios](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/P300Scenarios) for OpenVibe
* [Gipsa-lab Installer](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/InstallerMain) - installs all on top of OpenVibe
* C++ and .NET library for communication with OpenVibe

You can install all with our easy to use Gipsa-Installer from the [Downloads page](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/Downloads).

Yo can learn much more from the list of [all our pages](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/browse/).
***
**Aknowledgements:** 
The work deployed through this site has been partially funded by ANR project [OpenViBE2](http://www.irisa.fr/vr4i/openvibe2/wiki/index.php?title=Main_Page), ANR project [RoBIK](https://sites.google.com/site/robustbcikeyboard) and by [AFM](http://www.afm-telethon.fr).
***
**Publications:** [see here](https://bitbucket.org/toncho11/openvibe-gipsa-extensions/wiki/Publications)
***
**Related projects:**

* https://github.com/alexandrebarachant/covariancetoolbox Covariance toolbox (Matlab) - useful functions for Riemannian geometry
* https://sites.google.com/site/marcocongedo/software/ovtools Marco's OpenViBE Tools